#!/usr/bin/env python3

import f90nml,os
from scipy.interpolate import interp1d
from nimpy.plot_nimrod import PlotNimrod as pn
from nimpy.eval_nimrod import EvalNimrod
from nimpy.fsa import FSA

# this file requires .h5 dump files to read the time 
# edit dump file list
step_number_list = ['00000','22000','50000','68000']

# Do FSA
def basefsa(rzc, dy, eval_nimrod, isurf):
    '''
    Flux surface averge quantities (f/bdgrth where y[2]=1/bdgrth)
    Set neq to number of outputs in FSA call 
    and fill dy[4:4+neq]
    '''
    n = eval_nimrod.eval_field('n', rzc, dmode=0, eq=2) # total
    n0 = eval_nimrod.eval_field('n', rzc, dmode=0, eq=3) # axisym part
    dy[4] = n0[0]*dy[2] # ne0
    dy[5] = numpy.fabs(n[0]-n0[0])*dy[2] # ne tilde
    ti = eval_nimrod.eval_field('ti', rzc, dmode=0, eq=2)
    ti0 = eval_nimrod.eval_field('ti', rzc, dmode=0, eq=3)
    dy[6] = ti0[0]*dy[2]
    dy[7] = numpy.fabs(ti[0]-ti0[0])*dy[2]
    te = eval_nimrod.eval_field('te', rzc, dmode=0, eq=2)
    te0 = eval_nimrod.eval_field('te', rzc, dmode=0, eq=3)
    dy[8] = te0[0]*dy[2]
    dy[9] = numpy.fabs(te[0]-te0[0])*dy[2]
    # axisym only
    bf = eval_nimrod.eval_field('b', rzc, dmode=0, eq=3)
    vf = eval_nimrod.eval_field('v', rzc, dmode=0, eq=3)
    dy[10] = vf[2]*dy[2]/rzc[0] # omega
    dy[11] = (vf[0]*bf[0]+vf[1]*bf[1])*dy[2]/numpy.sqrt(bf[0]*bf[0]+bf[1]*bf[1]) # Kpol
    bigr[isurf] = max(bigr[isurf], rzc[0])
    return dy

# collect output here
rhon = numpy.arange(.5,1.0,0.002)
yrhon = []

# load dump files
time_list = []
for step_number in step_number_list:
    dumpfile = 'dumpgll.'+step_number+'.h5'
    infile = h5py.File(dumpfile,'r')
    time_list.append(infile["/dumpTime"].attrs.get("vsTime")*1.e6) # in micro-s
    nml = f90nml.read('nimrod.in')
    gmt = nml['grid_input']['geom']
    if gmt == 'tor':
        gmt=True
    else:
        gmt=False
    eval_nimrod = EvalNimrod(dumpfile, fieldlist='nvptb') #, coord='xyz')
    
    md=3.3435860e-27
    zc=6
    mc=1.9944235e-26
    echrg=1.609e-19
    kboltz=echrg
    kb=1.609e-19
    eps0=8.85418782e-12
    rzo=[1.5, 0, 0]
    
    nsurf = 150 # FSA surfaces
    bigr = numpy.zeros([nsurf])
    bigr[:] = -numpy.inf # max

    fsafilename = 'fsa'+step_number+'.npz'
    if os.path.exists(fsafilename):
        fsaDict = numpy.load(fsafilename)
        dvar = fsaDict['arr_0']
        yvars = fsaDict['arr_1']
        Gcontours = fsaDict['arr_2']
        bigr = fsaDict['arr_3']
    else:
        dvar, yvars, contours = FSA(eval_nimrod, rzo, basefsa, 8, nsurf=nsurf, \
                                    depvar='eta', dpow=0.5, rzx=[1.3, -1.14, 0])
        fsaArr = [dvar, yvars, contours, bigr]
        numpy.savez(fsafilename,*fsaArr)

    #import IPython; IPython.embed()
    
    # Determine where the FSA failed
    iend=-1
    while numpy.isnan(yvars[:,iend]).any():
        iend -= 1
    iend += yvars.shape[1]+1
    
    yrhon.append(interp1d(dvar[1,:iend], yvars[:,:iend], kind='cubic')(rhon))

ii=6
uc=1.e-3
pn.plot_scalar_line(None, uc*yrhon[0][ii], flabel=f"{time_list[0]:.4f}",
                    f2=uc*yrhon[1][ii], f2label=f"{time_list[1]:.4f}",
                    f3=uc*yrhon[2][ii], f3label=f"{time_list[2]:.4f}",
                    f4=uc*yrhon[3][ii], f4label=f"{time_list[3]:.4f}",
                    xvar=rhon, xlabel=r'\rho_N', ylabel=r'\Omega (kHz)',legend_loc='upper right')

ii=7
uc=1.
pn.plot_scalar_line(None, uc*yrhon[0][ii], flabel=f"{time_list[0]:.4f}",
                    f2=uc*yrhon[1][ii], f2label=f"{time_list[1]:.4f}",
                    f3=uc*yrhon[2][ii], f3label=f"{time_list[2]:.4f}",
                    f4=uc*yrhon[3][ii], f4label=f"{time_list[3]:.4f}",
                    xvar=rhon, xlabel=r'\rho_N', ylabel=r'K_{pol} (mT/s)',legend_loc='upper center')

ii=0
uc=1.
pn.plot_scalar_line(None, uc*yrhon[0][ii], flabel=f"{time_list[0]:.4f}",
                    f2=uc*yrhon[1][ii], f2label=f"{time_list[1]:.4f}",
                    f3=uc*yrhon[2][ii], f3label=f"{time_list[2]:.4f}",
                    f4=uc*yrhon[3][ii], f4label=f"{time_list[3]:.4f}",
                    xvar=rhon, xlabel=r'\rho_N', ylabel=r'n_{e0} (m^{-3})',legend_loc='upper right')

ii=1
uc=1.
pn.plot_scalar_line(None, uc*yrhon[0][ii], flabel=f"{time_list[0]:.4f}",
                    f2=uc*yrhon[1][ii], f2label=f"{time_list[1]:.4f}",
                    f3=uc*yrhon[2][ii], f3label=f"{time_list[2]:.4f}",
                    f4=uc*yrhon[3][ii], f4label=f"{time_list[3]:.4f}",
                    xvar=rhon, xlabel=r'\rho_N', ylabel=r'\tilde{n} (m^{-3})',legend_loc='upper left')

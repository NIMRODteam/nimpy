import numpy
from scipy.integrate import ode
from nimpy.plot_nimrod import PlotNimrod as pn
from nimpy.fsa import find_pf_null
from nimpy.field_class import Scalar,Vector

def particle_motion(eta, y, dy, eval_nimrod, mudm, chgtomass, gmt):
    '''
    Integrates a field line with a independent variable of eta
    :param eta: independent variable
    :param y: dependent variables (r, l, V', etc.)
    :param dy: pre-allocated space for dy for optimization
    :param eval_nimrod: eval_nimrod class instance
    :param mudm: mu over mass
    :param chgtomass: species charge to mass ratio
    :param gmt: True if in toroidal geometry
    :return dy: trajectory change in (R,Z,P,upar)
    '''
    fval = eval_nimrod.eval_field('b', y[0:3], dmode=1, eq=2)
    if numpy.isnan(fval).any():
        raise Exception('Hit wall')
    B = Vector(fval, y[0:3], torgeom=gmt, dmode=1)
    fval = eval_nimrod.eval_field('e', y[0:3], dmode=0, eq=2)
    E = Vector(fval, y[0:3], torgeom=gmt, dmode=0)
    bhat = B.hat()
    kappa = curl(bhat).cross(bhat) # k = - b x curl(b)
    omega = chgtomass*B.mod
    grdB = grad(B.mag())
    uperp = E.cross(bhat, dmode=0)/B.mag() \
            + (mudm/omega) * bhat.cross(grdB) \
            + y[3]**2/omega * bhat.cross(kappa)
    dy[0] = y[3]*bhat.r + uperp.r
    dy[1] = y[3]*bhat.z + uperp.z
    dy[2] = y[3]*bhat.p + uperp.p
    if gmt:
        dy[2]=dy[2]/y[0]
    dy[3] = chgtomass * (E.dot(bhat)).data - mudm * (bhat.dot(grdB)).data
    return dy

def particle(eval_nimrod, rzp, vp, chrgtomass, gmt, rzo, ncross=3):
    """
    Integrate single particle motion equations for a particle at rz
    with phase-space velocity vp (vpar, vperp).
    :param eval_nimrod: instance of eval nimrod class
    :param rzp: initial rzp location [r, z, phi]
    :param vp: initial phase-space velocity [vpar, vperp]
    :param chrgtomass: species electric charge to mass ratio
    :param gmt: True if in toroidal geometry
    :param rzo: o-point guess
    :return (trajectory, hit_wall, trapped): tuple
             trajectory - numpy array [rzp, npts] of size [3, npts]
             hit_wall - true if hit wall
             trapped - true if trapped (false if passing)
    """
    # find rz at the o-point
    rzo = find_pf_null(eval_nimrod, numpy.array(rzo))
    # to collect output
    max_segments=5000
    trajectory = numpy.zeros([4,max_segments+1])
    trajectory[0:3,0] = rzp[0:3]
    trajectory[3,0] = vp[0]
    # preallocate arrays used in integrand for optimization
    dy = numpy.zeros(4)
    # loop over surfaces and integrate
    hit_wall=False
    trapped=False
    rtol=1.e-11
    nsteps_max=5e4
    lsode = ode(particle_motion).set_integrator('lsoda', rtol=rtol, nsteps=nsteps_max)
    dt = 0.1/numpy.linalg.norm(vp)
    y0 = [rzp[0], rzp[1], rzp[2], vp[0]]
    b = eval_nimrod.eval_field('b', rzp, dmode=0, eq=2)
    mudm = vp[1]**2/(2*numpy.linalg.norm(b)) # mu/m
    lsode.set_initial_value(y0)
    lsode.set_f_params(dy, eval_nimrod, mudm, chrgtomass, gmt)
    sign_flips = 0
    etalast = numpy.arctan2(rzp[1]-rzo[1],rzp[0]-rzo[0]) 
    for isegment in range(max_segments):
        # collect contour locations during integration
        try:
            y = lsode.integrate(lsode.t+dt)
        except Exception:
            hit_wall=True
            print("particle: Hit wall at rz=", lsode.y[0], lsode.y[1])
            #import traceback
            #traceback.print_exc()
            break
        trajectory[:,isegment+1] = lsode.y
        eta = numpy.arctan2(lsode.y[1]-rzo[1],lsode.y[0]-rzo[0])
        #print(lsode.y,eta)
        if eta*etalast<0:
            sign_flips = sign_flips + 1
        etalast = eta
        if sign_flips > ncross or hit_wall:
            break
        if not lsode.successful():
            print("particle: Integration failed at rz=", lsode.y[0], lsode.y[1])
            break

    # Check for changes in sign of u_par to determine if trapped
    if not hit_wall:
        if numpy.sum(numpy.fabs(numpy.diff(numpy.sign(trajectory[3,:isegment+1])))) != 0.:
            trapped=True
    return (trajectory[:,:isegment+1], hit_wall, trapped)

import sympy
import sympy.vector
import numpy
import matplotlib.pyplot as pl
import argparse

class Singleton(type):
    '''
    This is a singleton class metaclass pattern
    '''
    _instances = {}
    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton,cls).__call__(*args, **kwargs)
        return cls._instances[cls]

def div(a):
    '''
    For convenience to reduce verbosity
    '''
    return sympy.vector.divergence(a)

def grad(a):
    '''
    For convenience to reduce verbosity
    '''
    return sympy.vector.gradient(a)

def curl(a):
    '''
    For convenience to reduce verbosity
    '''
    return sympy.vector.curl(a)


class WaveAnalysis(metaclass=Singleton):
    '''
    Perform a plane-wave analysis. Because this class defines symbols in the
    global namespace to avoid excessive namespacing when defining equations use
    the singleton instance. 
    '''

    def __init__(self):
        '''
        Basic initialization
        '''
        sympy.init_printing()
        self.conservative_form=False
        self.navier_stokes=False
        self.crank_nicolson=False
        self.print_output=False
        self.M=None
        self.L=None

    def create_matrices(self):
        '''
        Create the linearized matrix, M, and LHS matrix, L as sympy expressions.
        '''
        outstr = "Analyzing "
        if self.navier_stokes:
            outstr += "Navier-Stokes "
        else:
            outstr += "MHD "
        if self.conservative_form:
            outstr += "conservative-form "
        else:
            outstr += "primitive-form "
        outstr += "equation system with "
        if self.crank_nicolson:
            outstr += "Crank-Nicolson "
        else:
            outstr += "leap-frog "
        outstr += "time discretization..."
        print(outstr)
        self.__define_symbols()
        self.__get_lin_mat()
        self.__get_lhs_mat()
        return

    
    def __define_symbols(self):
        '''
        Define basic symbols.
        Do this globally to avoid excessive namespacing in functions.
        '''
        global rho0, rhop
        global V0, V0mag, vp
        global rhoV0, rhovp
        global Ve0, vep, jp
        global rhoVe0, rhovep
        global Te0, Ti0, tep, tip
        global B0, B0mag, bhat0, bp
        global mi, echrg, kb, mu0, Gamma
        global va, csi, cse, vnorm, di
        global eta, elecd, kdivb, nuperp, cgv, nupar, kperpi, kperpe, Dn
        global dt, theta_adv, theta_d, theta_si, theta_si_fl, theta_si_lap
        global N, k, spi, kmag, phik
        global delop
    
        kmag, B0mag, V0mag = sympy.symbols("kmag, B0mag, V0mag", positive=True, real=True)
        rho0, Ti0, Te0 = sympy.symbols("rho0 Ti0 Te0", positive=True, real=True)
        thetak, phik = sympy.symbols("theta_k phi_k", real=True)
        mi, echrg, kb, mu0, Gamma = sympy.symbols("mi echrg kb mu0 Gamma", positive=True, real=True)
        va, csi, cse, di = \
                sympy.symbols("va csi cse di", positive=True, real=True)
        eta, elecd, kdivb, nuperp, cgv, nupar, kperpi, kperpe, Dn = \
                sympy.symbols("eta elecd kdivb nu_perp c_gv nu_par k_perp_i k_perp_e D_n", positive=True, real=True)
        dt, theta_adv, theta_d, theta_si, theta_si_fl, theta_si_lap = \
                sympy.symbols("dt theta_adv theta_d theta_si theta_si_fl theta_si_lap", positive=True, real=True)
        
        N = sympy.vector.CoordSys3D('N')
        xv = N.x*N.i + N.y*N.j + N.z*N.k
        
        k = kmag*((N.i*sympy.sin(thetak) + N.j*sympy.cos(thetak))*sympy.sin(phik) + N.k*sympy.cos(phik))
        k = k.subs(thetak,0) # simplify
        sp = sympy.exp(sympy.I*k.dot(xv))
        spi = sympy.exp(-sympy.I*k.dot(xv))
        
        thetab, phib = sympy.symbols("theta_b phi_b", real=True)
        B0 = B0mag*((N.i*sympy.sin(thetab) + N.j*sympy.cos(thetab))*sympy.sin(phib) + N.k*sympy.cos(phib))
        B0 = B0.subs([(thetab,0),(phib,0)]) # simplify
        bhat0 = B0/B0mag
        bx, by, bz = sympy.symbols("bx by bz", real=True)
        bp = (bx*N.i + by*N.j + bz*N.k)*sp
        
        rho = sympy.symbols("rho", positive=True, real=True)
        rhop = rho*sp
        
        thetav, phiv = sympy.symbols("theta_v phi_v", real=True)
        V0 = V0mag*((N.i*sympy.sin(thetav) + N.j*sympy.cos(thetav))*sympy.sin(phiv) + N.k*sympy.cos(phiv))
        V0 = V0.subs([(thetav,0),(phiv,0)]) # simplify
        rhoV0 = rho0*V0
        vx, vy, vz = sympy.symbols("vx vy vz", real=True)
        rhovx, rhovy, rhovz = sympy.symbols("rhovx rhovy rhovz", real=True)
        if self.conservative_form:
            rhovp = (rhovx*N.i + rhovy*N.j + rhovz*N.k)*sp
            vp = rhovp/rho0 - rhop*V0/rho0
        else:
            vp = (vx*N.i + vy*N.j + vz*N.k)*sp
            rhovp = rho0*vp+rhop*V0
        vnorm = va
        if self.navier_stokes:
            vnorm = csi
        
        ti = sympy.symbols("ti", positive=True, real=True)
        tip = ti*sp
        te = sympy.symbols("te", positive=True, real=True)
        tep = te*sp
        
        # J0 is zero
        jp = curl(bp)/mu0
        vep = vp - jp/(rho0/mi*echrg)
        Ve0 = V0
        rhovep = vep*rho0+Ve0*rhop
        rhoVe0 = rho0*Ve0
     
        self.navier_stokes_subs = [(B0, 0), (bx, 0), (by, 0), (bz, 0), \
                                   (Te0, 0), (tep, 0)]
    
        # default subdict
        self.subdict={B0mag : sympy.sqrt(mu0*rho0)*va, \
                      Ti0 : mi/(kb*Gamma)*csi**2, \
                      Te0 : mi/(kb*Gamma)*cse**2, \
                      echrg :  sympy.sqrt(mi**2/(mu0*rho0))/di, \
                      eta : elecd*mu0, \
                      va : 1, csi: 0.05, cse : 0.07, V0mag : 0.1, \
                      di : 0., Gamma : 5./3., \
                      elecd : 0.e-7, kdivb : 1.e-3,  \
                      kperpi : 0.e-7, kperpe : 0.e-7, Dn : 0.e-8, \
                      nuperp : 0.e-7, cgv : 0., nupar : 0., \
                      dt : 0.1, theta_adv : 0.5, theta_d : 1.0, \
                      theta_si : 0.5, theta_si_fl : 0., theta_si_lap : 0.}
    
        delop = sympy.vector.Del()
        return
    
    def __process_equations(self, eqnlist):
        '''
        Process equations into a matrix form
        Input is eqnlist which is a list-of-lists where is list is
        eqnlist[0] = [expr, type, variable, normalization]
        where for example for continuity
        expr is div(rho*v)
        type si scalar (or vector)
        variable is rho
        normalization is rho0
        Returns the matrix
        '''
        eqnlist = [list(x) for x in zip(*eqnlist)]
        eqexpr = eqnlist[0]
        eqtype = eqnlist[1]
        eqvars0 = eqnlist[2]
        eqnorm0 = eqnlist[3]
    
        # remove e^(ikx) dependence and assemble by component
        eqns = []
        eqvars = []
        eqnorm = []
        for ieqn,eqn in enumerate(eqexpr):
            eqn = eqn*spi
            if self.navier_stokes:
                eqn = eqn.subs(self.navier_stokes_subs)
            if eqtype[ieqn] == 'vector':
                for unitvec in [N.i, N.j, N.k]:
                    eqns.append(eqn.dot(unitvec))
                    eqvars.append(eqvars0[ieqn].dot(unitvec)*spi)
                    eqnorm.append(eqnorm0[ieqn])
            else:
                eqns.append(eqn)
                eqvars.append(eqvars0[ieqn]*spi)
                eqnorm.append(eqnorm0[ieqn])
    
        mat = []
        for ieqv,eqvar in enumerate(eqvars):
            sublist = []
            for eqv in eqvars:
                if eqv is eqvar:
                    sublist.append((eqv,1))
                else:
                    sublist.append((eqv,0))
            row = []
            for ieqn,eqn in enumerate(eqns):
                entry = sympy.simplify(eqn.subs(sublist)*eqnorm[ieqv]/eqnorm[ieqn])
                row.append(entry)
            mat.append(row)
        mat = list(map(list, zip(*mat))) # transpose
        return sympy.Matrix(mat) 
    
    
    def __get_divten(self, theta_adv_=1, theta_d_=1, theta_gv_=1, crhop=1):
        '''
        Return divten tensor ( div ( rhovv + Pi) )
        The gyroviscous and advection terms may use different time centering that
        the perpendicular and parallel terms. This can be accounted for with the
        optional theta input variables.
        '''
        if self.conservative_form:
            advten = sympy.Matrix([[ \
                       (rhovp.dot(ii)*rhoV0.dot(ij)+rhoV0.dot(ii)*rhovp.dot(ij) \
                       -crhop*rhop*V0.dot(ii)*rhoV0.dot(ij))/rho0 \
                       for ii in [N.i,N.j,N.k]] \
                       for ij in [N.i,N.j,N.k]])
            grdrhovp = sympy.Matrix([[grad(rhovp.dot(ii)).dot(ij) \
                         for ii in [N.i,N.j,N.k]] \
                         for ij in [N.i,N.j,N.k]])
            cten = sympy.Matrix([[ \
                     (grad(crhop*rhop).dot(ii)*rhoV0.dot(ij) \
                     +grad(rho0).dot(ii)*rhovp.dot(ij) \
                     -grad(rho0).dot(ii)*rhoV0.dot(ij)*crhop*rhop/rho0)/rho0
                     for ii in [N.i,N.j,N.k]] \
                     for ij in [N.i,N.j,N.k]])
            rhowt = grdrhovp+grdrhovp.transpose() \
                    -cten-cten.transpose() \
                    -2/3*sympy.eye(3)*( \
                       div(rhovp) \
                      -(rhovp.dot(grad(rho0)) \
                       +rhoV0.dot(grad(crhop*rhop)) \
                       -crhop*rhop*rhoV0.dot(grad(rho0))/rho0)/rho0)
        else:
            advten = 0*sympy.eye(3)
            rhowt = rho0*sympy.Matrix([[grad(vp.dot(ii)).dot(ij) \
                       for ii in [N.i,N.j,N.k]] \
                       for ij in [N.i,N.j,N.k]])
        pi_perp = -nuperp*rhowt
        if self.navier_stokes:
            pi_gv = 0*sympy.eye(3)
            pi_par = 0*sympy.eye(3)
        else:
            bhat0m = bhat0.to_matrix(N)
            eye = [[1,0,0], [0,1,0], [0,0,1]]
            bcrgvibb = sympy.Matrix([[sum([sum([sum([
                              sympy.LeviCivita(ii,ij,ik)*bhat0m[ij,0]*rhowt[ik,il] \
                                *(eye[il][im]+3*bhat0m[il,0]*bhat0m[im,0]) \
                              for ik in range(3)]) \
                              for ij in range(3)]) \
                              for il in range(3)]) \
                              for ii in range(3)] \
                              for im in range(3)])
            pi_gv = -cgv*kb*Ti0/(4*echrg*B0mag)*(bcrgvibb-bcrgvibb.T)
            brhowtb = sum(sum(bhat0m[ii,0]*rhowt[ii,ij]*bhat0m[ij,0] \
                        for ii in range(3)) \
                        for ij in range(3))
            pi_par = -nupar \
                     *( bhat0.outer(bhat0).to_matrix(N) - sympy.eye(3)/3 ) \
                     *3/2*brhowtb
        divten = sympy.vector.matrix_to_vector( \
                sympy.tensorcontraction( \
                sympy.derive_by_array(theta_adv_*advten+theta_d_*pi_perp \
                                      +theta_gv_*pi_gv+theta_d_*pi_par, 
                                      (N.x, N.y, N.z)), (0, 1)), N)
        return divten
    
    def __get_lin_mat(self):
        '''
        return a linearized matrix of the full MHD equations
        du/dt = M u
        '''
        
        print("Computing the linearized RHS matrix of equations...")
        
        eqnlist = []
        if self.conservative_form:
            divten = self.__get_divten()
            veqn = jp.cross(B0) \
                   -grad(kb*(Ti0+Te0)*rhop+kb*(tip+tep)*rho0)/mi \
                   -divten
            eqnlist.append([veqn, 'vector', rhovp, rho0*vnorm])
        else:
            divten = self.__get_divten(theta_adv_=0)
            veqn = (-rho0*vp.dot(delop)(V0) \
                    -rho0*V0.dot(delop)(vp) \
                    -rhop*V0.dot(delop)(V0) \
                    +jp.cross(B0) \
                    -grad(kb*(Ti0+Te0)*rhop+kb*(tip+tep)*rho0)/mi \
                    -divten \
                   )/rho0
            eqnlist.append([veqn, 'vector', vp, vnorm])
    
        rhoeqn = div( \
                   -rhovp \
                   +Dn*grad(rhop)
                 )
        eqnlist.append([rhoeqn, 'scalar', rhop, rho0])
    
        tieqn = ( -rhovp.dot(delop)(Ti0) \
                  -rhoV0.dot(delop)(tip) \
                  -(Gamma-1)*rho0*Ti0*div(vp) \
                  -(Gamma-1)*rhop*Ti0*div(V0) \
                  -(Gamma-1)*rho0*tip*div(V0) \
                  +(Gamma-1)*div(rho0*kperpi*grad(tip)) \
                  +(Gamma-1)*div(rhop*kperpi*grad(Ti0))
                )/rho0
        eqnlist.append([tieqn, 'scalar', tip, Ti0])
    
        if not self.navier_stokes:
            indeqn = curl( \
                       +vep.cross(B0) \
                       +Ve0.cross(bp) \
                       # drop tep term; Curl(Grad)=0
                       +grad(rhop*kb*Te0)/(rho0*echrg) \
                       -eta*jp \
                      ) \
                      +grad(kdivb*div(bp))
            eqnlist.append([indeqn, 'vector', bp, B0mag])
    
    
            teeqn = ( -rhovep.dot(delop)(Te0) \
                      -rhoVe0.dot(delop)(tep) \
                      -(Gamma-1)*rho0*Te0*div(vep) \
                      -(Gamma-1)*rhop*Te0*div(Ve0) \
                      -(Gamma-1)*rho0*tep*div(Ve0) \
                      +(Gamma-1)*div(rho0*kperpe*grad(tep)) \
                      +(Gamma-1)*div(rhop*kperpe*grad(Te0))
                    )/rho0
            eqnlist.append([teeqn, 'scalar', tep, Te0])
           
        self.M = self.__process_equations(eqnlist)
        if (self.print_output):
            sympy.pprint(self.M)
        return
    
    def __get_lhs_mat(self):
        '''
        return a matrix of the LHS that captures the time discretization effects
        the full equation set is then:
          L du = dt M u 
        where
          L is the result of this routine
          M is the result of get_lin_mat
        '''
    
        print("Computing the linearized LHS matrix of equations...")
    
        if self.crank_nicolson:
            theta_cn = 0.5
            theta = 0.5
        else: # leap-frog
            theta_cn = 0.
            theta = 1.

        eqnlist = []
        veqn = dt*(-theta_cn*jp.cross(B0) \
                   +theta_cn*grad(kb*(Ti0+Te0)*rhop+kb*(tip+tep)*rho0)/mi)
        if self.conservative_form:
            divten = self.__get_divten(theta_adv_=theta_adv, theta_d_=theta_d, \
                                       theta_gv_=theta_adv, crhop=0)
            veqn = rhovp + veqn + dt* \
                   ( divten \
                     -(theta_si**2)*dt*( \
                        curl(curl(rhovp.cross(B0))).cross(B0)/mu0 \
                       +grad(Gamma*kb*(Ti0+Te0)*rho0/mi*div(rhovp)) \
                       # Drop the J0 and grad(P0) terms
                     )/rho0 \
                     +(theta_si_lap**2)*dt*csi**2*( \
                        curl(curl(rhovp))-grad(div(rhovp)) \
                     ) \
                   )
            eqnlist.append([veqn, 'vector', rhovp, rho0*vnorm])
        else:
            divpi = self.__get_divten(theta_adv_=0, theta_d_=theta_d, theta_gv_=theta_adv)
            veqn = vp + veqn/rho0 + dt* \
                   ( +theta_adv*rho0*V0.dot(delop)(vp) \
                     +theta_adv*rho0*vp.dot(delop)(V0) \
                     +divpi \
                     -(theta_si**2)*dt*(
                        curl(curl(vp.cross(B0))).cross(B0)/mu0 \
                       +grad(Gamma*kb*(Ti0+Te0)*rho0/mi*div(vp)) \
                        # Drop the J0 and grad(P0) terms
                     )
                     +(theta_si_lap**2)*dt*rho0*csi**2*( \
                        curl(curl(vp))-grad(div(vp)) \
                     ) \
                   )/rho0
            eqnlist.append([veqn, 'vector', vp, vnorm])       

        if self.conservative_form:
            rhovp_lhs = theta*rhovp
        else:
            rhovp_lhs = theta_adv*V0*rhop + theta*vp*rho0
        rhoeqn = rhop + dt* \
                 div( \
                   +rhovp_lhs
                   -theta_d*Dn*grad(rhop) \
                   # SI op
                   +(theta_si_fl**2)*dt*( \
                     V0*V0.dot(grad(rhop))) \
                 )
        eqnlist.append([rhoeqn, 'scalar', rhop, rho0])

        if self.conservative_form:
            rhovp_lhs = theta*rhovp
            vp_lhs = (theta*rhovp - 0.5*V0*rhop)/rho0
        else:
            rhovp_lhs = 0.5*V0*rhop + theta*vp*rho0
            vp_lhs = theta*vp
        tieqn = tip + dt* \
                ( +theta_adv*rhoV0.dot(delop)(tip) \
                  +theta_adv*(Gamma-1)*rho0*tip*div(V0) \
                  -theta_d*(Gamma-1)*div(rho0*kperpi*grad(tip)) \
                  # leap-frog/CN/time-centered 
                  +rhovp_lhs.dot(delop)(Ti0) \
                  +(Gamma-1)*rho0*Ti0*div(vp_lhs) \
                  +0.5*(Gamma-1)*rhop*Ti0*div(V0) \
                  +0.5*(Gamma-1)*div(rhop*kperpi*grad(Ti0)) \
                  # SI op
                  +(theta_si_fl**2)*dt*div(rho0*V0*V0.dot(grad(tip))) \
                )/rho0
        eqnlist.append([tieqn, 'scalar', tip, Ti0])
    
        if not self.navier_stokes:
            indeqn = bp + dt*curl( \
                       # should be theta_b not theta_adv
                       -theta_adv*Ve0.cross(bp) \
                       +0.5*jp.cross(B0)*mi/(rho0*echrg) \
                       +theta_d*eta*jp \
                       # leap-frog/CN/time-centered
                       -vp_lhs.cross(B0) \
                       -0.5*grad(rhop*kb*Te0)/(rho0*echrg) \
                       # SI op
                       +(theta_si_fl**2)*dt*(curl(V0.cross(bp))).cross(V0)
                     ) \
                     -dt*grad(kdivb*div(bp))
            eqnlist.append([indeqn, 'vector', bp, B0mag])

            if self.conservative_form:
                rhovep_lhs = theta*rhovp - 0.5*mi*jp/echrg
                vep_lhs = (theta*rhovp - 0.5*V0*rhop - 0.5*mi*jp/echrg)/rho0
            else:
                rhovep_lhs = theta*vp*rho0 + 0.5*V0*rhop - 0.5*mi*jp/echrg
                vep_lhs = theta*vp - 0.5*mi*jp/echrg/rho0
            teeqn = tep + dt* \
                    ( +theta_adv*rhoVe0.dot(delop)(tep) \
                      +theta_adv*(Gamma-1)*rho0*tep*div(Ve0) \
                      -theta_d*(Gamma-1)*div(rho0*kperpe*grad(tep)) \
                      # leap-frog/CN/time-centered 
                      +rhovep_lhs.dot(delop)(Te0) \
                      +(Gamma-1)*rho0*Te0*div(vep_lhs) \
                      +0.5*(Gamma-1)*rhop*Te0*div(Ve0) \
                      +0.5*(Gamma-1)*div(rhop*kperpe*grad(Te0)) \
                      # SI op
                      +(theta_si_fl**2)*dt*div(rho0*V0*V0.dot(grad(tep))) \
                    )/rho0
            eqnlist.append([teeqn, 'scalar', tep, Te0])
           
        self.L = self.__process_equations(eqnlist)
        if (self.print_output):
            sympy.pprint(self.L)
        return
    
    def plot_dispersion(self, kmin=1, kmax=1000, omegamin=None, omegamax=None, outfile=None):
        '''
        Plot omega versus k for 3 values of phi_k: 0.03, pi/3, 0.97*pi/2
        Solve the exact system: du = dt M u
        and the time-discretized von Neumann analysis: L du = dt M u
        '''
    
        print("Lambdifying expressions...")
        
        dt_ = self.subdict[dt]
        di_ = self.subdict[di]
        phiarr=[0.03, numpy.pi/3.0, 0.97*numpy.pi/2.0]
        sublist = [(k, v) for k, v in self.subdict.items()] 
        # orders list for substitutions within substitutions
        #sympy.utilities.iterables.topological_sort(sublist, sympy.utilities.iterables.default_sort_key)
        M = sympy.simplify(self.M.subs(sublist))
        L = sympy.simplify(self.L.subs(sublist))
        lam_M = sympy.lambdify([kmag,phik], M, "numpy")
        lam_L = sympy.lambdify([kmag,phik], L, "numpy")
    
        print("Calculating the dispersion relation...")
    
        npts = int(10*(numpy.log10(kmax)-numpy.log10(kmin)) + 1)
        karr = numpy.logspace(numpy.log10(kmin), numpy.log10(kmax), num=npts, dtype=numpy.float64)
        csi_ = self.subdict[csi]
        cse_ = self.subdict[cse]
        va_ = self.subdict[va]
        if self.navier_stokes:
            va_=0
        if omegamin == None:
            omegamin = 0.95*numpy.sqrt(csi_**2+cse_**2)*kmin/10.
        if omegamax == None:
            omegamax = 1.05*numpy.sqrt(csi_**2+cse_**2+va_**2)*kmax
        fig, axs = pl.subplots(figsize=[24,12], ncols=4, nrows=len(phiarr))
        fig.suptitle('Dispersion relation, x analytic, + von Neumann', fontsize=16)
        omega   = numpy.zeros([npts, M.rows], dtype=numpy.complex128)
        omegadt = numpy.zeros([npts, M.rows], dtype=numpy.complex128)
        for iphi,phi in enumerate(phiarr):
            for ik,kval in enumerate(karr):
                # This is the "exact" dispersion relation without time discretization
                # du/dt = M.u 
                # u = u_0 e^{i\omega t}
                # i \omega u = M.u
                # \omega = -i eigval(M)
                Mmat = lam_M(kval,phi)
                eigenv = -1j*numpy.linalg.eigvals(Mmat)
                eigenv = numpy.sort_complex(eigenv)
                omega[ik,:] = eigenv[:]
                # This is a Von-Neumann analysis to include time discretization
                # L.du = dt M.u
                # u^{j+1} = dt L^{-1}.M.u^j + u^j = (dt L^{-1}.M + I) u^j
                # \lambda is the Von-Neumann stability parameter, \lambda = e^{i \omega dt}
                # u^{j+1} = \lambda u^j => \lambda = eigval(dt L^{-1}.M + I)
                # \omega = -i log(\lambda)/dt
                Lmat = lam_L(kval,phi)
                matinv = dt_*numpy.dot(numpy.linalg.inv(Lmat),Mmat)
                eigenv = numpy.linalg.eigvals(matinv + numpy.identity(M.rows)) # lambda
                eigenv = -1j*numpy.log(eigenv)/dt_  # omega
                omegadt[ik,:] = numpy.sort_complex(eigenv)
            lsan=''
            lsdt=''
            msan='x'
            msdt='+'
            # Positive real omega
            ax = axs[iphi,0]
            ax.loglog(karr, numpy.real(omega), linestyle=lsan, marker=msan)
            ax.set_prop_cycle(None)
            ax.loglog(karr, numpy.real(omegadt), linestyle=lsdt, marker=msdt)
            ax.set(ylabel='Re(omega)')
            ax.text(0.05, 0.95, 'phik='+f"{phi:.4f}", transform=ax.transAxes, fontsize=14,
                    verticalalignment='top')
            # Negative real omega
            ax = axs[iphi,1]
            ax.loglog(karr, -numpy.real(omega), linestyle=lsan, marker=msan)
            ax.set_prop_cycle(None)
            ax.loglog(karr, -numpy.real(omegadt), linestyle=lsdt, marker=msdt)
            ax.set(ylabel='-Re(omega)')
            # Positive imaginary omega
            ax = axs[iphi,2]
            ax.loglog(karr, numpy.imag(omega), linestyle=lsan, marker=msan)
            ax.set_prop_cycle(None)
            ax.loglog(karr, numpy.imag(omegadt), linestyle=lsdt, marker=msdt)
            ax.set(ylabel='Im(omega)') 
            # Negative imaginary omega
            ax = axs[iphi,3]
            ax.loglog(karr, -numpy.imag(omega), linestyle=lsan, marker=msan)
            ax.set_prop_cycle(None)
            ax.loglog(karr, -numpy.imag(omegadt), linestyle=lsdt, marker=msdt)
            ax.set(ylabel='-Im(omega)') 
        # general plot settings
        thoff=1.1
        dthline=2*numpy.pi/dt_
        tvoff=0.8
        if di_>0:
            divline=2*numpy.pi/di_
        for ax in axs.reshape(-1):
            ax.axhline(y=dthline,color='k')
            ax.annotate('2pi/dt',xy=(kmin,thoff*dthline),fontsize=16)
            if di_>0:
                ax.axvline(x=divline,color='k')
                ax.annotate('2pi/di',xy=(divline,tvoff),fontsize=16)
            ax.set_ylim(omegamin, omegamax)
        for ax in axs[-1,:]:
            ax.set(xlabel='wavenumber')
    
        pl.tight_layout()
        if outfile:
            pl.savefig(outfile)
        else:
            pl.show()

def main():
    '''
    Example of usage
    '''
    parser = argparse.ArgumentParser(description='Perform a Von Neumann plane wave analysis.')
    parser.add_argument('-c', '--conservative', action="store_true", \
                        help='Use the conservative form of the equations for rho and rhov')
    parser.add_argument('-i', '--interactive', action="store_true", \
                        help='Drop to interactive mode')
    parser.add_argument('-n', '--navierstokes', action="store_true", \
                        help='Do Navier-Stokes equations instead of MHD')
    parser.add_argument('-p', '--printoutput', action="store_true", \
                        help='Print symbolic matrices')
    parser.add_argument('-s', '--simultaneous', action="store_true", \
                        help='Use simultaneous (Crank-Nicolson) time centering')
    parser.add_argument('-f', '--file', default=None, \
                        help='Save dispersion plot to file')
    args = parser.parse_args()

    waveanalysis = WaveAnalysis()
    # Must call create_matrices to change these
    waveanalysis.conservative_form = args.conservative
    waveanalysis.navier_stokes = args.navierstokes
    waveanalysis.print_output = args.printoutput
    waveanalysis.crank_nicolson = args.simultaneous
    waveanalysis.create_matrices()
    # Can modify subdict values and plot on the fly without recreating matrices
    if waveanalysis.crank_nicolson:
        waveanalysis.subdict[theta_si] = 0
    if waveanalysis.navier_stokes:
        waveanalysis.subdict[va] = 0
    #waveanalysis.subdict[nuperp] = 1.e-3
    waveanalysis.plot_dispersion(kmin=0.1, kmax=1000., outfile=args.file)

    if args.interactive:
        import readline # optional, will allow Up/Down/History in the console
        import code
        variables = globals().copy()
        variables.update(locals())
        shell = code.InteractiveConsole(variables)
        shell.interact()

if __name__ == "__main__":
    main()


#!/usr/bin/env python3

import f90nml,os,h5py
import matplotlib.pyplot as pl
from nimpy.plot_nimrod import PlotNimrod as pn
from nimpy.eval_nimrod import EvalNimrod
from nimpy.fsa import find_pf_null
from particle import *

# load dump file
dumpfile = 'dumpgll.00000.h5'
nml = f90nml.read('nimrod.in')
gmt = nml['grid_input']['geom']
if gmt == 'tor':
    gmt=True
else:
    gmt=False
eval_nimrod = EvalNimrod(dumpfile, fieldlist='nvptbje') #, coord='xyz')

# read contours.h5
confile = h5py.File('contours.h5','r')
RZsol = confile['/sol/points']
RZsep = confile['/LCFS/points']
RZwall = confile['/wall/points']
RZwall = numpy.vstack([RZwall,RZwall[0,:]])

def setOrbitPlot():
    fig, ax = pl.subplots()
    fig.set_size_inches(8,12)
    ax.plot(RZsol[:,0],RZsol[:,1],color='k')
    ax.plot(RZsep[:,0],RZsep[:,1],color='k')
    ax.plot(RZwall[:,0],RZwall[:,1],color='k')
    ax.set(ylabel="Z (m)", xlabel="R (m)")
    ax.ticklabel_format(axis='both', style='sci', scilimits=(10**3,10**-3),
                        useOffset=None, useLocale=None, useMathText=True)
    pl.tight_layout()
    # find o-point
    rzo = find_pf_null(eval_nimrod, [1.7, -0.2, 0])
    ax.plot(rzo[0],rzo[1],'o')

md=3.3435860e-27
echrg=1.609e-19
kb=1.609e-19

# RZ sweep
#rz = numpy.array([1.8, -0.013, 0])
#ti = eval_nimrod.eval_field('ti', rz, dmode=0, eq=1)
#vti = numpy.sqrt(kb*ti/md)
#vp = [2.,5.0]*vti
#print(vp)
#trajectories=[]
#for rr in [1.8,1.9,2.0,2.1,2.2, 2.3]:
#    rz[0]=rr
#    trajectory, hit_wall, trapped = particle(eval_nimrod, rz, vp, echrg/md, gmt, [1.7, -0.2, 0])
#    print(rr,trapped)
#    trajectories.append(trajectory)
#for trajectory in trajectories:
#    pl.plot(trajectory[0,:],trajectory[1,:])
#pl.show()

# Scan phase space at a point
nsp = 32
nxi = 32
# these points are a sets of 4 flux surfaces at 5 poloidal locations
# surface     : 1 (0-4)       2 (5-9)      3 (10-14)     5 (15-19)
# psin        : 0.97112792,   0.98413813,   0.99363061,   0.99897349
# rhon        : 0.96094922,   0.97718249,   0.98997539,   0.99792883
# q           :-4.85565425,  -5.20494633,  -5.84540093,  -7.16950079
rzpts1 = [[ 1.85905099e+00, -8.23673307e-01, 0.0],
          [ 2.19655108e+00, -3.78398582e-01, 0.0],
          [ 2.29082974e+00,  1.10439078e-03, 0.0],
          [ 2.18110744e+00,  3.80565384e-01, 0.0],
          [ 1.81425555e+00,  8.39506629e-01, 0.0]]
rzpts2 = [[ 1.85986154e+00, -8.32531353e-01, 0.0],
          [ 2.19983320e+00, -3.81309699e-01, 0.0],
          [ 2.29516249e+00,  1.22755242e-03, 0.0],
          [ 2.18428003e+00,  3.83719014e-01, 0.0],
          [ 1.81459873e+00,  8.49474743e-01, 0.0]]
rzpts3 = [[ 1.86044660e+00, -8.38925244e-01, 0.0],
          [ 2.20219956e+00, -3.83408564e-01, 0.0],
          [ 2.29828715e+00,  1.31637309e-03, 0.0],
          [ 2.18656830e+00,  3.85993600e-01, 0.0],
          [ 1.81484622e+00,  8.56663776e-01, 0.0]]
rzpts4 = [[ 1.86077468e+00, -8.42510607e-01, 0.0],
          [ 2.20352279e+00, -3.84582222e-01, 0.0],
          [ 2.30003535e+00,  1.36606712e-03, 0.0],
          [ 2.18784815e+00,  3.87265802e-01, 0.0],
          [ 1.81498508e+00,  8.60696959e-01, 0.0]]
ifile=0
rzpts=rzpts1
#ifile=5
#rzpts=rzpts2
#ifile=10
#rzpts=rzpts3
#ifile=15
#rzpts=rzpts4
#ifile=0
#rzpts=rzpts1+rzpts2+rzpts3+rzpts4

rzpts=[[ 2.29516249e+00,  1.22755242e-03, 0.0]]
nvx=128
nvy=64
nsp=nvx
nxi=nvy

for rz in rzpts:
    if os.path.exists('trajectories'+str(ifile)+'.npz'):
        fileread = numpy.load('trajectories'+str(ifile)+'.npz')
        trajectories = []
        for trajectory in fileread:
            trajectories.append(fileread[trajectory])
        output = numpy.load('particles_map'+str(ifile)+'.npy')
    else:
        rz = numpy.array(rz)
        ti = eval_nimrod.eval_field('ti', rz, dmode=0, eq=2)
        vti = numpy.sqrt(kb*ti[0]/md)
        vsmin = 0.1*vti
        vsmax = 5.*vti
        output = numpy.zeros([3,nsp,nxi])
        trajectories=[]
        #for ixi in range(nxi):
            #xi = float(ixi)*numpy.pi/float(nxi-1) - numpy.pi/2.0
            #vpunit = numpy.array([numpy.sin(xi), numpy.cos(xi)])
            #for isp in range(nsp):
                #sp = float(isp)*(vsmax-vsmin)/float(nsp-1) + vsmin
                #vp = sp*vpunit
        for ivx in range(nvx):
            for ivy in range(nvy):
                isp=ivx
                ixi=ivy
                vp = numpy.array([2*vsmax*(ivx-nvx/2)/nvx, vsmax*(ivy/nvy)])
                
                if (numpy.all(vp == 0)):
                    output[0:2,isp,ixi] = vp[0:2]/vti
                    output[2,isp,ixi] = 1.
                else:
                    trajectory, hit_wall, trapped = \
                      particle(eval_nimrod, rz, vp, echrg/md, gmt, [1.7, -0.2, 0])
                    print(vp,trapped,hit_wall)
                    output[0:2,isp,ixi] = vp[0:2]/vti
                    if hit_wall:
                        output[2,isp,ixi] = 2.
                    elif trapped:
                        output[2,isp,ixi] = 1.
                    else:
                        output[2,isp,ixi] = 0.
                    trajectories.append(trajectory)
        numpy.savez('trajectories'+str(ifile)+'.npz',*trajectories)
        numpy.save('particles_map'+str(ifile)+'.npy',output)

    # plot trajectories
    outline = numpy.zeros([2,nxi])
    setOrbitPlot()
    for ixi in range(nxi):
        for isp in range(nsp):
            if isp==nsp/2:
                outline[0,ixi]=output[0,isp,ixi]
                outline[1,ixi]=output[1,isp,ixi]
                pl.plot(trajectories[ixi*nsp+isp][0,:],trajectories[ixi*nsp+isp][1,:])
    pl.savefig("trajectory"+str(ifile)+".png")

    # plot phase space
    fig, ax = pl.subplots()
    fig.set_size_inches(14,8)
    ax.set(ylabel=r"$v_\perp / v_{Ti}$", xlabel=r"$v_\parallel / v_{Ti}$")
    ax.scatter(output[0], output[1], s=5.*(output[0]**2+output[1]**2),
               c=output[2], cmap='brg')
    #ax.plot(outline[0],outline[1])
    # define color map
#    import matplotlib.colors as colors
#    cmap = colors.ListedColormap(['tab:purple','tab:orange','tab:green','tab:red'])
#    bounds = [-0.1, 0.5, 1.5, 2.5, 3.1]
#    norm = colors.BoundaryNorm(bounds, cmap.N)
#    for ixi in range(nxi):
#        for isp in range(nsp):
#            if output[0,isp,ixi] > 0 and output[2,isp,ixi] == 1:
#                output[2,isp,ixi]=3.
#    ax.imshow(numpy.transpose(output[2]), interpolation=None,
#              cmap=cmap, norm=norm, origin='lower',
#              extent=[output[0,0,0],output[0,-1,-1],output[1,0,0],output[1,-1,-1]])
    pl.tight_layout()
    pl.savefig("phaseSpace"+str(ifile)+".png")

    ifile = ifile + 1

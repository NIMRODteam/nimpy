#!/usr/bin/env python
import h5py, argparse, re
import numpy as np
import f90nml

def setEqFields(field, nxbl, nybl, in_nmodes, infile, outfile):
  '''
    Loop over eq fields from infile and set them in outfile + n=0 part
  '''
  print(field)
  try:
    bldat = infile['/rblocks/0001/'+field[0]+'0001']
  except:
    print('NOT FOUND')
    return # skip variable if it does not exist

  for ixbl in np.arange(nxbl):
    for iybl in np.arange(nybl):
      ib = ixbl*nybl+iybl+1
      blstr = str(ib)
      while(len(blstr) < 4):
        blstr='0'+blstr
      in_eqfld = infile['/rblocks/'+blstr+'/'+field[0]+blstr]
      in_refld = infile['/rblocks/'+blstr+'/re'+field[1]+blstr]
      in_imfld = infile['/rblocks/'+blstr+'/im'+field[1]+blstr]

      out_eqfld = outfile.create_dataset('/rblocks/'+blstr+'/'+field[0]+blstr,
                                         in_eqfld.shape, dtype='float64')
      nqty = in_eqfld.shape[2]
      out_eqfld[:,:,:] = in_eqfld + in_refld[:,:,0:nqty]

      out_eqfld.attrs.create("vsMesh",'/rblocks/'+blstr+'/rz'+blstr)
      out_eqfld.attrs.create("vsType","variable")
      
def setPertFields(field, nxbl, nybl, in_nmodes, nmin, nmax, infile, outfile):
  '''
    Copy appropriate part of perturbation fields
  '''
  try:
    bldat = infile['/rblocks/0001/re'+field+'0001']
  except:
    return # skip variable if it does not exist

  for ixbl in np.arange(nxbl):
    for iybl in np.arange(nybl):
      ib = ixbl*nybl+iybl+1
      blstr = str(ib)
      while(len(blstr) < 4):
        blstr='0'+blstr
      in_refld = infile['/rblocks/'+blstr+'/re'+field+blstr]
      in_imfld = infile['/rblocks/'+blstr+'/im'+field+blstr]
      print(in_refld.shape[2],in_nmodes)
      nqty = int(in_refld.shape[2]/in_nmodes)

      outshape = list(in_refld.shape)
      if nmin > -1:
        outshape[2] = (nmax-nmin+1)*nqty
      print(nqty,outshape)

      out_refld = \
        outfile.create_dataset('/rblocks/'+blstr+'/re'+field+blstr, outshape,
                               dtype='float64')
      out_imfld = \
        outfile.create_dataset('/rblocks/'+blstr+'/im'+field+blstr, outshape,
                               dtype='float64')

      if nmin > -1:
        print(nmin*nqty+1,(nmax+1)*nqty+1)
        out_refld[:,:,:] = in_refld[:,:,nmin*nqty+1:(nmax+1)*nqty+1]
        out_imfld[:,:,:] = in_imfld[:,:,nmin*nqty+1:(nmax+1)*nqty+1]
      else: 
        out_refld[:,:,:] = in_refld[:,:,:]
        out_imfld[:,:,:] = in_imfld[:,:,:]
      out_refld.attrs.create("vsMesh",'/rblocks/'+blstr+'/rz'+blstr)
      out_refld.attrs.create("vsType","variable")
      out_imfld.attrs.create("vsMesh",'/rblocks/'+blstr+'/rz'+blstr)
      out_imfld.attrs.create("vsType","variable")

  return

def main():
  '''
    Transfer n=0 to eq and set for linear run"
  '''
  parser = argparse.ArgumentParser(
                  prog="nonlin_to_linear.py",
                  description="transfer n=0 to eq and set for linear run",
                  usage="nonlin_to_linear.py [options] h5 file"
                  )
  parser.add_argument('-n', '--nmin', dest='nmin', type=int,
                      help='Minimum mode number', default=-1)
  parser.add_argument('-x', '--nmax', dest='nmax', type=int,
                      help='Maximum mode number', default=-1)
  parser.add_argument('file', metavar='dump file',
                      help='File to process')
  args = vars(parser.parse_args())
  if not args['file']:
    parser.print_usage()
    return
  else:
    infilename=args['file']

  reset_nmodes = False
  nmin = args['nmin']
  nmax = args['nmax']
  if args['nmin'] > -1 or args['nmax'] > -1:
    reset_nmodes = True
    if not (args['nmin'] > -1 and args['nmax'] > -1):
      print('Both nmin and nmax must be specified, not one')
      parser.print_usage()
      return

  infile = h5py.File(infilename, 'r')
  outfilename = re.sub(r'.h5', '.init.h5', infilename)
  outfile = h5py.File(outfilename, 'w')
  outfile.attrs.create("nbl", infile.attrs.get("nbl"))
  outfile.attrs.create("nrbl", infile.attrs.get("nrbl"))
  outfile.attrs.create("poly_degree", infile.attrs.get("poly_degree"))
  inTimeGroup = infile["/dumpTime"]
  outTimeGroup = outfile.create_group("/dumpTime")
  outTimeGroup.attrs.create("vsStep", inTimeGroup.attrs.get("vsStep"))
  outTimeGroup.attrs.create("vsTime", inTimeGroup.attrs.get("vsTime"))
  outTimeGroup.attrs.create("vsType", inTimeGroup.attrs.get("vsType"))
  infile.copy('/seams', outfile)
  nml = f90nml.read('nimrod.in')
  # set keff with new mode number range
  per_length = nml['grid_input'].get('per_length',1.)
  geom = nml['grid_input'].get('geom','tor')
  zperiod = nml['grid_input'].get('zperiod',1)
  in_keff = infile["/keff"] 
  in_nmodes = infile.attrs.get("nmodes",[1])[0]
  if args['nmin'] > in_nmodes or args['nmax'] > in_nmodes:
      print('nmin and nmax must be less than nmodes')
      parser.print_usage()
      return
  if reset_nmodes:
    nmodes = args['nmax'] - args['nmin'] + 1
    outfile.attrs.create("nmodes", nmodes)
    keff = outfile.create_dataset('/keff', (nmodes), dtype='float64')
    keff[:] = np.arange(args['nmin'],args['nmax'] + 1)
    print(keff)
    if (geom == 'lin'):
      keff[:] *= 2*np.pi/per_length
    keff[:] *= zperiod
    print(keff)
  else:
    outfile.attrs.create("nmodes", in_nmodes)
    infile.copy('/keff', outfile)
  nxbl = nml['grid_input'].get('nxbl',1)
  nybl = nml['grid_input'].get('nybl',1)

  dump_ja = nml['output_input'].get('dump_ja',False)
  if not dump_ja:
      print('Script requires dump_ja=T')
      parser.print_usage()
      return

  # create a list of eq and pert field pairs
  field_pairs = [['bq', 'be'], ['jq', 'ja'], ['eq', 'ee'],
                 ['nq', 'nd'], ['vq', 've'], ['pq', 'pr'], 
                 ['peq', 'pe']]
  pert_only_fields = ['conc', 'te', 'ti']
  copy_2D_fields = ['rz', 'diff', 'psi_eq']

  for field_pair in field_pairs:
    setEqFields(field_pair, nxbl, nybl, in_nmodes, infile, outfile)
    setPertFields(field_pair[1], nxbl, nybl, in_nmodes, nmin, nmax, infile,
                  outfile)
  for field in pert_only_fields:
    setPertFields(field, nxbl, nybl, in_nmodes, nmin, nmax, infile, outfile)

  for ixbl in np.arange(nxbl):
    for iybl in np.arange(nybl):
      ib = ixbl*nybl+iybl+1
      blstr = str(ib)
      while(len(blstr) < 4):
        blstr='0'+blstr
      grp = '/rblocks/'+blstr
      igrp = infile[grp]
      outfile[grp].attrs.create("degenerate", igrp.attrs.get("degenerate"))
      outfile[grp].attrs.create("id", igrp.attrs.get("id"))
      outfile[grp].attrs.create("mx", igrp.attrs.get("mx"))
      outfile[grp].attrs.create("mxe", igrp.attrs.get("mxe"))
      outfile[grp].attrs.create("my", igrp.attrs.get("my"))
      outfile[grp].attrs.create("nfour", nmodes)
      outfile[grp].attrs.create("poly_degree", igrp.attrs.get("poly_degree"))
      for field in copy_2D_fields:
        igrp.copy(field+blstr, outfile[grp])

  return

if __name__ == "__main__":
        main()

from nimpy.plot_nimrod import PlotNimrod as pn
import f90nml, optparse
from nimpy.eval_nimrod import *
from nimpy.field_class import *
import matplotlib.pyplot as pl
from matplotlib import gridspec

### Plot profiles ###
def plot_neutral_eqn(files, grid, eval_grid, gmt, nml):

    ne = {}
    nd = {}
    ndn = {}
    rhovn = {}
    ti = {}
    te = {}
    tn = {}
    prn = {}

    kb=1.60217733e-19
    mi=3.3435860e-27
    ### field evals ###
    for dumpfile in files:
        eval_nimrod = EvalNimrod(dumpfile, fieldlist='vnt')
        fval = eval_nimrod.eval_field('n', eval_grid, dmode=0, eq=2)
        ne[dumpfile] = (grid[0,:], 
                        Scalar(fval, grid, torgeom=gmt, dmode=0, iqty=0).data)
        nd[dumpfile] = (grid[0,:],
                        Scalar(fval, grid, torgeom=gmt, dmode=0, iqty=1).data)
        fval = eval_nimrod.eval_field('ndn', eval_grid, dmode=0, eq=2)
        ndn[dumpfile] = (grid[0,:],
                         Scalar(fval, grid, torgeom=gmt, dmode=0).data)
        fval = eval_nimrod.eval_field('vn', eval_grid, dmode=0, eq=2)
        rhovn[dumpfile] = (grid[0,:],
                           mi*ndn[dumpfile][1]*
                           Vector(fval, grid, torgeom=gmt, dmode=0).data[0])
        fval = eval_nimrod.eval_field('ti', eval_grid, dmode=0, eq=2)
        ti[dumpfile] = (grid[0,:],
                        Scalar(fval, grid, torgeom=gmt, dmode=0).data)
        fval = eval_nimrod.eval_field('te', eval_grid, dmode=0, eq=2)
        te[dumpfile] = (grid[0,:],
                        Scalar(fval, grid, torgeom=gmt, dmode=0).data)
        fval = eval_nimrod.eval_field('tn', eval_grid, dmode=0, eq=2)
        tn[dumpfile] = (grid[0,:],
                        Scalar(fval, grid, torgeom=gmt, dmode=0).data)
        prn[dumpfile] = (grid[0,:],
                        kb*ndn[dumpfile][1]*tn[dumpfile][1])

    ### plot ###
    nrow = 1
    ncol = 3
    figfac = 8
    vmargfac = 0.2
    hmargfac = 0.4
    fig = pl.figure(figsize=(figfac*ncol+1,figfac*nrow+1)) 
    gs = gridspec.GridSpec(nrow, ncol,
         wspace=hmargfac, hspace=vmargfac, 
         top=1.-vmargfac/(nrow+2*vmargfac), bottom=vmargfac/(nrow+2*vmargfac), 
         left=0.5*hmargfac/(ncol+2*hmargfac), right=1.-0.5*hmargfac/(ncol+2*vmargfac)) 
    
    ax = pl.subplot(gs[0,0])
    pn.plot_dict_scalar_line(ne, ylabel=r'electron density ($m^{-3}$), solid',
                             style='-',  legend_loc='lower left', 
                             arrow_dir='left', arrow_xloc=0.4,
                             ax=ax, show=False, xlabel=r'R ($m$)')
    ax2 = ax.twinx()
    pn.plot_dict_scalar_line(te, ylabel=r'plasma temperature ($eV$), dashed',
                             legend_loc=None,
                             arrow_dir='right', arrow_xloc=0.45,
                             style='--', ax=ax2, show=False)
    
    ax = pl.subplot(gs[0,1])
    pn.plot_dict_scalar_line(tn, ylabel=r'neutral temperature ($eV$), solid',
                             legend_loc=None,
                             arrow_dir='left', arrow_xloc=0.4,
                             style='-', ax=ax, show=False)
    ax2 = ax.twinx()
    pn.plot_dict_scalar_line(ndn, ylabel=r'neutral density ($m^{-3}$), dashed',
                             style='--', legend_loc='center left', ax=ax2, 
                             arrow_dir='right', arrow_xloc=0.7,
                             show=False, xlabel=r'R ($m$)')

    ax = pl.subplot(gs[0,2])
    pn.plot_dict_scalar_line(rhovn,
                             ylabel=r'neutral $\rho v_R$ ($kg m^{-2} s^{-1}$), solid',
                             legend_loc=None,
                             arrow_dir='left', arrow_xloc=0.6,
                             style='-', ax=ax, show=False)
    ax.set_ylim([-4.e-7,4.e-7])
    ax2 = ax.twinx()
    pn.plot_dict_scalar_line(prn, ylabel=r'neutral pressure ($Pa$), dashed',
                             legend_loc='lower left', style='--', 
                             arrow_dir='right', arrow_xloc=0.7,
                             ax=ax2, show=False, xlabel=r'R ($m$)')
    ax2.set_ylim([-0.15,0.15])

    pl.show()

def main():
    parser = optparse.OptionParser(usage="%prog [options] h5 files")
    parser.add_option('-d', '--debug', dest='debug',
                        help='Print out debug information',
                        action='store_true')
    options, args = parser.parse_args()
    if len(args) < 1:
        parser.print_usage()
        return
      
    grid = pn.grid_1d_gen([0, 0.1, 0], [0.8, 0.1, 0], 800)
    eval_grid = EvalGrid(grid)
    
    # parse namelist
    nml = f90nml.read('nimrod.in')
    gmt = nml['grid_input']['geom']
    if gmt == 'tor':
        gmt=True
    else:
        gmt=False
    
    plot_neutral_eqn(args, grid, eval_grid, gmt, nml)

if __name__ == "__main__":
    main()


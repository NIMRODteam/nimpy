Collection of rough scripts.

### init_fields.py
Read a dump file and set fields to expressions

### muCalc.py
Calculate neoclassical quantities and rho mu for a multispecies input

### nl_force.py
Calculate the FSA Maxwell and Reynolds stresses

### particle.py
Trace single-particle drift motion

### plotEqns.py
Plot momentum and continuity equations

### plotProf.py
Plot FSA profiles

### traceOrbits.py
Use particle.py to trace out single-particle drift motion as a function of
phase space and physical coordinate.

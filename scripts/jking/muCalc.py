#!/usr/bin/env python3

import f90nml,os,argparse
import numpy as np
import matplotlib.pyplot as plt
from scipy.interpolate import interp1d,splev,UnivariateSpline,splrep
from datetime import date
from nimpy.plot_nimrod import PlotNimrod as pn
from nimpy.eval_nimrod import EvalNimrod
from nimpy.field_class import Scalar,Vector,Tensor
from nimpy.fsa import FSA,find_xpoint,find_opoint

def basefsa(rzc, y, dy, eval_nimrod, fdict):
    '''
    Flux surface averge quantities (f/bdgrth where y[2]=1/bdgrth)
    Set neq to number of outputs in FSA call
    and fill dy[4:4+neq]
    '''
    isurf=fdict.get('isurf')
    bextrema=fdict['bextrema']
    bigr=fdict['bigr']
    eval_eq=fdict['eval_eq']
    nsp=eval_nimrod.ndnq
    n = eval_nimrod.eval_field('n', rzc, dmode=0, eq=eval_eq)
    for isp in range(nsp):
        dy[4+isp] = n[isp]*dy[2]
    ti = eval_nimrod.eval_field('ti', rzc, dmode=0, eq=eval_eq)
    dy[4+nsp] = ti[0]*dy[2] # ti
    te = eval_nimrod.eval_field('te', rzc, dmode=0, eq=eval_eq)
    dy[5+nsp] = te[0]*dy[2] # te
    bf = eval_nimrod.eval_field('b', rzc, dmode=1, eq=eval_eq)
    B = Vector(bf, rzc, torgeom=True, dmode=1)
    bsq = B.dot(B,dmode=0).data
    dy[6+nsp] = bsq*dy[2] # B**2
    dy[7+nsp] = (B.hat(dmode=0).dot(grad(B.mag())).data)**2*dy[2] # (b.grad(|B|))**2
    dy[8+nsp] = rzc[0]*bf[2]*dy[2] # R B_Phi
    bmag = np.sqrt(bsq)
    bextrema[0,isurf] = min(bextrema[0,isurf], bmag)
    bextrema[1,isurf] = max(bextrema[1,isurf], bmag)
    bigr[isurf] = max(bigr[isurf], rzc[0])
    vf = eval_nimrod.eval_field('v', rzc, dmode=0, eq=eval_eq)
    dy[9+nsp] = vf[2]*dy[2]/rzc[0] # omega
    dy[10+nsp] = (vf[0]*bf[0]+vf[1]*bf[1])*dy[2]/(bf[0]*bf[0]+bf[1]*bf[1]) # Kpol
    return dy

def trapfsa(rzc, y, dy, eval_nimrod, fdict):
    '''
    Flux surface averge quantities (f/bdgrth where y[2]=1/bdgrth)
    Set neq to number of outputs in FSA call
    and fill dy[4:4+neq]
    '''
    eval_eq=fdict['eval_eq']
    bf = eval_nimrod.eval_field('b', rzc, dmode=0, eq=eval_eq)
    B = Vector(bf, rzc, torgeom=True, dmode=0)
    bmag = B.mag().data
    bave=fdict['bave']
    nlam=fdict['nlam']
    lam=fdict['lam']
    rzo=fdict['rzo_copy']
    dy[4:4+nlam] = np.sqrt(1.0 - lam[:]*bmag/bave)
    dy[4+nlam:4+2*nlam] = dy[2]/(dy[4:4+nlam])
    dy[4:4+nlam]*=dy[2] #note dy[4:4+nlam] is used in above calc
    dy[4+2*nlam] = dy[2]*bmag/bave
    dy[4+2*nlam+1] = dy[2]*np.sqrt((rzc[0]-rzo[0])**2+(rzc[1]-rzo[1])**2)
    return dy

def create_interp_extrap(xcoord, ycoord, y0):
    return interp1d(np.concatenate((np.array([0.0]), xcoord)),
                    np.concatenate((np.array([y0]), ycoord)),
                    kind = 'cubic', fill_value = 'extrapolate')

def bump_fit(func, pfit, title, write_plots, qlist):
    npts = 400
    psi = np.linspace(0,1,npts,endpoint=True)
    spl = splrep(psi,func(psi))
    
    temp = np.zeros_like(psi)
    dpsisq = (1-pfit)**2
    for ii, ipsi in enumerate(psi):
      if ipsi <= pfit:
        temp[ii] = splev(ipsi,spl)
      elif ipsi >= 1.0:
        temp[ii] = 0.0
      else:
        temp[ii] = splev(ipsi,spl) * np.exp(-dpsisq/(dpsisq-(ipsi-pfit)**2))
    fig,ax = create_figure(nrows=1, qlist=qlist)
    pn.plot_scalar_line(None, splev(psi, spl), flabel=r'calculated',
                        f2=temp, f2label=r'bump fit',
                        xvar=psi, xlabel=r'$\psi_N$',
                        ylabel=title+r' $\rho \mu$',
                        style='varied', legend_loc='upper left', 
                        ax=ax, show=False)
    if write_plots:
        fig.savefig(title+'_bump.svg')
    else:
        fig.show()

    return psi, temp

def write_mufile(psii, rho_mui, psie, rho_mue):
    PSI_NAME = "psinorm"
    ELE_NAME = "rho_mue"
    ION_NAME = "rho_mui"
    DELE_NAME = "drho_mue/dpsiN"
    DION_NAME = "drho_mui/dpsiN"  
    ispl = splrep(psii, rho_mui)
    espl = splrep(psie, rho_mue)
    numi = psii.shape[0]
    nume = psie.shape[0]
    filename = "mu.dat"
    with open(filename, 'w') as f:
      f.write(f"{nume} {PSI_NAME} {ELE_NAME} {DELE_NAME}\n")
      for psi in psie:
        if psi == 0:
          f.write(f"{psi} {0.0} {0.0}\n")
        else:
          f.write(f"{psi} {splev(psi, espl)} {splev(psi, espl)}\n")
      f.write(f"{numi} {PSI_NAME} {ION_NAME} {DION_NAME}\n")
      for psi in psie:
        if psi == 0:
          f.write(f"{psi} {0.0} {0.0}\n")
        else:
          f.write(f"{psi} {splev(psi, ispl)} {splev(psi, ispl)}\n")

def create_figure(nrows=1, qlist=None):
    fig_size = [12,nrows*6.75]
    fig,axs = plt.subplots(nrows=nrows, figsize=fig_size)
    if nrows>1:
        for ax in axs:
            for rhoq in qlist:
                ax.axvline(rhoq, ls=':')
    else:
        for rhoq in qlist:
            axs.axvline(rhoq, ls=':')
    return fig,axs

def neoclassical_calculator(dumpfile, write_plots, fd, nedge, ncore, 
                            tedge, tcore, ticonst, epsilon):
    '''
    Compute neoclassical quantities from dumpfile
    '''
    eval_nimrod = EvalNimrod(dumpfile, fieldlist='nvptbjd')
    nsp = eval_nimrod.ndnq
    try:
        rzo = find_opoint(eval_nimrod)
    except Exception as e:
        raise Exception('O-point location is required')
    try:
        rzx = find_xpoint(eval_nimrod)
    except:
        rzx = None

################################################################################
# Calculate basic fsa quantitites
################################################################################

    nsurf = 150 # FSA surfaces
    bextrema = np.zeros([2,nsurf])
    bextrema[0,:] =  np.inf # min
    bextrema[1,:] = -np.inf # max
    bigr = np.zeros([nsurf])
    bigr[:] = -np.inf # max

    dpow=1.0
    fsafilename = 'fsa.npz'
    if os.path.exists(fsafilename):
        fsaDict = np.load(fsafilename)
        dvar = fsaDict['arr_0']
        yvars = fsaDict['arr_1']
        contours = fsaDict['arr_2']
        bextrema = fsaDict['arr_3']
        bigr = fsaDict['arr_4']
    else:
        dvar, yvars, contours = FSA(eval_nimrod, rzo, basefsa, 7+nsp, nsurf=nsurf, 
                                    depvar='eta', dpow=dpow, rzx=rzx,
                                    bextrema=bextrema, bigr=bigr, eval_eq=3)
        fsaArr = [dvar, yvars, contours, bextrema, bigr]
        np.savez(fsafilename,*fsaArr)

    # Determine where the FSA failed
    iend=-1
    while np.isnan(yvars[:,iend]).any():
        iend -= 1
    iend += yvars.shape[1]+1

################################################################################
# Calculate trapped and passing fractions
################################################################################

    trapfilename = 'trap.npz'
    if os.path.exists(trapfilename):
        trapDict = np.load(trapfilename)
        f_pass = trapDict['arr_0']
        eps = trapDict['arr_1']
    else:
        # Arrays for passing/trapped fractions
        # ind 0 - Helander and Sigmar Eq. 11.24
        #   f_t = 1 - (3/4)*int_0^(Bave/Bmax) dlambda lambda/<SQRT(1-lambda*B/Bave)>
        # ind 1 - Lin-Liu and Miller (1995)
        #   f_tl = 1 - (3/4)*int_0^(Bave/Bmax) dlambda lambda*<1/SQRT(1-lambda*B/Bave)>
        # ind 2 - f_tu from Lin-Liu and Miller (1995)
        #   f_tu = 1 - (3/4)*int_0^(Bave/Bmax) dlambda lambda/SQRT(1-lambda*<B/Bave>)
        # int 3 - f_te from inverse aspect ratio as in B5
        #   f_c ~ 1 - 1.46*sqrt(eps) + 0.46*eps*sqrt(eps)
        f_pass = np.zeros([4,iend])
        eps = np.zeros([iend])
        # integrate from 0 to bmax
        for ii in range(iend):
            nlam = 100
            lam, weights = np.polynomial.legendre.leggauss(nlam)
            bave = np.sqrt(yvars[2+nsp,ii]) # sqrt(<B^2>)
            lam += 1
            lam *= bave/(2.0*bextrema[1,ii])
            weights *= bave/(2.0*bextrema[1,ii])
            rzp = [contours[0,0,ii], contours[1,0,ii], 0]
            intgr, contour = FSA(eval_nimrod, rzo, trapfsa, 2*nlam+2, nsurf=1,
                                 depvar='eta', rzp=rzp, bave=bave, nlam=nlam,
                                 lam=lam, rzo_copy=rzo, eval_eq=3)
            f_pass[0,ii] = 0.75*np.sum(weights*lam/intgr[0:nlam]) # Callen Eqn B5
            f_pass[1,ii] = 0.75*np.sum(weights*lam*intgr[nlam:2*nlam])
            f_pass[2,ii] = 0.75*np.sum(weights*lam/np.sqrt(1.0-lam*intgr[2*nlam]))
            eps[ii] = intgr[2*nlam+1]/rzo[0]
            f_pass[3,ii] = 1 + (-1.46 + 0.46*eps[ii])*np.sqrt(eps[ii])
            print(ii,dvar[1,ii],f_pass[:,ii])
        trapArr = [f_pass,eps]
        np.savez(trapfilename,*trapArr)

    # Determine where the FSA failed
    iend=-1
    while np.isnan(f_pass[:,iend]).any():
        iend -= 1
    while np.isnan(eps[iend]).any():
        iend -= 1
    iend += eps.shape[0]+1
    f_pass = f_pass[:,0:iend]
    eps = eps[0:iend]
    f_trap = 1.0 - f_pass[:,:]

################################################################################
# Plot fsa quantities
################################################################################

    ti = yvars[0+nsp,:iend]
    te = yvars[1+nsp,:iend]
    fsabsq = yvars[2+nsp,:iend]
    fsabdgrBsq = yvars[3+nsp,:iend]
    rbphi = yvars[4+nsp,:iend]
    omega = yvars[5+nsp,:iend]
    kpol = yvars[6+nsp,:iend]
    psin = dvar[0,:iend]
    rhon = dvar[1,:iend]
    psi = dvar[2,:iend]
    psix = dvar[2,-1]
    q = np.fabs(dvar[7,:iend])
    bigr = bigr[:iend]

    ### determine q values to plot as horizontal line ###
    rhoofq = interp1d(q,rhon)
    psiofq = interp1d(q,psin)
    qmin = numpy.min(q)
    qmax = numpy.max(q)
    qrange = range(int(numpy.ceil(qmin)), 
                   min(int(numpy.ceil(qmin))+5, int(numpy.ceil(qmax))))
    rhoq_list = rhoofq(qrange)
    psiq_list = psiofq(qrange)

    ### Plot trapped fraction ###
    fig,axs = create_figure(nrows=2, qlist=rhoq_list)
    pn.plot_scalar_line(None, f_trap[0,:], flabel=r'$f_t$',
                        f2=f_trap[3,:], f2label=r'$f_{t}$ approx',
                        xvar=rhon, xlabel=r'$\rho_N$', ylabel=r'$f_t$',
                        style='varied', legend_loc='upper left', ax=axs[0],
                        show=False)
    pn.plot_scalar_line(None, fsabsq, flabel=r'$\langle B^2 \rangle$',
                        f2=100*fsabdgrBsq, f2label=r'$100\times \langle(\mathbf{b}\cdot\nabla B)^2\rangle$',
                        f3=10*eps, f3label=r'$10\times \epsilon$',
                        xvar=rhon, xlabel=r'$\rho_N$', ylabel='',
                        style='varied', legend_loc='upper left', ax=axs[1],
                        show=False)
    if write_plots:
        fig.savefig('trap.svg')
    else:
        fig.show()

################################################################################
#   general plasma quantities
################################################################################

    ### backward compatible with nimdevel ###
    if eval_nimrod.ndnq==1:
        nsp=2

    ### if adjusting values, set nsp accordingly ###
    if fd>0:
        if fd==1:
            nsp=2
        else:
            nsp=3 

    #### create arrays that mirror eventual multispecies v2 structure ###
    ns = numpy.zeros([nsp,iend])
    ts = numpy.zeros([nsp,iend])
    ms = numpy.zeros([nsp])
    qs = numpy.zeros([nsp])
    zs = numpy.zeros([nsp])
    kb   = 1.609e-19    # eV/J
    eps0 = 8.85418782e-12
    mu0  = 4.e-7*np.pi
    nml = f90nml.read('nimrod.in')
    try:
        misp = nml['const_input']['misp_input']
    except:
        misp = [3.3435860e-27, 1.9944235e-26]
        #misp = [1.6735575e-27, 1.9944235e-26]
    try:
        zisp = nml['const_input']['zisp_input']
    except:
        zisp = [1., 6.]
    try:
        me = nml['const_input']['me_input']
    except:
        me = 9.107e-31
    try:
        echrg = nml['const_input']['chrg_input']
    except:
        echrg = 1.602e-19
    ns[0:nsp] = yvars[0:nsp,:iend]
    if eval_nimrod.ndnq==1: # backward compatible with nimdevel
        ns[1]=ns[0]
    ts[0] = te
    for isp in range(1,nsp):
        ts[isp] = ti
    ms[0] = me; ms[1:nsp] = misp[0:nsp-1]
    zs[0] = 1;  zs[1:nsp] = zisp[0:nsp-1]
    qs=zs*echrg
    slabel = ["e", "d", "c"]

    ### apply adjustment ###
    # Te = Te0
    # Ti = fi*Ti0
    # ne = fe*ne0
    # nd = fd*ne (the rest is carbon)
    # Require fi>0
    if fd>0:
        #print('init',ns[:,0],ts[:,0],ns[:,-1],ts[:,-1])
        # compute pressure profile
        p0 = ns[0]*ts[0]
        for isp in range(1,nsp):
            p0 += ns[isp]*ts[isp]
        # modified density, electron temperature
        ns[0] = nedge+(ncore-nedge)*(te-te[-1])/te[0]
        ns[1] = fd*ns[0]
        if nsp>2:
            ns[2]=(1.-fd)*ns[0]/zs[2]
        te = tedge+(tcore-tedge)*(te-te[-1])/te[0]
        ts[0] = te
        # modified ion temperature profile
        if ticonst>0:
            ti[:] = ticonst
        else:
            nisum = 0.
            for isp in range(1,nsp):
                nisum += ns[isp]
            ti = (p0 - ns[0]*te)/nisum
        if numpy.all(ti<0):
            Exception('Adjustment produces negative ion temperature')
        for isp in range(1,nsp):
            ts[isp] = ti
        #print('mod ',ns[:,0],ts[:,0],ns[:,-1],ts[:,-1])
        print('n(0)=',"%.2e"%ncore,'n(1)=',"%.2e"%nedge)
        print('Te(0)=',"%.2f"%tcore,'Te(1)=',"%.2f"%tedge)
        print('Ti(0)=',"%.2f"%ti[0],'Ti(1)=',"%.2f"%ti[-1])

    fig,axs = create_figure(nrows=2, qlist=rhoq_list)
    plt_dict = {}
    for isp in range(nsp):
        plt_dict[r'$n_{'+slabel[isp]+'}$'] = rhon,ns[isp]
    pn.plot_dict_scalar_line(plt_dict, xlabel=r'$\rho_N$', ylabel=r'Density ($m^{-3}$)',
                             style='varied', legend_loc='lower left', 
                             ax=axs[0], show=False)
    plt_dict = {}
    for isp in range(nsp):
        plt_dict[r'$T_{'+slabel[isp]+'}$'] = rhon,ts[isp]
    pn.plot_dict_scalar_line(plt_dict, xlabel=r'$\rho_N$', ylabel=r'Temperature (eV)',
                             style='varied', legend_loc='lower left',
                             ax=axs[1], show=False)
    if write_plots:
        fig.savefig('profiles.svg')
    else:
        fig.show()

    zeff_qn = 0 # quasineutrality definition
    zeff_tr = 0 # transport definition
    zstar   = 0
    for isp in range(1,nsp):
        zeff_qn += ns[isp]
        zeff_tr += ns[isp]*zs[isp]**2
        if isp>1:
            zstar += ns[isp]*zs[isp]**2
    zeff_qn = ns[0]/zeff_qn
    zeff_tr = zeff_tr/ns[0]
    zstar   = zstar/ns[1]

    fig,ax = create_figure(nrows=1, qlist=rhoq_list)
    plt_dict = {r'$Z_{eff,qn}$':(rhon,zeff_qn), r'$Z_{eff,tr}$':(rhon,zeff_tr),
                r'$Z_{*}$':(rhon,zstar)}
    pn.plot_dict_scalar_line(plt_dict, xlabel=r'$\rho_N$', ylabel='',
                             style='varied', legend_loc='upper right',
                             ax=ax, yscilabel=False, show=False)
    if write_plots:
        fig.savefig('zeff.svg')
    else:
        fig.show()

################################################################################
#   compute collision freqencies
################################################################################
    
    ### gamma_ab ###
    gam_ab=numpy.zeros([nsp,nsp,iend])
    for isp in range(nsp):
        gam_ab[isp,isp]=ms[isp]/(kb*ts[isp])
    for isp in range(nsp):
        for jsp in range(isp):
            if (isp==jsp):
                continue
            gam_ab[isp,jsp]=gam_ab[isp,isp]*gam_ab[jsp,jsp] \
                            /(gam_ab[isp,isp]+gam_ab[jsp,jsp])
            gam_ab[jsp,isp]=gam_ab[isp,jsp]
    for isp in range(nsp):
        gam_ab[isp,isp]=gam_ab[isp,isp]/2

    ### reduced mass ###
    rmass=numpy.zeros([nsp,nsp,iend])
    for isp in range(nsp):
        for jsp in range(nsp):
            rmass[isp,jsp]=ms[isp]*ms[jsp]/(ms[isp]+ms[jsp])

    ### Debye length & Coulomb log ###
    debye_len=0
    for isp in range(nsp):
        debye_len+=ns[isp]*qs[isp]**2/ts[isp]
    debye_len=np.sqrt(eps0*kb/debye_len)

    coul_log=numpy.zeros([nsp,nsp,iend])
    for isp in range(nsp):
        for jsp in range(nsp):
            coul_log[isp,jsp]= \
                    np.log(12*np.pi*eps0*rmass[isp,jsp] \
                           /np.fabs(qs[isp]*qs[jsp]) \
                           *debye_len/gam_ab[isp,jsp])

    plt_dict = {}
    for isp in range(nsp):
        for jsp in range(isp,nsp):
            plt_dict[r'$ln\Lambda_{'+slabel[isp]+slabel[jsp]+'}$'] \
              = rhon,coul_log[isp,jsp]

    fig,axs = create_figure(nrows=2, qlist=rhoq_list)
    pn.plot_scalar_line(None, debye_len,
                        xvar=rhon, xlabel='', ylabel='Debye length (m)',
                        style='varied',legend_loc='upper right',
                        ax=axs[0], show=False)
    pn.plot_dict_scalar_line(plt_dict, xlabel=r'$\rho_N$', ylabel='Coulomb Log',
                             style='varied',legend_loc='lower left',
                             ax=axs[1], show=False)
    if write_plots:
        fig.savefig('debye.svg')
    else:
        fig.show()

    ### nu ###
    vt_s=numpy.zeros([nsp,iend])
    for isp in range(nsp):
        vt_s[isp] = np.sqrt(kb*ts[isp]/ms[isp])
    mu_s=numpy.zeros([nsp,iend])
    nu_s=numpy.zeros([nsp,iend])
    tau_ss=numpy.zeros([nsp,iend])
    nu_ij=numpy.zeros([nsp,nsp,iend])
    for isp in range(nsp):
        for jsp in range(nsp):
            nu_ij[isp,jsp]=4/3*np.sqrt(2*np.pi) \
                  *(qs[isp]*qs[jsp]/(4*np.pi*eps0*rmass[isp,jsp]))**2 \
                  *ns[jsp]*gam_ab[isp,jsp]**1.5*coul_log[isp,jsp]
            nu_s[isp]=nu_s[isp]+nu_ij[isp,jsp]
            if isp==jsp:
                tau_ss[isp]=1/nu_ij[isp,jsp]

    plt_dict = {}
    for isp in range(nsp):
        plt_dict[r'$\nu_{'+slabel[isp]+'}$'] = rhon,nu_s[isp]

    fig,axs = create_figure(nrows=3, qlist=rhoq_list)
    pn.plot_dict_scalar_line(plt_dict, xlabel='', ylabel='collision freq 1/s',
                             style='varied', legend_loc='upper left',
                             ax=axs[0], logy=True, show=False)

    ### Electrical diffusivity ###
    elecd = numpy.zeros([iend])
    elecd = nu_s[0]*ms[0]/(mu0*qs[0]**2*ns[0])
    plt_dict = {r'$\eta /\mu_0$':(rhon,elecd)}
    pn.plot_dict_scalar_line(plt_dict, xlabel='', ylabel=r'Resistivity $m^2/s$',
                             style='varied', legend_loc='upper left',
                             ax=axs[1], logy=True, show=False)

    ### eta_00 (parallel viscosity) ###
    z_for_K = numpy.zeros([nsp,iend])
    z_for_K[0] = zeff_tr
    z_for_K[1] = zstar
    # TODO nsp>3, unclear on formula. Using n_d / n_i Z_i**2 ~ 1/zstar for now
    for isp in range(2,nsp):
        z_for_K[isp] = ns[1]/(ns[isp]*zs[isp]**2)
    sqrt2 = np.sqrt(2)
    eta_00_coef = (5/6)*(17*z_for_K/4+205/(48*sqrt2)) \
                  /(2*z_for_K**2+301*z_for_K/(48*sqrt2)+89/48)
    eta00_s=numpy.zeros([nsp,iend])
    for isp in range(nsp):
        eta00_s[isp] = eta_00_coef[isp] * ms[isp] * ns[isp] \
                       * nu_s[isp] * vt_s[isp]**2 # A17 

    plt_dict = {}
    for isp in range(nsp):
        plt_dict[r'$\eta_{00'+slabel[isp]+'}$'] = rhon,eta00_s[isp]

    pn.plot_dict_scalar_line(plt_dict, xlabel=r'$\rho_N$',
                             ylabel='parallel viscosity $m^2/s$',
                             style='varied', legend_loc='lower left',
                             logy=True, ax=axs[2], show=False)
    if write_plots:
        fig.savefig('collision.svg')
    else:
        fig.show()

    omegat_s=numpy.zeros([nsp,iend])
    nustar_s=numpy.zeros([nsp,iend])
    nustar_approx_s=numpy.zeros([nsp,iend])
    for isp in range(nsp):
        omegat_s[isp] = vt_s[isp]/ (rzo[0] * q) # B10
        nustar_s[isp] = f_trap[0] / (2.92 * f_pass[0]) \
                        * nu_s[isp] * omegat_s[isp] / vt_s[isp]**2 \
                        * fsabsq / fsabdgrBsq # B12
        if epsilon: # use crude value if available
            nustar_approx_s[isp] = nu_s[isp]/(omegat_s[isp]*epsilon**1.5) # B7
        else: # otherwise use exact formula
            nustar_approx_s[isp] = nu_s[isp]/(omegat_s[isp]*eps**1.5)


    plt_dict = {}
    for isp in range(nsp):
        plt_dict[r'$\nu_{*'+slabel[isp]+'}$'] = rhon,nustar_s[isp]

    fig,ax = create_figure(nrows=1, qlist=rhoq_list)
    pn.plot_dict_scalar_line(plt_dict, xlabel=r'$\rho_N$', ylabel='',
                             style='varied', legend_loc='upper center',
                             logy=True, ax=ax, show=False)
    if write_plots:
        fig.savefig('nu_star.svg')
    else:
        fig.show()

################################################################################
#   compute viscosity coefficients
################################################################################

    mu00_s=numpy.zeros([nsp,iend])
    mu01_s=numpy.zeros([nsp,iend])
    mu11_s=numpy.zeros([nsp,iend])
    for isp in range(nsp):
        # Table 1 equations
        D = 1.2 * (2 * z_for_K[isp]**2 + 301 / (48 * sqrt2) * z_for_K[isp] + 89 / 48)
        nustauss = nu_s[isp]*tau_ss[isp]  
        K00B_s = (z_for_K[isp] + sqrt2 - np.log(1 + sqrt2)) / nustauss
        K01B_s = (z_for_K[isp] + 1.0 / sqrt2) / nustauss
        K11B_s = (2.0 * z_for_K[isp] + 9.0 / (4.0 * sqrt2)) / nustauss
        K00P_s = np.sqrt(np.pi)
        K01P_s = 3 * np.sqrt(np.pi)
        K11P_s = 12 * np.sqrt(np.pi)
        K00PS_s = (17 * z_for_K[isp] / 4 + 205 / (sqrt2 * 48) ) / D
        K01PS_s = (7 / 2) * (23 * z_for_K[isp] / 4 + 241 / (sqrt2 * 48) ) / D
        K11PS_s = (49 / 4) * (33 * z_for_K[isp] / 4 + 325 / (sqrt2 * 48) ) / D

        # B13
        K00tot_s = K00B_s / (1 + np.sqrt(nustar_s[isp]) + 2.92 * nustar_s[isp] \
                 * K00B_s / K00P_s) \
                 / (1 + 2 * K00P_s / (3 * omegat_s[isp] * tau_ss[isp] * K00PS_s) )
        K01tot_s = K01B_s / (1 + np.sqrt(nustar_s[isp]) + 2.92 * nustar_s[isp] \
                 * K01B_s / K01P_s) \
                 / (1 + 2 * K01P_s / (3 * omegat_s[isp] * tau_ss[isp] * K01PS_s) )
        K11tot_s = K11B_s / (1 + np.sqrt(nustar_s[isp]) + 2.92 * nustar_s[isp] \
                 * K11B_s / K11P_s) \
                 / (1 + 2 * K11P_s / (3 * omegat_s[isp] * tau_ss[isp] * K11PS_s) )

        # B3 / B14
        nu_ref = nu_s[isp] * f_trap[0] / f_pass[0]
        mu00_s[isp] = K00tot_s * nu_ref
        mu01_s[isp] = ( (5/2)*K00tot_s - K01tot_s ) * nu_ref
        mu11_s[isp] = ( K11tot_s - 5*K01tot_s + (25/4)*K00tot_s ) * nu_ref

    plt_dict = {}
    for isp in range(nsp):
        plt_dict[r'$\mu_{00'+slabel[isp]+'}$'] = rhon,mu00_s[isp]
        plt_dict[r'$\mu_{01'+slabel[isp]+'}$'] = rhon,mu01_s[isp]
        plt_dict[r'$\mu_{11'+slabel[isp]+'}$'] = rhon,mu11_s[isp]

    fig,ax = create_figure(nrows=1, qlist=rhoq_list)
    pn.plot_dict_scalar_line(plt_dict, xlabel=r'$\rho_N$', ylabel='Damping Freq 1/s',
                             style='varied', legend_loc='lower center',
                             legend_ncol=3, logy=True, ax=ax, show=False)
    if write_plots:
        fig.savefig('mu.svg')
    else:
        fig.show()

################################################################################
#   compare M_e (mu00_e) (neoclassical drive) and N_e (nu00_e)
################################################################################

    # C9
    nu00_e = zeff_tr * nu_ij[0,0]
    nu01_e = 1.5*nu00_e
    nu11_e = ( np.sqrt(2) + (13/4) * zeff_tr ) * nu_ij[0,0]
    
    # compute (N_e+M_e)^-1 . M_e
    NM00 = nu00_e + mu00_s[0]
    NM01 = nu01_e + mu01_s[0]
    NM11 = nu11_e + mu11_s[0]
    invdet = 1 / ( NM00 * NM11 - NM01**2 )
    NM00inv = NM11*invdet
    NM01inv = -NM01*invdet
    NM11inv = NM00*invdet
    NMinvM00 = NM00inv*mu00_s[0] + NM01inv*mu01_s[0]
    NMinvM01 = NM00inv*mu01_s[0] + NM01inv*mu11_s[0]
    NMinvM10 = NM01inv*mu00_s[0] + NM11inv*mu01_s[0]
    NMinvM11 = NM01inv*mu01_s[0] + NM11inv*mu11_s[0]

    # compute collisionless (banana), large aspect ratio coefficient from C17
    BS_coef = mu00_s[0] / (nu00_e + mu00_s[0] )

    plt_dict = {}
    plt_dict[r'$[[N_e+M_e]^{-1}\cdot M_e]_{00}$'] = rhon,NMinvM00
    plt_dict[r'$[[N_e+M_e]^{-1}\cdot M_e]_{01}$'] = rhon,NMinvM01
    plt_dict[r'$\mu_{e00}/(\nu_{e00}+\mu_{e00})$'] = rhon,BS_coef

    fig,ax = create_figure(nrows=1, qlist=rhoq_list)
    pn.plot_dict_scalar_line(plt_dict, xlabel=r'$\rho_N$', ylabel='Bootstrap coef.',
                             style='varied', legend_loc='lower center',
                             logy=True, ax=ax, show=False)
    if write_plots:
        fig.savefig('bootstrap.svg')
    else:
        fig.show()

################################################################################
#   write mu result to file
################################################################################

    rho_mue = interp1d( np.concatenate((np.array([0.0]), psin)),
                        np.concatenate((np.array([0.0]), ms[0]*ns[0]*mu00_s[0])),
                        kind = 'cubic', fill_value = 'extrapolate')

    rho_mui_arr = 0
    for isp in range(1,nsp):
      rho_mui_arr += ms[isp]*ns[isp]*mu00_s[isp]
    rho_mui = interp1d( np.concatenate((np.array([0.0]), psin)),
                        np.concatenate((np.array([0.0]), rho_mui_arr)),
                        kind = 'cubic', fill_value = 'extrapolate')
    psi_extrap_to0 = 0.98
    psi_i, rho_mui_fit =  bump_fit(rho_mui, psi_extrap_to0, "Ion",
                                   write_plots, psiq_list)
    psi_e, rho_mue_fit =  bump_fit(rho_mue, psi_extrap_to0, "Electron",
                                   write_plots, psiq_list)
    write_mufile(psi_i, rho_mui_fit, psi_e, rho_mue_fit)


################################################################################
#   write ped top/mid values to file
################################################################################

    spl_ne = create_interp_extrap(psin, ns[0], ns[0,0])
    spl_te = create_interp_extrap(psin, ts[0], ts[0,0])
    spl_NMinvM00 = create_interp_extrap(psin, NMinvM00, 0.)
    spl_NMinvM01 = create_interp_extrap(psin, NMinvM01, 0.)
    spl_BS_coef  = create_interp_extrap(psin, BS_coef, 0.)
    spl_nustar = create_interp_extrap(psin, nustar_s[0], 0.)
    spl_nustar_approx = create_interp_extrap(psin, nustar_approx_s[0], 0.)
    spl_fsabsq = create_interp_extrap(psin, fsabsq, fsabsq[0])
    spl_fsabdgrBsq = create_interp_extrap(psin, fsabdgrBsq, 0.)
    spl_eps   = create_interp_extrap(psin, eps, 0.)
    spl_f_trap   = create_interp_extrap(psin, f_trap[0], 0.)
    spl_nue   = create_interp_extrap(psin, nu_s[0], nu_s[0][0])
    spl_nuee  = create_interp_extrap(psin, nu_ij[0,0], nu_ij[0,0][0])
    spl_mu00e = create_interp_extrap(psin, mu00_s[0], 0.)
    spl_nu00e = create_interp_extrap(psin, nu00_e, 0.)
    spl_vte   = create_interp_extrap(psin, vt_s[0], vt_s[0][0])

    psi_top=0.92
    psi_mid=0.96

    print('n(top)=',"%.2e"%spl_ne(psi_top),'n(mid)=',"%.2e"%spl_ne(psi_mid))
    print('Te(top)=',"%.2e"%spl_te(psi_top),'Te(mid)=',"%.2e"%spl_te(psi_mid))
    print('NMinvM00(top)=',"%.2e"%spl_NMinvM00(psi_top),
          'NMinvM00(mid)=',"%.2e"%spl_NMinvM00(psi_mid))

    filename = "ped.dat"
    with open(filename, 'w') as f:
        f.write("psi ne te NMinvM00 NMinvM01 BS_coef nustar nustar_approx"
                +"fsabsq fsabdgrBsq eps f_trap nue nuee mu00e nu00e vte\n")
        p=psi_top
        vals = p,spl_ne(p),spl_te(p),spl_NMinvM00(p),spl_NMinvM01(p), \
                spl_BS_coef(p),spl_nustar(p),spl_nustar_approx(p), \
                spl_fsabsq(p),spl_fsabdgrBsq(p),spl_eps(p),spl_f_trap(p), \
                spl_nue(p),spl_nuee(p),spl_mu00e(p),spl_nu00e(p),spl_vte(p)
        f.write(" ".join(str(val) for val in vals)+"\n")
        p=psi_mid
        vals = (p,spl_ne(p),spl_te(p),spl_NMinvM00(p),spl_NMinvM01(p),
                spl_BS_coef(p),spl_nustar(p),spl_nustar_approx(p),
                spl_fsabsq(p),spl_fsabdgrBsq(p),spl_eps(p),spl_f_trap(p),
                spl_nue(p),spl_nuee(p),spl_mu00e(p),spl_nu00e(p),spl_vte(p))
        f.write(" ".join(str(val) for val in vals)+"\n")
        p=1.
        vals = (p,spl_ne(p),spl_te(p),spl_NMinvM00(p),spl_NMinvM01(p),
                spl_BS_coef(p),spl_nustar(p),spl_nustar_approx(p),
                spl_fsabsq(p),spl_fsabdgrBsq(p),spl_eps(p),spl_f_trap(p),
                spl_nue(p),spl_nuee(p),spl_mu00e(p),spl_nu00e(p),spl_vte(p))
        f.write(" ".join(str(val) for val in vals)+"\n")
    
if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description='Perform mu calculations from UW-CPTC-09-6R'
    )
    parser.add_argument('file',help='dumpfile',default='dumpgll.00000.h5')
    parser.add_argument('-w', dest='write_plots', action='store_true',
                        help='write plots instead of showing them')
    parser.add_argument('--fd', dest='fd', type=float, action='store',
                        default=-1.0, help='deuterium fraction of electron density')
    parser.add_argument('--nedge', dest='nedge', type=float, action='store',
                        default=-1.0, help='LCFS electron density')
    parser.add_argument('--ncore', dest='ncore', type=float, action='store',
                        default=-1.0, help='core electron density')
    parser.add_argument('--tedge', dest='tedge', type=float, action='store',
                        default=-1.0, help='LCFS electron temperature')
    parser.add_argument('--tcore', dest='tcore', type=float, action='store',
                        default=-1.0, help='core electron temperature')
    parser.add_argument('--ticonst', dest='ticonst', type=float, action='store',
                        default=-1.0, help='ion temperature')
    parser.add_argument('--epsilon', dest='epsilon', type=float, action='store',
                        default=-1.0, help='inverse aspect ratio for approx formulas')
    args = vars(parser.parse_args())
    neoclassical_calculator(dumpfile=args['file'], write_plots=args['write_plots'],
                            fd=args['fd'], nedge=args['nedge'], ncore=args['ncore'],
                            tedge=args['tedge'], tcore=args['tcore'], 
                            ticonst=args['ticonst'], epsilon=args['epsilon'])

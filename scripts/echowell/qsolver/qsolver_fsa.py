#!/usr/bin/env python3
import os
import h5py
#import surfmnstep
import matplotlib.pyplot as plt
import numpy as np
import argparse
import pickle
import glob
import fsa
from shutil import copy2
#import nim_timer as timer
import f90nml
import eval_nimrod as eval
from scipy.interpolate import interp1d,splev,UnivariateSpline,griddata


def load_qcvs(file):
  print(file)
  psin, newq = np.loadtxt(file)
  return psin, newq

def write_qcvs(file,psin,q):
  np.savetxt(file,(psin,q))

def qsolver_fsa(rzc,y,dy,evalnimrod,fdict):
  '''
  Integrand for fsa, this is used to get and q, F and 1/R^2
  Flux surface averge quantities (f/bdgrth where y[2]=1/bdgrth)
  dy(0)=dl/deta or d eta/dl
  dy(1)=dr/deta or dr/dl
  dy(2)=1/bdgth :v'
  dy(3)=dq: q
  '''
  # 4 should be F
  # 5 should be 1/R^2
  b0 = fsa.get_b0(evalnimrod, rzc, addpert=fdict.get('addpert',False))
  dy[4] = b0[2] * rzc[0] * dy[2]
  dy[5] = dy[2]/rzc[0]**2 # 1/R^2
  return dy
  
def q_fsarunner(dumpfile, plot, args):
  fargs = args.copy()
  eqflag = args.get('eqflag',1)
  nsurf = args.get('nsurf',150)
  rzx = args.get('rzx', None)
  rzo = args.get('rzo', [1.768,-0.018831,0.0])

  evalNimrod=eval.EvalNimrod(dumpfile,fieldlist='b')

  dvar, yvar, contours = fsa.FSA(evalNimrod, rzo, qsolver_fsa, 2, \
      nsurf=nsurf,depvar='eta',dpow=0.5,rzx=rzx,flag=eqflag,normalize=False, \
      fargs=fargs)

  all_contour = True
  if np.isnan(yvar[:,:]).any():
    all_contour = False
    iend=-1
    while np.isnan(yvar[:,iend]).any():
      iend -= 1
    iend += yvar.shape[1]+1
  else:
    iend = yvar.shape[1]+1

  psi = dvar[2,:iend]

  psi0 = dvar[2,0]
  if all_contour:
    psi1 = dvar[2,-1]
  else:
    psi1 = dvar[2,iend-1]

  dpsi = psi1 - psi0
  psin = (psi - psi0) / dpsi

  f_fsa_tmp = yvar[0,:iend]/dvar[6, :iend]
  f_comp_tmp = 2.0 * np.pi * dvar[7,:iend]/yvar[1,:iend]

  qpsi=interp1d(dvar[2,:iend], dvar[7,:iend], kind='cubic',fill_value="extrapolate")

  f_fsa = interp1d(dvar[2,:iend], f_fsa_tmp, kind='cubic',fill_value="extrapolate")

  f_comp = interp1d(dvar[2,:iend], f_comp_tmp, kind='cubic',fill_value="extrapolate")

  fsa_rn2 = interp1d(dvar[2,:iend], yvar[1,:iend], kind='cubic',fill_value="extrapolate")


  return psi, psin, qpsi, fsa_rn2, f_fsa, f_comp



def q_runner(fileName,plot,pickle,args):
    #todo this needs to be updated
    dump_pre=["dumpgll","dump"]
    dump_suf=["h5"]
    pickle_pre=["ntm"]
    pickle_suf=["pickle"]
    nimrodin="nimrod.in"
    pre=fileName.split('.')[0]
    step=fileName.split('.')[1]
    if pre in dump_pre:
        print(f"Performing ntm analysis from dump file")
        # check for nimrod.in and hdf5 format
        if not os.path.isfile(nimrodin):
            print(f"nimrod.in not found")
            raise IOError
        if not fileName.split('.')[-1] in dump_suf:
            print(f"dump file is not hdf5 format")
            raise IOError
        nsurf=args.get("nsurf",150)
        fargs={}
        eqflag=args.get("eqflag",1)
        print(fileName)
        qsurfaces, qsurfaces2=get_qsurfaces(fileName,rzo=np.array(args['rzo']),nsurf=nsurf,fargs=fargs)
    #  timer.timer.print_times()

    #pickle data here
        if args['pickle']:
            pfile=pickle_pre[0]+'.'+str(step).zfill(5)+'.'+pickle_suf[0]
            print(f"writing file {pfile}")
            with open(pfile,'wb') as file:
                pass
    #            ntm.dump(file)
    elif pre in pickle_pre:
        pass
      #print("pickle_pre")
      #ntm=step.ntmstep(None,None)
      #ntm.load(file_name)
      #print(f"Time: {ntm.time}" )
      #ntm.plot_fsa_phase(key=None)
#    with open(file_name,'rb') as file:
#      surf=surfmn.fsasurfmn(None,None)
#      surf.load(file)
    else:
        #print(f"File {file_name} is not a recognized file type")
        raise IOError





if __name__ == "__main__":
      parser = argparse.ArgumentParser(description='plots eq q surfaces runner.')
      parser.add_argument('file',help='file name')
      parser.add_argument('--plot', action='store_true',help='shows plots')
      parser.add_argument('--pickle', action='store_true',help='pickle data')
      parser.add_argument('--nsurf', type=int, default=150, help="number of surfaces")
      parser.add_argument('--eqflag', type=int, default=1, help="flag to add n=0 perturbation to eq")
      parser.add_argument('--rzo',type=float, nargs=3, default=[1.768,-0.018831,0.0], help="intial guess for o-point")
      args = vars(parser.parse_args())
      print(args)
      q_runner(fileName=args['file'],plot=args['plot'],\
                    pickle=args['pickle'],args=args)

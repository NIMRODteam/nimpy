#!/usr/bin/env python3
import fortranformat as ff
import numpy as np
import os

def write_newf(oldfile, newfile, fspl):
  if not os.path.isfile(oldfile):
    print(f"File {filename} not found")
    raise OSError
  with open(oldfile, 'r') as file:
    lines = file.readlines()
  with open(newfile, 'w') as file:
    line = lines[0]
    file.write(line)
    code, date, shot, time, dum, nwstr, nhstr = line.split()
    nw = int(nwstr)
    nh = int(nhstr)
    mr = nw-1
    mz = nh-1
    ma = nw-1
    psi = np.linspace(0, 1, num=ma+1, endpoint=True)
    fnew = -1.0 * fspl(psi) #efit uses a negative sign convention?
    for line in lines[1:5]:
      file.write(line)
    nlines, r = divmod(nw, 5)
    if r > 1:
      nlines += 1
    format_line = ff.FortranRecordWriter('(5e16.9)')
    file.write(format_line.write(fnew)+'\n')
    for line in lines[5+nlines:]:
      file.write(line)

def read_f(gfile):
  if not os.path.isfile(gfile):
    print(f"File {gfile} not found")
    raise OSError
  with open(gfile, 'r') as file:
    lines = file.readlines()

  line = lines[0]
  code, date, shot, time, dum, nwstr, nhstr = line.split()
  nw = int(nwstr)
  nh = int(nhstr)
  mr = nw-1
  mz = nh-1
  ma = nw-1
  psi = np.linspace(0, 1, num=ma+1, endpoint=True)
  nlines, rem = divmod(nw, 5)
  if rem > 1:
    nlines += 1
  format_line = ff.FortranRecordReader('(5e16.9)')
  farray = []
  fvalues = []
  for line in lines[5:5+nlines]:
    for value in format_line.read(line):
      if not value is None:
        fvalues.append(-1.0*value)

  farray = np.array(fvalues)
  return psi, farray

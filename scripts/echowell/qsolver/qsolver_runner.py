#!/usr/bin/env python3

import yaml
import argparse
import qsolver_fsa as qfsa
import qsolver_gfile_edit as gedit 
import os
import shutil
import matplotlib.pyplot as plt
import numpy as np
from scipy.interpolate import interp1d
import scipy.optimize as opt
import scipy.linalg as sla
import subprocess
import copy
#ruamel is a fork of pyyaml
#pyyaml has not been updated since 2020 and might be a dead library
#https://yaml.readthedocs.io/en/latest/overview.html
#to install ruamel.yaml uses
#pip install -U pip setuptools wheel
#pip install ruamel.yaml

class QSolver:
  def __init__(self,inputFile):
    self.rootdir = os.getcwd()
    self.readinput(inputFile)
    self.step = 0

  def readinput(self,inputFile):
    try:
      with open(inputFile, 'r') as file:
        self.input = yaml.safe_load(file)
    except IOError as error:
      raise error
    self.qfile = self.input.get('qfile', None)
    self.NIMROD_IN_COPY = 'nimrod.in'
    self.dumpfile = self.input.get('dumpfile', 'dumpgll.00000.h5')
    self.nimrodin = self.input.get('nimrodin', 'nimrod.in')
    self.fluxgridin = self.input.get('fluxgridin', 'fluxgrid.in')
    self.nimeqin = self.input.get('nimeqin', 'nimeq.in')
    self.oculusin = self.input.get('oculus', None)
    self.gfile = self.input.get('gfile', None)
    self.pfile = self.input.get('pfile', None)
    self.mufile = self.input.get('mufile', None)
    self.maxits = self.input.get('maxits',10)
    self.tol = self.input.get('tol',0.01)
    self.plot = self.input.get('plot', False)
    self.tcenter = self.input.get('tcenter', 0.5)
    self.fgexe = self.input.get('fgnimeq',"fgnimeq-ser")
    self.qpts = self.input.get('qpts',151)
    self.gfile_org = self.gfile + '.org'
    self.gfile_new = self.gfile + '.new'
    self.qerror_fst_call = True
    self.jac_method = self.input.get('jac_method', 'foward')

  def readQfile(self):
    if self.qfile is None:
      print("No qfile in input yaml file")
      raise ValueError

    try :
      self.tgt_psin, self.tgt_q = qfsa.load_qcvs(self.qfile)
    except IOError as error:
      print("Error: Can not read qfile")
      raise error
    self.tgt_qspl = interp1d(self.tgt_psin, self.tgt_q,
                             kind='cubic',fill_value="extrapolate")


  def initFileList(self):
      shutil.copy2(self.gfile, self.gfile_org)
      if self.nimrodin != self.NIMROD_IN_COPY:
        shutil.copy2(self.nimrodin, self.NIMROD_IN_COPY)
      
      self.filelist = [self.dumpfile, 
                       self.NIMROD_IN_COPY,
                       self.fluxgridin,
                       self.nimeqin,
                       self.gfile]

      for file in [self.oculusin, self.pfile, self.mufile]:
        if file is not None:
          self.filelist.append(file)

      for file in self.filelist:
        if not os.path.isfile(file):
          print(f"{file} not found")
          raise IOError

  def copy_files(self, dir):
    target = dir + '/'
    for file in self.filelist:
      shutil.copy2(file, target+file)

  def copy_gfile(self, gfile, org_name, itr_name, targetdir):
    shutil.copy2(gfile, targetdir+'/'+itr_name)
    shutil.copy2(gfile, targetdir+'/'+org_name)

  def run_fgnimeq(self):
    ERRMSG = ['NIMEQ: at maxiters', 'NIMEQ: residual increased by a factor']
    subprocess.run(self.fgexe)
    failure = 0
    with open("fgnimeq.out", 'r') as file:
      output = file.read()
      for msg in ERRMSG:
        if msg in output:
          failure = 1
    return failure


  def initalize_f(self):
    self.numknots = self.input.get('numfknots',11)
    self.spl_psi = np.linspace(0,1,num=self.numknots, endpoint=True)
    psi0, f0 = gedit.read_f(self.gfile_org)
    self.f0 = interp1d(psi0, f0, kind='cubic', 
                       fill_value="extrapolate")(self.spl_psi)

  def compute_fspl(self,f):
    fspl = interp1d(self.spl_psi, f, kind='cubic',fill_value="extrapolate")
    return fspl

  def compute_q(self, f, dir_str=None, *args):
    step_str = str(self.step)
    gfile_step = self.gfile + step_str
    if dir_str is None:
      dir = "rundir" + step_str
    else:
      dir = dir_str + step_str
    os.makedirs(dir, exist_ok=True)
    self.copy_files(dir)
    os.chdir(dir)
    shutil.copy2(self.gfile, self.gfile_org)
    fspl = self.compute_fspl(f)
    gedit.write_newf(self.gfile_org, self.gfile, fspl)
    failure = self.run_fgnimeq()
    
    if failure:
      os.chdir(self.rootdir)
      self.step += 1
      return np.full(self.qpts,np.inf,)
    tpsi, tpsin, tqpsi, trn2_fsa, tf_fsa, tf_com = qfsa.q_fsarunner(
      self.dumpfile, False, self.input)
    qnspl = interp1d(tpsin, tqpsi(tpsi), kind='cubic',fill_value="extrapolate")
    os.chdir(self.rootdir)
    self.step += 1
    psin = np.linspace(0,1,num=self.qpts, endpoint=True)
    qn = qnspl(psin)
    return qn

  def qerror_vec(self, qn, debug=False):
    if self.qerror_fst_call == True:
      self.qerror_fst_call = False
      self.qpsin = np.linspace(0,1,num=self.qpts, endpoint=True)
      self.qf = self.tgt_qspl(self.qpsin)
    dq = (self.qf-qn) / self.qf
    return dq

  def compute_jac(self, fn):
    numd_stp = self.input.get('numd_stp',1e-4)
    amt = np.zeros([self.qpts, self.numknots])
    for fidx, fknot in enumerate(fn):
      tf = copy.deepcopy(fn)
      df = numd_stp * fknot
      tf[fidx] += df
      tq = self.compute_q(tf, dir_str='jac')
      tdq = self.qerror_vec(tq)
      if self.jac_method == 'forward':
        amt[:,fidx] = - tdq / df
      elif self.jac_method == 'central':
        tf = copy.deepcopy(fn)
        tf[fidx] -= df
        btq = self.compute_q(tf, dir_str='jac')
        bdq = self.qerror_vec(btq)
        amt[:,fidx] = - (tdq - bdq) / (2 * df)
      
    return amt

  def compute_svn_inv(self, amt):
    svd_coff = self.input.get('svd_coff', 1e-4)    
    u, s, vh = sla.svd(amt)
    w_coff_mat = np.zeros([self.numknots,self.qpts])
    for sidx, sv in enumerate(s):
      if np.abs(sv) < svd_coff * np.abs(s[0]):
        w_coff_mat[sidx,sidx] = 0
      else:
        w_coff_mat[sidx,sidx] = 1.0/sv
    a_svd_inv = vh.transpose().dot(w_coff_mat.dot(u.transpose()))
    return a_svd_inv, u, s, vh, w_coff_mat

  def quasi_newton(self,):
    ns = self.qpts
    nk = self.numknots
    maxit = self.input.get('maxit',10)
    outputfile = "qsolver.out"
    tol = self.tol
    amt = np.zeros([ns,nk])
    fn = copy.deepcopy(self.f0)
    qn = self.compute_q(fn,dir_str='org')
    lastq = qn
    dq = self.qerror_vec(lastq, debug=False)
    lasterr = np.dot(dq,dq)
    if lasterr < tol:
      print("initial guess is within tolerance")
    for itr in range(maxit):
      with open(outputfile, 'a') as file:
        file.write(f"Begin iteration: {itr}\n")
        file.write(f"Last f: {fn}\n")
        file.write(f"Last q: {lastq}\n")
        file.write(f"Last err: {lasterr}\n")
      amt = self.compute_jac(fn)
      a_svd_inv, u, s, vh, w_coff_mat = self.compute_svn_inv(amt)
      da = a_svd_inv.dot(dq)
      with open(outputfile, 'a') as file:
        file.write(f"A matrix: {amt}")
        file.write(f"S matrix: {s}")
        file.write(f"w matrix: {w_coff_mat}")
        file.write(f"da vector: {da}")
      bestf = copy.deepcopy(fn)
      bestq = copy.deepcopy(lastq)
      besterr = copy.deepcopy(lasterr)
      better = False
      with open(outputfile, 'a') as file:
        file.write(f"Begin line search at: {itr}\n")
      for litr, lenda in enumerate(np.linspace(1.e-4,1,num=11,endpoint=True)):
    #  for litr, lenda in enumerate(np.geomspace(1e-4,1,num=21,endpoint=True)):
        tf = fn + lenda * da
        tq = self.compute_q(tf)
        if np.inf in tq:
          terr = np.inf
          break
        else:
          tdq = self.qerror_vec(tq)
          terr =np.dot(tdq,tdq)
        with open(outputfile, 'a') as file:
          file.write(f"Error at {litr} is {terr}\n")
        if terr < besterr:
          with open(outputfile, 'a') as file:
            file.write(f"A better solution found at linesearch: {litr}\n")
            file.write(f"Error at {litr} is {terr}\n")
          better = True
          besterr = copy.deepcopy(terr)
          bestf = copy.deepcopy(tf)
          bestq = copy.deepcopy(tq)
          bestdq = copy.deepcopy(tdq)
      if not better:
        with open(outputfile, 'a') as file:
          file.write(f"No convergence at step {itr}\n")
        print(f"No convergence at step {itr}\n")
        break
      else:
        fn = copy.deepcopy(bestf)
        lasterr = copy.deepcopy(besterr)
        lastq = copy.deepcopy(bestq) #fix
        dq = copy.deepcopy(bestdq)
      if lasterr < tol:
        with open(outputfile, 'a') as file:
          file.write(f"convergence at step {itr}\n")
        print(f"convergence at step {itr}\n")
        break
    return fn, lasterr


def qsolver(inputFile, args):
  this = QSolver(inputFile)
  this.readQfile()
  this.initFileList()
  this.initalize_f()
  optdict = {'maxiter': 10, 'stepmx' : 20}
  bounds = []
  bestf, err = this.quasi_newton()
  qn = this.compute_q(bestf,dir_str='final')

if __name__ == '__main__':
  parser = argparse.ArgumentParser(description="Driver for qsolver fgnimeq")
  parser.add_argument('file',help='yaml input file')
  args = vars(parser.parse_args())
  qsolver(inputFile=args['file'], args=args)
  



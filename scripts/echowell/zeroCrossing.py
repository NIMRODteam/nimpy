#!/usr/bin/env python3
from plot_nimrod import PlotNimrod as pn
import f90nml
from eval_nimrod import *
from field_class import *
from fsa import *
import matplotlib.pyplot as pl
from scipy.interpolate import interp1d,splev,UnivariateSpline
import os
import h5py
import sys
import numpy

# Do FSA
def dot(v1,v2):
  return v1[0]*v2[0]+v1[1]*v2[1]+v1[2]*v2[2]

def basefsa(rzc,y,dy,evalnimrod,fargs):
    '''
    Flux surface averge quantities (f/bdgrth where y[2]=1/bdgrth)
    Set neq to number of outputs in FSA call
    and fill dy[4:4+neq]
    dy(0)=dl/deta or d eta/dl
    dy(1)=dr/deta or dr/dl
    dy(2)=1/bdgth
    dy(3)=dq
    '''
    isurf=fargs['isurf']
    veq = eval_nimrod.eval_field('v',rzc,dmode=0,eq=1) #eq
    v0 = eval_nimrod.eval_field('v', rzc, dmode=0, eq=3) #eq + n0
    beq = eval_nimrod.eval_field('b', rzc, dmode=0, eq=1)
    b0 = eval_nimrod.eval_field('b', rzc, dmode=0, eq=3)
    n0 = eval_nimrod.eval_field('n',rzc,dmode=0,eq=3)
    Te0 = eval_nimrod.eval_field('te',rzc,dmode=0,eq=3)
    p0 = eval_nimrod.eval_field('p',rzc,dmode=0,eq=3)
    j0 = eval_nimrod.eval_field('j',rzc,dmode=0,eq=3)

    beq=b0
    dy[4] = veq[2]*dy[2]/rzc[0]
    dy[5] = v0[2]*dy[2]/rzc[0]


    modBp = numpy.sqrt(beq[0]*beq[0]+beq[1]*beq[1])
    vpeq = (veq[0]*beq[0]+veq[1]*beq[1])/modBp
    vp0 = (v0[0]*beq[0]+v0[1]*beq[1])/modBp
  #  modBeq=numpy.sqrt(dot(beq,beq))
    modB0 = numpy.sqrt(dot(b0,b0))
    jpar = dot(j0,b0)/modB0
    smallNum=1.0e-6
    if numpy.fabs(vpeq) < smallNum:
      vpeq=smallNum
    if numpy.fabs(vp0) < smallNum:
      print("FSA small num")
      print(vp0)
      vp0=smallNum

    dy[6] = dy[2]/vpeq
    dy[7] = dy[2]/vp0
    dy[8] = n0*dy[2]
    dy[9] = Te0*dy[2]/rzc[0]
    dy[10] = p0*dy[2]/rzc[0]
    dy[11]=jpar*dy[2]/rzc[0]
#    dy[6:7]=dy[4:5]
    bigr[isurf] = max(bigr[isurf], rzc[0])
    return dy

# collect output here
rhon = numpy.arange(.05,1.0,0.01)
yrhon = []
qrhon = []

# load dump files
step_number_list = ['20000','40000','80000','120000']
#step_number_list = ['68000']
time_list = []
for step_number in step_number_list:
    dumpfile = "/Volumes/Auburn/ResearchStorage/174446_mu_shape_analysis/21061101/fsa_dumps/"+'/dumpgll.'+step_number+'.h5'
    print(dumpfile)
    infile = h5py.File(dumpfile,'r')
    time_list.append(infile["/dumpTime"].attrs.get("vsTime")*1.e3) # in ms
    nml = f90nml.read("/Volumes/Auburn/ResearchStorage/174446_mu_shape_analysis/21061101/fsa_dumps/nimrod.in")
    gmt = nml['grid_input']['geom']
    if gmt == 'tor':
        gmt=True
    else:
        gmt=False
    eval_nimrod = EvalNimrod(dumpfile, fieldlist='nvptbj') #, coord='xyz')

    md=3.3435860e-27
    zc=6
    mc=1.9944235e-26
    echrg=1.609e-19
    kboltz=echrg
    kb=1.609e-19
    eps0=8.85418782e-12
    rzo=[1.768, -0.018831, 0]

    nsurf = 150 # FSA surfaces
    bigr = numpy.zeros([nsurf])
    bigr[:] = -numpy.inf # max

    fsafilename = 'fsa'+step_number+'.npz'
    if os.path.exists(fsafilename):
        fsaDict = numpy.load(fsafilename)
        dvar = fsaDict['arr_0']
        yvars = fsaDict['arr_1']
        Gcontours = fsaDict['arr_2']
        bigr = fsaDict['arr_3']
    else:
        dvar, yvars, contours = FSA(eval_nimrod, rzo, basefsa, 8, nsurf=nsurf, \
                                    depvar='eta', dpow=0.5, rzx=None,normalize=False)
        #normalize
        yvars[0,:]=yvars[0,:]/dvar[6,:] #omega_T eq
        yvars[1,:]=yvars[1,:]/dvar[6,:] #omega_T 0
        yvars[2,:]=2.0*numpy.pi/yvars[2,:] #omega_P eq
        yvars[3,:]=2.0*numpy.pi/yvars[3,:]  #omega_P 0
        yvars[4,:]=yvars[4,:]/dvar[6,:]
        yvars[5,:]=yvars[5,:]/dvar[6,:]
        yvars[6,:]=yvars[6,:]/dvar[6,:]
        yvars[7,:]=yvars[7,:]/dvar[6,:]
        print (yvars.shape)
        fsaArr = [dvar, yvars, contours, bigr]
        numpy.savez(fsafilename,*fsaArr)

    #import IPython; IPython.embed()

    # Determine where the FSA failed
    iend=-1
    while numpy.isnan(yvars[:,iend]).any():
        iend -= 1
    iend += yvars.shape[1]+1

    # set fields as views to the arrays
    #ist=80
    #ne0 = yvars[0,ist:iend]
    #ne = yvars[1,ist:iend]
    #ti0 = yvars[2,ist:iend]
    #ti = yvars[3,ist:iend]
    #te0 = yvars[4,ist:iend]
    #te = yvars[5,ist:iend]
    #omega = yvars[6,ist:iend]
    #kpol = yvars[7,ist:iend]
    #rhon = dvar[1,ist:iend]
    #psi = dvar[2,ist:iend]
    #psix = dvar[2,-1]
    #q = numpy.fabs(dvar[7,ist:iend])
    #bigr = bigr[ist:iend]

    print(numpy.min(dvar[1,:iend]))
    print(numpy.max(dvar[1,:iend]))
    print(iend)

    yrhon.append(interp1d(dvar[1,:iend], yvars[:,:iend], kind='cubic')(rhon))
    qrhon.append(interp1d(dvar[1,:iend], dvar[7,:iend], kind='cubic')(rhon))

ii=0
uc=1.e-3
pn.plot_scalar_line(None, qrhon[0], flabel=f"{time_list[0]:.4f}",
                    f2=qrhon[1], f2label=f"{time_list[1]:.4f}",
                    f3=qrhon[2], f3label=f"{time_list[2]:.4f}",
                    f4=qrhon[3], f4label=f"{time_list[3]:.4f}",
                    xvar=rhon, xlabel=r'$\rho_N$', ylabel=r'$q$',legend_loc='upper right')



ii=0
uc=1.e-3
pn.plot_scalar_line(None, uc*yrhon[0][ii], flabel=f"{time_list[0]:.4f}",
                    f2=uc*yrhon[1][ii], f2label=f"{time_list[1]:.4f}",
                    f3=uc*yrhon[2][ii], f3label=f"{time_list[2]:.4f}",
                    f4=uc*yrhon[3][ii], f4label=f"{time_list[3]:.4f}",
                    xvar=rhon, xlabel=r'$\rho_N$', ylabel=r'$\Omega_t Eq$',legend_loc='upper right')

ii=1
uc=1.
pn.plot_scalar_line(None, uc*yrhon[0][ii], flabel=f"{time_list[0]:.4f}",
                    f2=uc*yrhon[1][ii], f2label=f"{time_list[1]:.4f}",
                    f3=uc*yrhon[2][ii], f3label=f"{time_list[2]:.4f}",
                    f4=uc*yrhon[3][ii], f4label=f"{time_list[3]:.4f}",
                    xvar=rhon, xlabel=r'$\rho_N$', ylabel=r'$\Omega_t$ n=0',legend_loc='lower left')

ii=2
uc=1.
pn.plot_scalar_line(None, uc*yrhon[0][ii], flabel=f"{time_list[0]:.4f}",
                    f2=uc*yrhon[1][ii], f2label=f"{time_list[1]:.4f}",
                    f3=uc*yrhon[2][ii], f3label=f"{time_list[2]:.4f}",
                    f4=uc*yrhon[3][ii], f4label=f"{time_list[3]:.4f}",
                    xvar=rhon, xlabel=r'$\rho_N$', ylabel=r'$\Omega_p$ Eq',legend_loc='upper right')

ii=3
uc=1.
pn.plot_scalar_line(None, uc*yrhon[0][ii], flabel=f"{time_list[0]:.4f}",
                    f2=uc*yrhon[1][ii], f2label=f"{time_list[1]:.4f}",
                    f3=uc*yrhon[2][ii], f3label=f"{time_list[2]:.4f}",
                    f4=uc*yrhon[3][ii], f4label=f"{time_list[3]:.4f}",
                    xvar=rhon, xlabel=r'$\rho_N$', ylabel=r'$\Omega_p$ n=0',legend_loc='upper left')

ii=4
uc=1.
pn.plot_scalar_line(None, uc*yrhon[0][ii], flabel=f"{time_list[0]:.4f}",
                    f2=uc*yrhon[1][ii], f2label=f"{time_list[1]:.4f}",
                    f3=uc*yrhon[2][ii], f3label=f"{time_list[2]:.4f}",
                    f4=uc*yrhon[3][ii], f4label=f"{time_list[3]:.4f}",
                    xvar=rhon, xlabel=r'$\rho_N$', ylabel=r'$n$ ',legend_loc='upper left')

ii=5
uc=1.
pn.plot_scalar_line(None, uc*yrhon[0][ii], flabel=f"{time_list[0]:.2f}",
                    f2=uc*yrhon[1][ii], f2label=f"{time_list[1]:.2f}",
                    f3=uc*yrhon[2][ii], f3label=f"{time_list[2]:.2f}",
                    f4=uc*yrhon[3][ii], f4label=f"{time_list[3]:.2f}",
                    xvar=rhon, xlabel=r'$\rho_N$', ylabel=r'$Te$ ',legend_loc='upper right')

ii=6
uc=1.
pn.plot_scalar_line(None, uc*yrhon[0][ii], flabel=f"{time_list[0]:.2f}",
                    f2=uc*yrhon[1][ii], f2label=f"{time_list[1]:.2f}",
                    f3=uc*yrhon[2][ii], f3label=f"{time_list[2]:.2f}",
                    f4=uc*yrhon[3][ii], f4label=f"{time_list[3]:.2f}",
                    xvar=rhon, xlabel=r'$\rho_N$', ylabel=r'$P$ ',legend_loc='upper right')

ii=7
uc=1.
pn.plot_scalar_line(None, uc*yrhon[0][ii], flabel=f"{time_list[0]:.2f}",
                    f2=uc*yrhon[1][ii], f2label=f"{time_list[1]:.2f}",
                    f3=uc*yrhon[2][ii], f3label=f"{time_list[2]:.2f}",
                    f4=uc*yrhon[3][ii], f4label=f"{time_list[3]:.2f}",
                    xvar=rhon, xlabel=r'$\rho_N$', ylabel=r'$J_\parallel$ ',legend_loc='upper right')

# TripToNim
A collection of scripts for interfacing between NIMROD and trip3D/probe_g 

## tripClass.py
  - Base class for storing and reading trip3D/probe_g data

### Status;
  - Works robustly

### Todo:
  - [ ] Add tests
  - [ ] Check logic at maxnphi (in practice I never use maxnphi)
  - [ ] Implement or remove readRZ
  - [ ] Implement or remove readAFile
  - [ ] Remove 

## tripToNim.py
  - Reads trip3D/probe_g magnetic fields and writes NIMROD brmp file'''

### Status:
  - Simple interface for running tripClass
  - Works but needs to be cleaned up

### Todo:
  - [ ] Clean up
  - [ ] Add yaml input file

## writeRmpToDump.py
  - Write RMPs directly to dump file 
  
### Status:
  - Unknown, I have not used recently
  - In future, this will be very useful functionality

### Todo:
  - [ ] Test to determine if it works
  - [ ] Update todo list

## write_probe_points.py
  - Writes a probe_g probe.points.rz.in input file using nimrod boundary nodes 

### Status:
  - Code runs and works robustly

### Inputs:
  - NIMROD nimrod_bdry_rz.txt file

### Outputs:
  - probe.points.rz.in

### Todo:
  - [ ] Add yaml input
  - [ ] Add test

import argparse
import os
import tripClass as tc


''' Read external magnetic fields from trip3D output files
    and converts it to NIMROD brmpn files '''


def convert_data(rzfile: str, afile: str, bfile: str) -> None:

    INDEXSHIFT = 0  # was 5
    COMPLEXCON = True
    nimrodRmpFile = "brmpn"
    nimrodRmpSuffix = ".dat"
    writeDirectory = os.getcwd() + '/'
    tripData = tc.TripClass(rzfile, afile, bfile, INDEXSHIFT, COMPLEXCON)
    tripData.processBFile()
    tripData.writeNimrodBext(writeDirectory, nimrodRmpFile, nimrodRmpSuffix)


def parse_arguments() -> dict:
    '''
    Parse command line arguments

    Inputs
    ------
    None

    Outputs
    -------
    args dictionary
    '''
    msg = 'Covert ProbeG B field data to NIMROD brmp## format'
    parser = argparse.ArgumentParser(
                    prog='tripToNim',
                    description=msg,
                    usage="%(prog)s [options]")
    parser.add_argument('--rzfile', '-r', default='probe.points.rz.in',
                        help="probe points rz file")
    parser.add_argument('--afile', '-a', default='probe_ga.out',
                        help="probe ga out file")
    parser.add_argument('--bfile', '-b', default='probe_gb.out',
                        help="probe gb out file")
    args = parser.parse_args()
    return args


def main():
    args = parse_arguments()
    convert_data(args.rzfile, args.afile, args.bfile)


if __name__ == "__main__":
    main()

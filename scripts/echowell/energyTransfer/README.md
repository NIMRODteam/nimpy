# energy
A collection of scripts for performing the Fourier energy transfer analysis

## etFields.py
 - class for reading/storing nimrod data on a mesh
 - compute terms in energy analysis

## etMult.py
  - script for applying volume energy transfer analysis to multiple dumpfiles

## etPlot.py
  - script for plotting the result of the energy transfer analysis

## etRunner.py
  - script for applying volume energy transfer analysis to one dumpfiles

## etRunnerFsa.py
  - script for applying fsa energy transfer analysis to one dumpfile

## etStep.py
  - class for doing volume integrated ET analysis at a time step 

## etStepFsa.py
  - class for doing flux surface integrated ET analysis at a time step

## nim_timer.py
  - A timer class for profiling python scripts

### USAGE
  - use @nim_timer.timer_func before function def

### Status 
  - Works robustly
  - There are more powerful python decorator time classes

### Todo:
  - [ ]  Move to general util

## General Todo
  - [ ] Add tests
  - [ ] Test with new eval
  - [ ] Clean up
  - [ ] Test with realistic wall and mu shaping

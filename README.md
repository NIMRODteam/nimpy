## nimpy

Python utilities for NIMROD.

### Installation

#### Install nimpy using venv

At NERSC skip the next section and use conda.

It is recommended to use python virtual environments, but the first and second
steps can be skipped for system-wide installation.  It is recommended that
these commands are executed in the [nimpack](https://gitlab.com/NIMRODteam/nimpack)
directory, if used, or in a directory adjacent to the nimpy source.

``` console
$ python -m venv ./nimrod_venv
$ source ./nimrod_venv/bin/activate
$ python -m pip install --upgrade pip # Only needed once
$ pip install -e <path to nimpy source root>
```
Installing with the `-e` option makes changes to the source immediately
available within the install.

Testing and developer dependencies can be installed with
``` console
$ pip install -e <path to nimpy source root>[test,dev]
```
Sourcing the activate script is required with each new terminal. It could be
added to your .bash\_rc file.

#### Install nimpy using conda

Commands for a conda environment follow, as appropriate for NERSC. It is
recommended that these commands are executed in the
[nimpack](https://gitlab.com/NIMRODteam/nimpack) directory, if used, or in
a directory adjacent to the nimpy source.

``` console
$ module load conda
$ conda create --prefix ./nimrod_conda_env
$ conda activate ./nimrod_conda_env
$ conda install pip
$ pip install -e <path to nimpy source root>
```
The module load and activate command is required with each new terminal. It
could be added to your .bash\_rc file.

### Usage

On Linux, set the environment variable `LD_LIBRARY_PATH` to the lib directory in
your serial nimdevel installation. This will find the required libraries for
some nimpy functionality.

Examples are includes in the `scripts/` directory. Create a custom python
script - potentially starting from an example.

Installed commands (invoke with -h for command line argument help):
- `nimplot2D` - plot field contours in an RZ plane (uses element interpolation)
- `nimplotfield` - quick plotting of field contours in an RZ plane (uses nodal
  data)
- `nimglobaleq` - calculate standard equilibrium quantities

API Documentation is generated with Sphinx:
[https://nimrodteam.gitlab.io/open/nimpy/index.html](https://nimrodteam.gitlab.io/open/nimpy/index.html).

Documentation is generated with Sphinx.

Requires pypi packages sphinx-mdinclude and sphinx-math-dollar.

Execute these commands in the root project directory to build the docs:
''' console
> sphinx-apidoc -F -o docs/ nimpy
> sphinx-build -b html ./docs/ public
'''

Use '#noqa' at the end of a doc string to prevent flake8 checking.

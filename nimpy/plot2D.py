import argparse
import f90nml
import h5py
import matplotlib.pyplot as plt
from matplotlib import gridspec
import numpy as np
import os
from nimpy.eval_nimrod import EvalNimrod, EvalGrid
from nimpy.field_class import Scalar
from nimpy.plot_nimrod import PlotNimrod as pn


def __format_plot(ax, plot_params, lower=True, left=True):
    '''
    Helper function to format plots by adding boundaries and contours of
    interest
    '''
    # unpack plot_params
    rzo = plot_params[0]
    rzx = plot_params[1]
    plot_limits = plot_params[2]
    rmin = plot_limits[0][0]
    rmax = plot_limits[0][1]
    zmin = plot_limits[1][0]
    zmax = plot_limits[1][1]
    psin_eq = plot_params[3]
    RZsol = plot_params[4]
    RZsep = plot_params[5]
    RZwall = plot_params[6]
    grid = plot_params[7]

    ax.set_xlim([rmin, rmax])
    ax.set_ylim([zmin, zmax])
    ax.ticklabel_format(axis='both', style='sci', scilimits=(10**3, 10**-3),
                        useOffset=None, useLocale=None, useMathText=True)

    # add contours
    if psin_eq:
        psin_labels = [0.2, 0.4, 0.6, 0.8, 0.9, 0.95,
                       1.0, 1.2, 1.4, 1.6, 1.8]
        ax.contour(grid[0], grid[1], psin_eq.data, psin_labels,
                   alpha=0.5, colors='k', zorder=90)
    else:
        if RZsol is not None:
            ax.plot(RZsol[:, 0], RZsol[:, 1], alpha=0.5, color='k')
        if RZsep is not None:
            ax.plot(RZsep[:, 0], RZsep[:, 1], alpha=0.5, color='k')
    if rzo:
        ax.plot(rzo[0], rzo[1], 'o')
    if rzx:
        ax.plot(rzx[0], rzx[1], 'x')
    if RZwall is not None:
        ax.plot(RZwall[:, 0], RZwall[:, 1], color='k')

    # set labels
    if lower:
        ax.set(xlabel="R (m)")
    else:
        ax.set_xticklabels([])
    if left:
        ax.set(ylabel="Z (m)")
    else:
        ax.set_yticklabels([])
    return ax


def main():
    '''
    Plot 2D fields, installed a `nimplot2D`. See `nimplot2D -h`.
    '''
    parser = argparse.ArgumentParser(usage="%(prog)s [options] h5 file")
    parser.add_argument('-c', '--ncol', dest='ncol', type=int,
                        help='Number of plot columns', default=-1)
    parser.add_argument('-d', '--diverted', action='store_true',
                        dest='diverted',
                        help='If diverted plot o/x points', default=False)
    parser.add_argument('-e', '--eq', dest='eq', type=int,
                        help='Option to add eq fields: 0 - perturbation;'
                        + ' 1 - equilibrium;'
                        + ' 2 - equilibrium + perturbation;'
                        + ' 3 - equilibrium + n0 perturbation;'
                        + ' 4 - perturbation - n0 perturbation',
                        default=0)
    parser.add_argument('-f', '--fields', dest='fields',
                        help='Comma separated list of fields to plot '
                        + 'append quantity index as f1_#,f2_#,... ',
                        default='n_0')
    parser.add_argument('-g', '--norm', dest='norm', type=int,
                        help='Nomalization (optional) values the same as eq',
                        default=-1)
    parser.add_argument('-i', '--imode', dest='imode', help='Mode #',
                        type=int, default=0)
    parser.add_argument('-l', '--cbar', action='store_true',
                        dest='force_cbar', help='Force color bar plotting',
                        default=False)
    parser.add_argument('-p', '--phi', dest='phi', help='Toroidal angle',
                        type=float, default=-1.)
    parser.add_argument('-s', '--phase', dest='phase',
                        help='Phase for complex plots in degrees',
                        type=float, default=0.)
    parser.add_argument('-x', '--nxpts', dest='nx',
                        help='Plot resolution in x',
                        type=int, default=250)
    parser.add_argument('-y', '--nypts', dest='ny',
                        help='Plot resolution in y',
                        type=int, default=250)
    parser.add_argument('file', metavar='dump file',
                        help='File to process')
    args = vars(parser.parse_args())

    if not args['file']:
        parser.print_help()
        return
    else:
        dumpfile = args['file']
        if not os.path.isfile(dumpfile):
            print('Skipping absent file '+dumpfile)
            return

    # parse namelist
    nml = f90nml.read('nimrod.in')
    gmt = nml['grid_input'].get('geom', 'tor')
    if gmt == 'tor':
        gmt = True
    else:
        gmt = False

    # read contours.h5
    success = False
    if os.path.isfile('contours.h5'):
        try:
            confile = h5py.File('contours.h5', 'r')
            RZsol = confile['/sol/points']
            RZsep = confile['/LCFS/points']
            RZwall = confile['/wall/points']
            RZwall = np.vstack([RZwall, RZwall[0, :]])
            rmax = RZwall[:, 0].max()
            rmin = RZwall[:, 0].min()
            zmax = RZwall[:, 1].max()
            zmin = RZwall[:, 1].min()
            success = True
        except Exception:
            success = False
    elif os.path.isfile('sol.grn'):
        try:
            with open('sol.grn', 'r') as file:
                lines = file.readlines()
            nsep = int(lines[0].split()[1])
            RZsol = np.zeros([nsep, 2])
            for idx in range(nsep):
                RZsol[idx, 0] = float(lines[1].split()[idx])
                RZsol[idx, 1] = float(lines[3].split()[idx])
            nsep = int(lines[4].split()[1])
            RZsep = np.zeros([nsep, 2])
            for idx in range(nsep):
                RZsep[idx, 0] = float(lines[5].split()[idx])
                RZsep[idx, 1] = float(lines[7].split()[idx])
            nsep = int(lines[8].split()[1])
            RZwall = np.zeros([nsep, 2])
            for idx in range(nsep):
                RZwall[idx, 0] = float(lines[9].split()[idx])
                RZwall[idx, 1] = float(lines[11].split()[idx])
            rmax = RZwall[:, 0].max()
            rmin = RZwall[:, 0].min()
            zmax = RZwall[:, 1].max()
            zmin = RZwall[:, 1].min()
            success = True
        except Exception:
            success = False

    if not success:
        rmax = nml['grid_input'].get('xmax', 1)
        rmin = nml['grid_input'].get('xmin', 0)
        zmax = nml['grid_input'].get('ymax', 1)
        zmin = nml['grid_input'].get('ymin', 0)
        RZsol = None
        RZsep = None
        RZwall = None
    plot_limits = [[rmin, rmax], [zmin, zmax]]

    # parse plot options
    plot_list = args['fields'].split(',')
    nplots = len(plot_list)
    field_list = []
    iq_list = []
    for plot in plot_list:
        arr = plot.split('_')
        field_list.append(arr[0])
        iq_list.append(int(arr[1]))
    field_list_str = ''.join(set(field_list))
    eq_plt = args['eq']
    norm_plt = args['norm']
    phase = np.exp(1j*args['phase']*np.pi/180.)
    imode = args['imode']
    if nplots < 4 or args['force_cbar']:
        cbar = True
    else:
        cbar = False

    # create eval instance
    eval_nimrod = EvalNimrod(dumpfile, fieldlist=field_list_str)

    # setup the plot frames
    pn.set_pyplot_params()
    aspect_ratio = (rmax-rmin)/(zmax-zmin)
    screen_ratio = 16/9
    if args['ncol'] > 0:
        ncol = args['ncol']
    else:
        ncol = int(nplots//(screen_ratio*aspect_ratio)+1)
    nrow = int(-1*(-nplots//ncol))  # ceiling division
    margfac = 0.15
    vmargfac = 0.15
    hmargfac = 0.2
    fig = plt.figure(figsize=(16, 9))
    gs = gridspec.GridSpec(nrow, ncol,
                           wspace=margfac, hspace=margfac,
                           top=1.-vmargfac/(nrow+2*vmargfac),
                           bottom=vmargfac/(nrow+2*vmargfac),
                           left=hmargfac/(ncol+2*hmargfac),
                           right=1.-hmargfac/(ncol+2*vmargfac))

    # setup the plot grid
    nx = args['nx']
    ny = args['ny']
    if args['phi'] >= 0:
        grid = pn.grid_2d_gen([rmin, zmin, args['phi']],
                              [rmin, zmax, args['phi']],
                              [rmax, zmin, args['phi']], nx, ny)
    else:
        grid = pn.grid_2d_gen([rmin, zmin, 0], [rmin, zmax, 0],
                              [rmax, zmin, 0], nx, ny)

    # intialize eval grid
    eval_grid = \
        EvalGrid(grid, filename=f'logical_map_{nx}_{ny}.pickle')
    eval_grid.set_logical_grid(field_list[0], eval_nimrod)

    # determine flux contours if appropriate
    if args['diverted']:
        # TODO: store all of this in FSA class
        print('Finding x/o points')
        from nimpy.fsa import find_xpoint, find_opoint
        rzo = find_opoint(eval_nimrod)
        rzx = find_xpoint(eval_nimrod)
        print('O-point at ', rzo, ' X-point at ', rzx)
        # TODO: add ability to use FSA to generate updated contours
        psio = eval_nimrod.eval_field('psi', rzo, dmode=0, eq=1)
        psix = eval_nimrod.eval_field('psi', rzx, dmode=0, eq=1)
        fval = eval_nimrod.eval_field('psi', eval_grid, dmode=0, eq=1)
        psin_eq = Scalar((fval-psio[0])/(psix[0]-psio[0]), grid,
                         torgeom=gmt, dmode=0)
    else:
        rzo = None
        rzx = None
        psin_eq = None

    # store values
    plot_params = [rzo, rzx, plot_limits, psin_eq, RZsol, RZsep, RZwall, grid]

    print('Making plots')
    field_last = None
    for iplt in range(nplots):
        iq = iq_list[iplt]
        field = field_list[iplt]
        irow = int(iplt/ncol)
        icol = iplt % ncol
        ax = plt.subplot(gs[irow, icol])
        lower = eval("irow == nrow-1")
        left = eval("icol == ncol-1")
        __format_plot(ax, plot_params, lower, left)
        if args['phi'] >= 0:
            if field != field_last:
                fval = eval_nimrod.eval_field(field, eval_grid, dmode=0,
                                              eq=eq_plt)
                if norm_plt > 0:
                    fval0 = eval_nimrod.eval_field(field, eval_grid, dmode=0,
                                                   eq=norm_plt)
                    fval = fval[:, :, :]/fval0[:, :, :]
                field_last = field
            pn.plot_scalar_plane(grid, fval[iq, :, :], title=plot_list[iplt],
                                 ax=ax, show=False, cbar=cbar)
        else:
            if field != field_last:
                cfval = eval_nimrod.eval_comp_field(field, eval_grid, dmode=0)
                if norm_plt > 0:
                    fval0 = eval_nimrod.eval_field(field, eval_grid, dmode=0,
                                                   eq=norm_plt)
                    cfval = cfval[:, imode, :, :]/fval0[:, :, :]
                field_last = field
            pn.plot_scalar_plane(grid, (cfval[iq, imode, 0, :, :]*phase).real,
                                 title=plot_list[iplt],
                                 ax=ax, show=False, cbar=cbar)

    plt.show()
    plt.close(fig)


if __name__ == "__main__":
    main()

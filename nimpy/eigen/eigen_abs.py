from abc import ABC, abstractmethod
import pickle
import numpy as np
from scipy.integrate import solve_ivp
import sympy
import sympy.vector


# Define convenience functions to reduce verboisty.


def curl(a):
    '''
    Convenience curl function.
    '''
    return sympy.vector.curl(a)


# Convenience del operator
delop = sympy.vector.Del()


def div(a):
    '''
    Convenience divergence function.
    '''
    return sympy.vector.divergence(a)


def grad(a):
    '''
    Convenience gradient function.
    '''
    return sympy.vector.gradient(a)


class WaveAnalysis(ABC):
    '''
    Abstract class for performing a plane-wave analysis.
    '''

    def __init__(self, compute_lin_mat=False):
        '''
        Basic initialization

        For an n-step method, the j-th step is defined
        L_j dU_j = f_j dt M_j U_{j-1}
        U_j = dU_j + U_{j-1}

        '''
        sympy.init_printing()
        self.verbosity = 0
        self.compute_linmat = compute_lin_mat
        self.lin_mat = None
        self.lam_lin = None
        self.MList = []
        self.LList = []
        self.fList = []
        self.odeList = []
        self.lam_MList = []
        self.lam_LList = []
        self.odemat = None
        self.field_names = []

    def compute_analytic_eigenvalues(self, karr, phiarr):
        '''
        Compute the analytic eigenvalues of the linear system
        '''
        print("Calculating the analytic dispersion relation...")
        nphi = np.size(phiarr)
        nk = np.size(karr)
        nrows = self.lin_mat.rows
        omega = np.zeros([nphi, nk, nrows], dtype=np.complex128)
        for iphi, phi in enumerate(phiarr):
            for ik, kval in enumerate(karr):
                eigenv = -1j * np.linalg.eigvals(self.lam_lin_mat(kval, phi))
                omega[iphi, ik, :] = np.sort_complex(eigenv)
        return omega

    def compute_eigen_vectors(self, kval, phival):
        '''
        Compute the eigenvectors associated with the numerical time step
        '''
        print("Calculating the dispersion relation...")
        nrows = self.MList[0].rows
        imat = np.identity(nrows)
        dt_ = self.subdict[self.dt]
        mat_calc = imat.copy()
        for ll, ff, lm, odef in zip(self.lam_LList,
                                    self.fList,
                                    self.lam_MList,
                                    self.odeList):
            lmat = ll(kval, phival)
            mmat = lm(kval, phival)
            if self.verbosity > 1:
                print(lmat)
                print(mmat)
            if odef:
                tmat = np.dot(np.linalg.inv(lmat), mmat)
                amat = self.integarte_ode(tmat, dt_ * ff)
                if self.verbosity > 1:
                    print(amat)
                mat_calc = np.dot(amat, mat_calc)
            else:
                amat = np.dot(np.linalg.inv(lmat), mmat) * dt_ * ff
                mat_calc = np.dot(imat + amat, mat_calc)
        eigenvalues, eigenvectors = np.linalg.eig(mat_calc)  # lambda
        eigenvalues = -1j * np.log(eigenvalues) / dt_   # omega
        return eigenvalues, eigenvectors

    def compute_numerical_eigenvalues(self, karr, phiarr):
        '''
        Compute the eigenvalues associated with the numerical time step
        '''
        print("Calculating the dispersion relation...")
        nphi = np.size(phiarr)
        nk = np.size(karr)
        nrows = self.MList[0].rows
        imat = np.identity(nrows)
        omega = np.zeros([nphi, nk, nrows], dtype=np.complex128)
        dt_ = self.subdict[self.dt]
        for iphi, phi in enumerate(phiarr):
            for ik, kval in enumerate(karr):
                mat_calc = imat.copy()
                for ll, ff, lm, odef in zip(self.lam_LList,
                                            self.fList,
                                            self.lam_MList,
                                            self.odeList):
                    lmat = ll(kval, phi)
                    mmat = lm(kval, phi)
                    if self.verbosity > 1:
                        print(lmat)
                        print(mmat)
                    if odef:
                        tmat = np.dot(np.linalg.inv(lmat), mmat)
                        amat = self.integarte_ode(tmat, dt_ * ff)
                        if self.verbosity > 1:
                            print(amat)
                        mat_calc = np.dot(amat, mat_calc)
                    else:
                        amat = np.dot(np.linalg.inv(lmat), mmat) * dt_ * ff
                        mat_calc = np.dot(imat + amat, mat_calc)
                eigenv = np.linalg.eigvals(mat_calc)  # lambda
                eigenv = -1j*np.log(eigenv)/dt_   # omega
                omega[iphi, ik, :] = np.sort_complex(eigenv)
        return omega

    def create_matrices(self, model):
        '''
        Create the operator matrices as sympy expressions
        and store in lists.
        '''
        outstr = "Analyzing " + model
        print(outstr)
        self._define_symbols()
        self._get_lhs_mat()
        self._get_rhs_mat()
        self._get_rhs_fac()
        self._get_ode_list()
        if self.compute_linmat:
            self._get_lin_mat()
        return

    def integarte_ode(self, mmat, dt):
        '''
        Numerically compute the propegator matrix of mmat for a timestep dt
        '''
        neq = mmat.shape[0]
        tspan = (0, dt)
        prop = np.zeros([neq, neq])
        method = 'LSODA'
        rtol = 1.e-12
        atol = 1.e-12
        for idx in range(neq):
            y0 = np.zeros([neq])
            y0[idx] = 1
            self.odemat = mmat
            sol = solve_ivp(self.ode_func, tspan, y0, method=method,
                            rtol=rtol, atol=atol)
            yf = sol.y[:, -1]
            prop[:, idx] = yf
        return prop

    def lambdify_mats(self):
        '''
        Create lambdified matrices that depend on kmag, phik
        '''
        print("Lambdifying expressions...")
        sublist = [(k, v) for k, v in self.subdict.items()]
        self.lam_MList = []
        self.lam_LList = []
        for mmat in self.MList:
            mmat = sympy.simplify(mmat.subs(sublist))
            lam_mmat = sympy.lambdify([self.kmag, self.phik], mmat, "numpy")
            self.lam_MList.append(lam_mmat)
        for lmat in self.LList:
            lmat = sympy.simplify(lmat.subs(sublist))
            lam_lmat = sympy.lambdify([self.kmag, self.phik], lmat, "numpy")
            self.lam_LList.append(lam_lmat)
        if self.compute_linmat:
            lin_mat = sympy.simplify(self.lin_mat.subs(sublist))
            self.lam_lin_mat = sympy.lambdify([self.kmag, self.phik],
                                              lin_mat, "numpy")

    def load(self, filename="wave_analysis.pickle"):
        '''
        Load matrices for reuse
        '''
        print(f"Reading matrices from {filename}")
        with open(filename, "rb") as pfile:
            self.MList = pickle.load(pfile)
            self.LList = pickle.load(pfile)
            self.fList = pickle.load(pfile)
            self.odeList = pickle.load(pfile)
            self.lin_mat = pickle.load(pfile)

    def ode_func(self, t, y):
        '''
        Callable function for solve_ivp that defines integrand of ode matrix
        solve_ivp defines call signature(t, y)
        odemat is set as a class variable to ease use with LSODA
        '''
        yf = np.matmul(self.odemat, y)
        return yf

    def save(self, filename="wave_analysis.pickle"):
        '''
        Save matrices for reuse
        '''
        print(f"Saving matrices to {filename}")
        with open(filename, "wb") as pfile:
            pickle.dump(self.MList, pfile, protocol=pickle.HIGHEST_PROTOCOL)
            pickle.dump(self.LList, pfile, protocol=pickle.HIGHEST_PROTOCOL)
            pickle.dump(self.fList, pfile, protocol=pickle.HIGHEST_PROTOCOL)
            pickle.dump(self.odeList, pfile, protocol=pickle.HIGHEST_PROTOCOL)
            pickle.dump(self.lin_mat, pfile, protocol=pickle.HIGHEST_PROTOCOL)

    def write_eigen_vector(self, kval=2, phival=np.pi/3.0,
                           idx=0, filename=None):
        '''
        The purpose of this step is to handle the communication of the eigen
        vector and values with NIMROD's CI testing

        Key Processes
        apply subdict
        Write names of perturbation fields
        Write eigenvector (1 or multiple)
        Write eigenvalue

        note the ith eigenvector is vector[:,ith]
        '''
        pass

    def _define_symbols(self):
        """
        Include variables that should be common to most models
        """
        self.dt = sympy.symbols("dt", positive=True)
        self.kmag = sympy.symbols("kmag", positive=True)
        self.thetak = sympy.symbols("theta_k", real=True)
        self.phik = sympy.symbols("phi_k", real=True)
        self.N = None
        self.spi = None
        self.subdict = None

    def _process_equations(self, eqnlist):
        '''
        Process equations into a matrix form
        Input is eqnlist which is a list-of-lists where is list is
        eqnlist[0] = [expr, type, variable, normalization]
        where for example for continuity
        expr is div(rho*v)
        type si scalar (or vector)
        variable is rho
        normalization is rho0
        Returns the matrix
        '''
        eqnlist = [list(x) for x in zip(*eqnlist)]
        eqexpr = eqnlist[0]
        eqtype = eqnlist[1]
        eqvars0 = eqnlist[2]
        eqnorm0 = eqnlist[3]

        # remove e^(ikx) dependence and assemble by component
        eqns = []
        eqvars = []
        eqnorm = []
        for ieqn, eqn in enumerate(eqexpr):
            eqn = eqn * self.spi
            if eqtype[ieqn] == 'vector':
                for unitvec in [self.N.i, self.N.j, self.N.k]:
                    eqns.append(eqn.dot(unitvec))
                    eqvars.append(eqvars0[ieqn].dot(unitvec)*self.spi)
                    eqnorm.append(eqnorm0[ieqn])
            else:
                eqns.append(eqn)
                eqvars.append(eqvars0[ieqn]*self.spi)
                eqnorm.append(eqnorm0[ieqn])
        mat = []
        for ieqv, eqvar in enumerate(eqvars):
            sublist = []
            for eqv in eqvars:
                if eqv is eqvar:
                    sublist.append((eqv, 1))
                else:
                    sublist.append((eqv, 0))
            row = []
            for ieqn, eqn in enumerate(eqns):
                entry = sympy.simplify(eqn.subs(sublist) * eqnorm[ieqv]
                                       / eqnorm[ieqn])
                row.append(entry)
            mat.append(row)
        mat = list(map(list, zip(*mat)))  # transpose
        return sympy.Matrix(mat)

    @abstractmethod
    def plot_dispersion(self):
        '''
        Abstract method for plotting dispersion relation
        '''
        pass

    @abstractmethod
    def _get_ode_list(self):
        '''
        Abstract method for determining which steps are "solved exactly"
        or to some numerical tolerance using LSODA
        '''
        pass

    @abstractmethod
    def _get_lin_mat(self):
        '''
        Abstract method for defining the analytic linear matrix
        '''
        pass

    @abstractmethod
    def _get_lhs_mat(self):
        '''
        Abstract method for setting LHS numerical matrices
        '''
        pass

    @abstractmethod
    def _get_rhs_fac(self):
        '''
        Abstract method for setting RHS numerical factors
        '''
        pass

    @abstractmethod
    def _get_rhs_mat(self):
        '''
        Abstract method for setting RHS numerical matrices
        '''
        pass

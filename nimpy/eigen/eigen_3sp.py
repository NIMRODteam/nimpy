import argparse
import matplotlib.pyplot as plt
import numpy as np
import sympy
from eigen_abs import delop, grad, curl, div, WaveAnalysis


class ThreeSpecies(WaveAnalysis):
    '''
    Class for performing plane-wave analysis of Three Species MHD system
    There are two ion and one electron species
    '''
    def __init__(self):
        super().__init__(compute_lin_mat=True)
        self.hall = False
        self.logplot = False
        self.ns = 3
        self.nis = self.ns - 1
        self.siop = 'split'
        nsname = []
        tsname = []
        vsname = []
        for idx in range(self.nis):
            nsname.append(f"ns{idx}")
            tsname.append(f"ts{idx}")
            vsname.append(f"vs{idx}")
        tsname.append("te")
        self.field_names.extend(vsname)
        self.field_names.extend(nsname)
        self.field_names.extend(tsname)
        self.field_names.append("b")
        print(self.field_names)

    def create_matrices(self):
        '''
        Create the linearized RHS matrix, M, and LHS matrix, L as sympy
        expressions
        '''
        model = "Multispecies "
        model += "3-species "
        model += "primitive-form "
        model += "equation system with "
        model += "leap-frog "
        model += "time discretization..."
        super().create_matrices(model)
        return

    def save(self, filename=None):
        '''
        Save matrices for future use
        '''
        if filename is None:
            filename = "threespsplit.pickle"
        super().save(filename=filename)

    def load(self, filename=None):
        '''
        Load matrices for future use
        '''
        if filename is None:
            filename = "threespsplit.pickle"
        super().load(filename=filename)

    def _define_symbols(self):
        '''
        Define basic symbols.

        TODO Currently the lists store electron quantities as the last
             element in a list. To be consistent with nimrod, they should
             be stored as the first element in a list
        '''
        super()._define_symbols()
        # Define numerical parameters
        self.N = sympy.vector.CoordSys3D('N')
        xv = self.N.x * self.N.i + self.N.y * self.N.j + self.N.z * self.N.k
        k = self.kmag * (
            (self.N.i * sympy.sin(self.thetak) +
             self.N.j*sympy.cos(self.thetak)) * sympy.sin(self.phik) +
            self.N.k * sympy.cos(self.phik))
        k = k.subs(self.thetak, 0)  # simplify
        sp = sympy.exp(sympy.I * k.dot(xv))
        self.spi = sympy.exp(-sympy.I * k.dot(xv))

        #  Define centering parameters
        self.th_si = sympy.symbols("th_si", nonnegative=True)
        self.th_ihy = sympy.symbols("th_ihy", nonnegative=True)
        self.th_adv = sympy.symbols("th_adv", nonnegative=True)
        self.th_d = sympy.symbols("th_d", nonnegative=True)
        self.th_b = sympy.symbols("th_b", nonnegative=True)
        self.th_o = sympy.symbols("th_o", nonnegative=True)

        # Define physical constants
        self.kb = sympy.symbols("kb", positive=True)
        self.mu0 = sympy.symbols("mu0", positive=True)
        self.Gamma = sympy.symbols("Gamma", positive=True)
        self.echrg = sympy.symbols("echrg", positive=True)

        # Define equilibrium
        B0mag = sympy.symbols("B0mag", positive=True)
        thetab, phib = sympy.symbols("theta_b phi_b", real=True)
        self.B0 = B0mag * (
            (self.N.i * sympy.sin(thetab) +
             self.N.j * sympy.cos(thetab)) * sympy.sin(phib) +
            self.N.k*sympy.cos(phib))
        self.B0 = self.B0.subs([(thetab, 0), (phib, 0)])  # simplify
        self.bhat0 = self.B0 / B0mag
        self.bnorm = B0mag
        self.va = sympy.symbols("va", positive=True)

        # Define perturbed fields
        bx, by, bz = sympy.symbols("bx by bz", real=True)
        self.bp = (bx*self.N.i + by*self.N.j + bz*self.N.k) * sp
        self.jp = curl(self.bp) / self.mu0

        self.Zilist = [_ for _ in sympy.symbols(f'z:{self.nis}',
                                                nonegative=True,
                                                integer=True)]
        self.milist = [_ for _ in sympy.symbols(f'm:{self.nis}',
                                                positive=True)]
        self.n0list = [_ for _ in sympy.symbols(f'nd:{self.nis}',
                                                positive=True)]
        ne = 0
        self.mnorm = sympy.symbols('mp', positive=True)

        self.omegacilist = []
        for z, mi in zip(self.Zilist, self.milist):
            self.omegacilist.append(z * self.echrg * self.bnorm / mi)

        for z, ni in zip(self.Zilist, self.n0list):
            ne += z * ni
        self.n0list.append(ne)  # Todo move e first consistent with NIMROD
        self.nplist = [_ * sp for _ in sympy.symbols(f'np:{self.nis}')]
        nep = 0
        for z, nip in zip(self.Zilist, self.nplist):
            nep += z * nip
        self.nplist.append(nep)
        self.nnorm = self.n0list[-1]  # normalize by electron density

        self.t0list = [_ for _ in sympy.symbols(f'Ts:{self.ns}',
                                                nonnegative=True)]
        self.tplist = [_ * sp for _ in sympy.symbols(f'tp:{self.ns}')]
        self.tnorm = sympy.symbols('Tref', positive=True)

        self.v0maglist = [_ for _ in sympy.symbols(f'V0mag:{self.nis}',
                                                   nonnegative=True)]
        thetav, phiv = sympy.symbols("theta_v phi_v", real=True)
        self.v0list = []
        for v0m in self.v0maglist:
            thisv0 = v0m * ((self.N.i * sympy.sin(thetav) + self.N.j *
                             sympy.cos(thetav)) * sympy.sin(phiv) +
                            self.N.k * sympy.cos(phiv))
            thisv0 = thisv0.subs([(thetav, 0), (phiv, 0)])  # simplify
            self.v0list.append(thisv0)

        vpxlist = [_ for _ in sympy.symbols(f'vpx:{self.nis}',
                                            positive=True)]
        vpylist = [_ for _ in sympy.symbols(f'vpy:{self.nis}',
                                            positive=True)]
        vpzlist = [_ for _ in sympy.symbols(f'vpz:{self.nis}',
                                            positive=True)]
        self.vplist = []
        for vpx, vpy, vpz in zip(vpxlist, vpylist, vpzlist):
            self.vplist.append((vpx * self.N.i + vpy * self.N.j +
                                vpz * self.N.k) * sp)

        self.vbar0 = sympy.S.Zero * self.v0list[0]
        self.vbarp = sympy.S.Zero * self.v0list[0]
        for sdx in range(self.nis):
            zs = self.Zilist[sdx]
            vs0 = self.v0list[sdx]
            vsp = self.vplist[sdx]
            ns0 = self.n0list[sdx]
            nsp = self.nplist[sdx]
            self.vbar0 += ns0 * zs * vs0
            self.vbarp += nsp * zs * vs0 + ns0 * zs * vsp
        self.vbar0 = self.vbar0 / self.n0list[-1]
        self.vbarp = self.vbarp / self.n0list[-1]
        self.vbarp += -self.nplist[-1] / self.n0list[-1] * self.vbar0

        ve0 = self.vbar0  # assume j0 = 0
        self.v0list.append(ve0)

        vep = self.vbarp
        if self.hall:
            vep += - self.jp / (self.n0list[-1] * self.echrg)
        vep = sympy.simplify(vep)
        self.vplist.append(vep)
        self.vnorm = self.va

        # Define dissapation parameters
        self.dn = sympy.symbols("D_n", nonnegative=True)
        self.nuij = sympy.symbols("nu_ij", nonnegative=True)
        self.kdivb = sympy.symbols("kdivb", nonnegative=True)
        #  TODO kappa
        self.kappalist = np.zeros(self.ns)

        self.beta = sympy.symbols("beta", nonegative=True)
        mp = 1.67262192e-27
        qe = 1.60217663e-19
        mu0 = 4e-7 * np.pi
        nref = 1e20
        # reference values give Omega_p ~ 44 rad/s
        self.subdict = {self.nuij: 0.e-4,
                        self.t0list[0]: 1 * self.tnorm,
                        self.t0list[1]: 1 * self.tnorm,
                        self.t0list[2]: 1 * self.tnorm,
                        self.tnorm: self.beta * self.bnorm**2 /
                        (self.mu0 * nref * self.kb),
                        self.bnorm: sympy.sqrt(self.mu0 * self.n0list[-1] *
                                               mp * self.va**2),
                        self.Zilist[0]: 1, self.Zilist[1]: 1,
                        self.milist[0]: 2 * self.mnorm,
                        self.milist[1]: 3 * self.mnorm,
                        self.n0list[0]: 0.5 * nref,
                        self.n0list[1]: 0.5 * nref,
                        self.v0maglist[0]: 0 * self.va,
                        self.v0maglist[1]: 0 * self.va,
                        self.mnorm: mp,
                        self.beta: 0.1,
                        self.va: 1,
                        self.dt: .1,
                        self.th_o: 0.5,
                        self.Gamma: 5./3., self.dn: 0.,
                        self.th_adv: 0.5, self.th_si: 0.5, self.th_d: 1.,
                        self.th_b: 0.5, self.kdivb: 1.e-3,
                        self.mu0: mu0, self.kb: qe, self.echrg: qe,
                        self.th_ihy: 0.0
                        }

        return

    def __get_divten(self, th_adv_=1, th_d_=1, th_gv_=1, crhop=1):
        '''
        Return divten tensor ( div ( rhovv + Pi) )
        The gyroviscous and advection terms may use different time centering
        than the perpendicular and parallel terms. This can be accounted for
        with the optional theta input variables.

        TODO
        '''
        pass

    def _get_lin_mat(self):
        '''
        Create matrix representing analytic linearized system of equations.
        This is called after other matrices are computed, so we can reuse
        the previous results
        '''
        self.lin_mat = self.MList[1] + self.MList[3]
        self.lin_mat += self.MList[0]

    def _get_rhs_mat(self):
        '''
        Compute the list of RHS matrices for the multi-step method:

        Multi-step method
        L_ODE del U^* = dt / 2 M_ODE U^i
        L_v del U^** = dt  M_v U^*
        L_ODE del U^*** = dt / 2 M_ODE U^**
        L_nTB del U^i+1 = dt M_nTB U^***
        return a linearized matrix of the full equations
        du/dt = M u
        '''
        ode_mat = self._get_rhs_mat_ode()
        self.MList.append(ode_mat)
        self.MList.append(self._get_rhs_mat_v())
        self.MList.append(ode_mat)
        self.MList.append(self._get_rhs_mat_ntb())

    def _get_rhs_mat_ode(self):
        '''
        Compute the rhs matrix for the ODE solves
        The order of the equations is V,n,T,B
        Call specific helper subroutines for individual field solves
        If there is no ODE terms in a equation, set RHS to zero

        '''
        eqnlist = []
        # V_s eqn
        for sdx in range(self.nis):
            eqnlist.append(self._get_rhs_ode_vps(sdx))
        # n_s eqn
        for sdx in range(self.nis):
            np = self.nplist[sdx]
            npeqn = sympy.S.Zero * np
            eqnlist.append([npeqn, 'scalar', np, self.nnorm])
        # T_s eqn
        for sdx in range(self.ns):
            tp = self.tplist[sdx]
            tpeqn = sympy.S.Zero * tp
            eqnlist.append([tpeqn, 'scalar', tp, self.tnorm])
        # B eqn
        bpeqn = sympy.S.Zero * self.bp
        eqnlist.append([bpeqn, 'vector', self.bp, self.bnorm])

        matrix = self._process_equations(eqnlist)
        if (self.verbosity > 0):
            sympy.pprint(matrix)
        return matrix

    def _get_rhs_ode_vps(self, sdx):
        '''
        Compute the rhs matrix terms that corresposnd to
        the RHS of the ion momentum equation for the ODE solves
        TODO electron placeholder terms
        TODO ion placeholder terms
        '''
        zs = self.Zilist[sdx]
        ms = self.milist[sdx]
        ns0 = self.n0list[sdx]
        e = self.echrg
        vs0 = self.v0list[sdx]
        vsp = self.vplist[sdx]

        B0 = self.B0
        bp = self.bp

        relax = sympy.S.Zero * vs0
        for adx in range(self.nis):
            vap = self.vplist[adx]
            if adx != sdx:
                relax += self.nuij * (vsp - vap)
        relax = relax / (ms * ns0)
        dv0 = vs0 - self.vbar0
        dvp = vsp - self.vbarp
        veqn = dv0.cross(bp) + dvp.cross(B0)
        veqn = veqn * zs * e / ms
        veqn = veqn - relax
        return [veqn, 'vector', vsp, self.vnorm]

    def _get_rhs_mat_v(self):
        '''
        Compute the rhs matrix for the v solves
        The order of the equations is V,n,T,B
        Call specific helper subroutines for individual field solves
        Set RHS to zero for the n,T,B solves
        '''
        eqnlist = []
        # V_s eqn
        for sdx in range(self.nis):
            eqnlist.append(self._get_rhs_mat_vps(sdx))
        # n_s eqn
        for sdx in range(self.nis):
            np = self.nplist[sdx]
            npeqn = sympy.S.Zero * np
            eqnlist.append([npeqn, 'scalar', np, self.nnorm])
        # T_s eqn
        for sdx in range(self.ns):
            tp = self.tplist[sdx]
            tpeqn = sympy.S.Zero * tp
            eqnlist.append([tpeqn, 'scalar', tp, self.tnorm])
        # B eqn
        bpeqn = sympy.S.Zero * self.bp
        eqnlist.append([bpeqn, 'vector', self.bp, self.bnorm])
        matrix = self._process_equations(eqnlist)
        if (self.verbosity > 0):
            sympy.pprint(matrix)
        return matrix

    def _get_rhs_mat_vps(self, sdx):
        '''
        Compute the rhs matrix terms that corresposnd to
        the RHS of the ion momentum equation for the v solve
        TODO ion placeholder terms
        '''
        zs = self.Zilist[sdx]
        ms = self.milist[sdx]
        kb = self.kb
        vs0 = self.v0list[sdx]
        vsp = self.vplist[sdx]
        ns0 = self.n0list[sdx]
        nsp = self.nplist[sdx]
        ts0 = self.t0list[sdx]
        tsp = self.tplist[sdx]
        ne0 = self.n0list[-1]
        nep = self.nplist[-1]
        te0 = self.t0list[-1]
        tep = self.tplist[-1]

        jp = self.jp
        B0 = self.B0
        psp = nsp * kb * ts0 + ns0 * kb * tsp
        pep = nep * kb * te0 + ne0 * kb * tep

        # J0, B0, grad Pe0, grad Ps0 = zero
        veqn = zs / (ne0 * ms) * (jp.cross(B0) - grad(pep))
        veqn -= grad(psp) / (ns0 * ms)

        # advecton (grad Vs0 = 0)
        veqn -= vs0.dot(delop)(vsp)

        return [veqn, 'vector', vsp, self.vnorm]

    def _get_rhs_mat_ntb(self):
        '''
        Compute the rhs matrix for the n,t,b solves
        The order of the equations is V,n,T,B
        Call specific helper subroutines for individual field solves
        Set RHS to zero for the v solves
        '''
        eqnlist = []
        # V_s eqn
        for sdx in range(self.nis):
            vp = self.vplist[sdx]
            veqn = sympy.S.Zero * vp
            eqnlist.append([veqn, 'vector', vp, self.vnorm])
        # n_s eqn
        for sdx in range(self.nis):
            eqnlist.append(self._get_rhs_np(sdx))
        # T_s eqn
        for sdx in range(self.ns):
            eqnlist.append(self._get_rhs_tp(sdx))
        # B eqn
        eqnlist.append(self._get_rhs_bp())
        matrix = self._process_equations(eqnlist)
        if (self.verbosity > 0):
            sympy.pprint(matrix)
        return matrix

    def _get_rhs_np(self, sdx):
        '''
        Compute the rhs matrix terms that corresposnd to
        the RHS of the ion continuity equation for the n,t,b matrix
        '''
        n0 = self.n0list[sdx]
        np = self.nplist[sdx]
        vp = self.vplist[sdx]
        v0 = self.v0list[sdx]
        rhoeqn = div(-vp * n0 - np * v0 + self.dn * grad(np))
        return (rhoeqn, 'scalar', np, self.nnorm)

    def _get_rhs_tp(self, sdx):
        '''
        Compute the rhs matrix terms that corresposnd to
        the RHS of the temperature equation for the n,t,b matrix
        '''
        tp = self.tplist[sdx]
        t0 = self.t0list[sdx]
        n0 = self.n0list[sdx]
        vp = self.vplist[sdx]
        v0 = self.v0list[sdx]
        # Todo q = n k T vs kappa grad(T)
        # Todo heating
        kappa = self.kappalist[sdx]
        teqn = (-v0.dot(delop)(tp) - vp.dot(delop)(t0) - (self.Gamma - 1) *
                (tp * div(v0) + t0 * div(vp) -
                 div(kappa * grad(tp)) / n0))
        return (teqn, 'scalar', tp, self.tnorm)

    def _get_rhs_bp(self):
        '''
        Compute the rhs matrix terms that corresposnd to
        the RHS of the induction equation for the n,t,b matrix
        TODO restivity
        TODO hall terms
        '''
        B0 = self.B0
        bp = self.bp

        ep = -self.vbarp.cross(B0) - self.vbar0.cross(bp)
        indeqn = -curl(ep) + grad(self.kdivb * div(bp))
        return (indeqn, 'vector', bp, self.bnorm)

    def _get_lhs_mat(self):
        '''
        Compute the list of LHS matrices for the multi-step method:

        Multi-step method
        L_ODE del U^* = dt / 2 M_ODE U^i
        L_v del U^** = dt  M_v U^*
        L_ODE del U^*** = dt / 2 M_ODE U^**
        L_nTB del U^i+1 = dt M_nTB U^***
        return a linearized matrix of the full equations
        du/dt = M u
        '''

        ode_mat = self._get_lhs_mat_ode()
        self.LList.append(ode_mat)
        self.LList.append(self._get_lhs_mat_v())
        self.LList.append(ode_mat)
        self.LList.append(self._get_lhs_mat_ntb())

    def _get_lhs_mat_ode(self):
        '''
        Compute the lhs matrix for the ODE solves
        The order of the equations is V,n,T,B
        Call specific helper subroutines for individual field solves
        If there is no ODE terms in a equation, set LHS martix elements
        to the indenty matrix
        '''
        eqnlist = []
        # V_s eqn
        for sdx in range(self.nis):
            eqnlist.append(self._get_lhs_ode_vps(sdx))
        # n_s eqn
        for sdx in range(self.nis):
            np = self.nplist[sdx]
            npeqn = np
            eqnlist.append([npeqn, 'scalar', np, self.nnorm])
        # T_s eqn
        for sdx in range(self.ns):
            tp = self.tplist[sdx]
            tpeqn = tp
            eqnlist.append([tpeqn, 'scalar', tp, self.tnorm])
        # B eqn
        bpeqn = self.bp
        eqnlist.append([bpeqn, 'vector', self.bp, self.bnorm])
        matrix = self._process_equations(eqnlist)
        if (self.verbosity > 0):
            sympy.pprint(matrix)
        return matrix

    def _get_lhs_ode_vps(self, sdx):
        '''
        Compute the lhs matrix terms that corresposnd to
        the LHS of the ion momentum equation for the ODE solves
        dt_ode = 0.5 * self.dt
        vpeqn += Zs e / ms Vs x B 0+.....
        '''

        vsp = self.vplist[sdx]
        ne0 = self.n0list[-1]

        vdamp = vsp
        for adx in range(self.nis):
            za = self.Zilist[adx]
            vap = self.vplist[adx]
            na0 = self.n0list[adx]
            vdamp += -na0 * za * vap / ne0

#        vdamp = self.th_ode * self.dt * zs * e / ms * vdamp.cross(B0)
#        vdamp = self.th_ode * self.dt**2/30 * zs * e / ms * vdamp.cross(B0)
#        vpeqn = vsp - vdamp
        vpeqn = vsp
        return (vpeqn, 'vector', vsp, self.vnorm)

    def _get_lhs_mat_v(self):
        '''
        Compute the Lhs matrix for the v solves
        The order of the equations is V,n,T,B
        Call specific helper subroutines for individual field solves
        Set LHS to the indenity for the n,T,B solves
        '''
        eqnlist = []
        # V_s eqn
        for sdx in range(self.nis):
            eqnlist.append(self._get_lhs_mat_vps(sdx))
        # n_s eqn
        for sdx in range(self.nis):
            np = self.nplist[sdx]
            npeqn = np
            eqnlist.append([npeqn, 'scalar', np, self.nnorm])
        # T_s eqn
        for sdx in range(self.ns):
            tp = self.tplist[sdx]
            tpeqn = tp
            eqnlist.append([tpeqn, 'scalar', tp, self.tnorm])
        # B eqn
        bpeqn = self.bp
        eqnlist.append([bpeqn, 'vector', self.bp, self.bnorm])
        matrix = self._process_equations(eqnlist)
        if (self.verbosity > 0):
            sympy.pprint(matrix)
        return matrix

    def _get_lhs_mat_vps(self, sdx):
        '''
        Compute the Lhs matrix for the ion momentum equation for the
        v solves
        TODO divpi
        '''
        ms = self.milist[sdx]
        vsp = self.vplist[sdx]
        ns0 = self.n0list[sdx]
        ne0 = self.n0list[-1]
        pe0 = ne0 * self.kb * self.t0list[-1]
        ps0 = ns0 * self.kb * self.t0list[sdx]
        zs = self.Zilist[sdx]
        vs0 = self.v0list[sdx]
        veqn = vsp + self.dt * self.th_adv * (vs0.dot(delop)(vsp) +
                                              vsp.dot(delop)(vs0))
        chrg_ratio = zs * self.echrg / ms
        vbar0 = sympy.S.Zero * vs0
        vbarp = sympy.S.Zero * vs0
        #  here vbarp only includes contributions from vap
        for adx in range(self.nis):
            za = self.Zilist[adx]
            va0 = self.v0list[adx]
            vap = self.vplist[adx]
            na0 = self.n0list[adx]
            vbar0 += na0 * za * va0
            vbarp += na0 * za * vap
        vbar0 = vbar0 / ne0
        vbarp = vbarp / ne0

        #  si op, J0 = 0
        dtfac = self.dt**2 * self.th_si**2
        chrg_weight = zs / (ne0 * ms)
        if self.siop == 'split':
            vpop1 = vsp
            vpop2 = vsp
            dvlor = vsp * (1.0 - ns0 * zs / ne0)
        elif self.siop == 'cw':
            vpop1 = vsp
            vpop2 = vsp
            dvlor = vsp - vbarp
        elif self.siop == 'da':
            vpop1 = vbarp
            vpop2 = vsp
            dvlor = vsp - vbarp
        else:
            pass

        siop1 = curl(curl(vpop1.cross(self.B0))).cross(self.B0) / self.mu0
        siop1 += grad(vpop1.dot(grad(pe0)) + self.Gamma * pe0 * div(vpop1))
        siop2 = grad(vpop2.dot(grad(ps0)) + self.Gamma * ps0 * div(vpop2))
        siop = siop1 * chrg_weight + siop2 / (ns0 * ms)
        lorop = self.dt * self.th_ihy * dvlor.cross(self.B0) * chrg_ratio
        veqn -= dtfac * siop + lorop  # TODO checksign of lorop

        # second si operator

        # add test operator to LHS
        # ode_eqn = vp.cross(self.B0) - (ns0 * zs * vsp).cross(self.B0) / ne0
        # ode_eqn = (1-self.fode) * ode_eqn * zs * self.echrg / ms
        # veqn = veqn - ode_eqn * self.dt * self.th_o

        return (veqn, 'vector', vsp, self.vnorm)

    def _get_lhs_mat_ntb(self):
        eqnlist = []
        '''
        Compute the Lhs matrix for the n,T,B solves
        The order of the equations is V,n,T,B
        Call specific helper subroutines for individual field solves
        Set LHS to the indenity for the v solves
        '''
        # V_s eqn
        for sdx in range(self.nis):
            vp = self.vplist[sdx]
            veqn = vp
            eqnlist.append([veqn, 'vector', vp, self.vnorm])
        # n_s eqn
        for sdx in range(self.nis):
            eqnlist.append(self._get_lhs_np(sdx))
        # T_s eqn
        for sdx in range(self.ns):
            eqnlist.append(self._get_lhs_tp(sdx))
        # B eqn
        eqnlist.append(self._get_lhs_bp())

        matrix = self._process_equations(eqnlist)
        if (self.verbosity > 0):
            sympy.pprint(matrix)
        return matrix

    def _get_lhs_np(self, sdx):
        '''
        Compute the Lhs matrix for the ion continuity equation for the
        n,t,b solves
        '''
        np = self.nplist[sdx]
        v0 = self.v0list[sdx]
        lhs = self.th_adv * v0 * np
        rhoeqn = np + self.dt * div(lhs - self.th_d * self.dn * grad(np))
        return (rhoeqn, 'scalar', np, self.nnorm)

    def _get_lhs_tp(self, sdx):
        '''
        Compute the Lhs matrix for the temperature equation for the
        n,t,b solves
        TODO q = n k T vs kappa grad(T)
        '''
        tp = self.tplist[sdx]
        n0 = self.n0list[sdx]
        v0 = self.v0list[sdx]
        kappa = self.kappalist[sdx]
        teqn = tp + self.dt * (self.th_adv * (
            v0.dot(delop)(tp) + (self.Gamma - 1) * tp * div(v0)) -
           (self.Gamma - 1) * div(self.th_d * kappa * grad(tp)) / n0)
        return (teqn, 'scalar', tp, self.tnorm)

    def _get_lhs_bp(self):
        '''
        Compute the Lhs matrix for the induction equation for the
        n,t,b solves

        TODO restivity
        TODO hall terms
        '''

        ne0 = self.n0list[-1]
        vcw0 = sympy.S.Zero * self.v0list[-1]
        for adx in range(self.nis):
            na0 = self.n0list[adx]
            va0 = self.v0list[adx]
            za = self.Zilist[adx]
            vcw0 += - na0 * za * va0
        vcw0 = vcw0 / ne0

        beqn = self.bp + self.dt * self.th_b * curl(vcw0.cross(self.bp))
        beqn = beqn - self.dt * grad(self.kdivb * div(self.bp))
        return (beqn, 'vector', self.bp, self.bnorm)

    def _get_rhs_fac(self):
        '''
        Multi-step method
        L_ODE del U^* = dt / 2 M_ODE U^i
        L_v del U^** = dt  M_v U^*
        L_ODE del U^*** = dt / 2 M_ODE U^**
        L_nTB del U^i+1 = dt M_nTB U^***
        '''
        self.fList.append(0.5)
        self.fList.append(1.0)
        self.fList.append(0.5)
        self.fList.append(1.0)

    def _get_ode_list(self):
        '''
        Multi-step method
        L_ODE del U^* = dt / 2 M_ODE U^i
        L_v del U^** = dt  M_v U^*
        L_ODE del U^*** = dt / 2 M_ODE U^**
        L_nTB del U^i+1 = dt M_nTB U^***
        '''
        self.odeList.append(True)
        self.odeList.append(False)
        self.odeList.append(True)
        self.odeList.append(False)

    def plot_dispersion(self, kmin=1, kmax=1000, omegamin=None, omegamax=None,
                        outfile=None):
        '''
        Plot omega versus k for 3 values of phi_k: 0.03, pi/3, 0.97*pi/2
        Solve the exact system: du = dt M u
        and the time-discretized von Neumann analysis: L du = dt M u
        '''
        self.lambdify_mats()
        dt_ = self.subdict[self.dt]
        phiarr = [0.03, np.pi / 3.0, 0.97 * np.pi / 2.0]
        npts = int(10*(np.log10(kmax)-np.log10(kmin)) + 1)
        karr = np.logspace(np.log10(kmin), np.log10(kmax), num=npts,
                           dtype=np.float64)
        omega_an = self.compute_analytic_eigenvalues(karr, phiarr)
        omega_num = self.compute_numerical_eigenvalues(karr, phiarr)
        lsan = '--'
        lsdt = ''
        msan = ''
        msdt = '+'
        lsan_im = ''
        msan_im = 'x'
        markersize = 10
        if self.logplot:
            fig, axs = plt.subplots(figsize=[24, 12], ncols=4,
                                    nrows=len(phiarr))
            for iphi, phi in enumerate(phiarr):
                ax = axs[iphi, 0]
                ax.loglog(karr, np.real(omega_an[iphi]), linestyle=lsan,
                          marker=msan, markersize=markersize)
                ax.set_prop_cycle(None)
                ax.loglog(karr, np.real(omega_num[iphi]), linestyle=lsdt,
                          marker=msdt, markersize=markersize)
                ax.set(ylabel='Re(omega)')
                ax.text(0.05, 0.95, 'phik='+f"{phi:.4f}",
                        transform=ax.transAxes, fontsize=14,
                        verticalalignment='top')
                ax = axs[iphi, 1]
                ax.loglog(karr, -np.real(omega_an[iphi]), linestyle=lsan,
                          marker=msan, markersize=markersize)
                ax.set_prop_cycle(None)
                ax.loglog(karr, -np.real(omega_num[iphi]), linestyle=lsdt,
                          marker=msdt, markersize=markersize)
                ax.set(ylabel='-Re(omega)')
                ax.text(0.05, 0.95, 'phik='+f"{phi:.4f}",
                        transform=ax.transAxes, fontsize=14,
                        verticalalignment='top')
                ax = axs[iphi, 2]
                ax.loglog(karr, np.imag(omega_an[iphi]), linestyle=lsan_im,
                          marker=msan_im, markersize=markersize)
                ax.set_prop_cycle(None)
                ax.loglog(karr, np.imag(omega_num[iphi]), linestyle=lsdt,
                          marker=msdt, markersize=markersize)
                ax.set(ylabel='Im(omega)')
                ax = axs[iphi, 3]
                ax.loglog(karr, -np.imag(omega_an[iphi]), linestyle=lsan_im,
                          marker=msan_im, markersize=markersize)
                ax.set_prop_cycle(None)
                ax.loglog(karr, -np.imag(omega_num[iphi]), linestyle=lsdt,
                          marker=msdt, markersize=markersize)
                ax.set(ylabel='-Im(omega)')
            thoff = 1.1
            dthline = 2 * np.pi / dt_
            for ax in axs.reshape(-1):
                ax.axhline(y=dthline, color='k')
                ax.annotate('2pi/dt', xy=(kmin, thoff * dthline), fontsize=16)
            for ax in axs[-1, :]:
                ax.set(xlabel='wavenumber')
        else:
            fig, axs = plt.subplots(figsize=[24, 12], ncols=2,
                                    nrows=len(phiarr))
            for iphi, phi in enumerate(phiarr):
                ax = axs[iphi, 0]
                ax.plot(karr, np.real(omega_num[iphi]), linestyle=lsdt,
                        marker=msdt, markersize=markersize)
                ax.set(ylabel='Re(omega)')
                ax.text(0.05, 0.95, 'phik='+f"{phi:.4f}",
                        transform=ax.transAxes, fontsize=14,
                        verticalalignment='top')
                ax.set_ylim(-5, 5)
                ax.set_xlim(0, 150)
                ax = axs[iphi, 1]
                ax.plot(karr, -np.imag(omega_num[iphi]), linestyle=lsdt,
                        marker=msdt, markersize=markersize)
                ax.set(ylabel='-Im(omega)')
                ax.set_xlim(0, 150)
            thoff = 1.1
            dthline = 2 * np.pi/dt_
            for ax in axs.reshape(-1):
                ax.axhline(y=dthline, color='k')
                ax.annotate('2pi/dt', xy=(kmin, thoff * dthline), fontsize=16)
            for ax in axs[-1, :]:
                ax.set(xlabel='wavenumber')

        plt.tight_layout()
        if outfile:
            plt.savefig(outfile)
        plt.show()

    def plot_pub(self, kmin=1, kmax=1000, omegamin=None, omegamax=None):
        '''
        Plot omega versus k for talks and publications
        This subroutine is meant to changed often,
        but changes should not checked into main
        Solve the exact system: du = dt M u
        and the time-discretized von Neumann analysis: L du = dt M u
        '''
        self.lambdify_mats()
        phiarr = [0.03, np.pi / 3.0, 0.97 * np.pi / 2.0]
        npts = int(10*(np.log10(kmax)-np.log10(kmin)) + 1)
        karr = np.logspace(np.log10(kmin), np.log10(kmax), num=npts,
                           dtype=np.float64)
        omega_an = self.compute_analytic_eigenvalues(karr, phiarr)
        omega_num = self.compute_numerical_eigenvalues(karr, phiarr)
        lsan = '--'
        lsdt = ''
        msan = ''
        msdt = '+'
        lsan_im = ''
        msan_im = 'x'
        markersize = 10
        figsize = [6, 4]
        for iphi, thisphi in enumerate(phiarr):
            fig, ax = plt.subplots(figsize=figsize)
            ax.loglog(karr, np.real(omega_an[iphi]), linestyle=lsan,
                      marker=msan, markersize=markersize)
            ax.set(ylabel='Re(omega)')
            ax.set(xlabel='wavenumber')
            plt.tight_layout()
            plt.show()
            fig, ax = plt.subplots(figsize=figsize)
            ax.plot(karr, omega_an[iphi], linestyle=lsan,
                    marker=msan, markersize=markersize)
            ax.set(ylabel=r'$\omega$')
            ax.set(xlabel='k')
            ax.set_title(
                r"Analytic Dispersion: $k\cdot \hat{b} \approx $" +
                f"{np.cos(thisphi):.3f}")
            plt.tight_layout()
            plt.show()
            fig, ax = plt.subplots(figsize=figsize)
            ax.set_title(
                r"Positive Real Frequency: $k\cdot \hat{b} \approx $" +
                f"{np.cos(thisphi):.3f}")
            ax.loglog(karr, np.real(omega_an[iphi]), linestyle=lsan,
                      marker=msan, markersize=markersize)
            ax.set_prop_cycle(None)
            ax.loglog(karr, np.real(omega_num[iphi]), linestyle=lsdt,
                      marker=msdt, markersize=markersize)
            ax.set(ylabel=r'$Re(\omega)$')
            ax.set(xlabel=r'k')
            plt.tight_layout()
            plt.show()
            fig, ax = plt.subplots(figsize=figsize)
            ax.set_title(
                r"Growth Rate: $k\cdot \hat{b} \approx $" +
                f"{np.cos(thisphi):.3f}")
            ax.loglog(karr, -np.imag(omega_an[iphi]), linestyle=lsan_im,
                      marker=msan_im, markersize=markersize)
            ax.set_prop_cycle(None)
            ax.loglog(karr, -np.imag(omega_num[iphi]), linestyle=lsdt,
                      marker=msdt, markersize=markersize)
            ax.set(ylabel=r'$Re(\omega)$')
            ax.set(xlabel=r'k')
            plt.tight_layout()
            plt.show()


def main():
    '''
    Parse command line inputs and then compute dispersion relation
    '''
    parser = argparse.ArgumentParser(
        description='Perform a Von Neumann plane wave analysis.')
    parser.add_argument('-i', '--interactive', action="store_true",
                        help='Drop to interactive mode')
    parser.add_argument('-v', '--verbose', action='count', default=0,
                        help="Vebosity of debuging info (2 levels)")
    parser.add_argument('-l', '--logplot', action="store_true",
                        help='plot dispersion using a loglog plot')
    parser.add_argument('-f', '--file', default=None,
                        help='Save dispersion plot to file')
    parser.add_argument('-s', '--save', action="store_true",
                        help='Save matrices to file')
    parser.add_argument('-r', '--read', action="store_true",
                        help='Read matrices from file')
    parser.add_argument('-m', '--mfile', default=None,
                        help='Read matrices from file: mfile')
    parser.add_argument('-d', '--da', action="store_true",
                        help="Use differential approximation operator")
    parser.add_argument('-c', '--cw', action="store_true",
                        help="Use charge weighted operator with full ihy op")

    args = parser.parse_args()

    mhdanalysis = ThreeSpecies()
    mhdanalysis.verbosity = args.verbose
    mhdanalysis.logplot = args.logplot
    if (args.da and args.cw):
        print('Options da and cw conflict, only specify one')
        raise ValueError
    elif args.da:
        mhdanalysis.siop = 'da'
    elif args.cw:
        mhdanalysis.siop = 'cw'

    if args.mfile is not None:
        # Load matrices from given file
        mhdanalysis._define_symbols()
        mhdanalysis.load(args.mfile)
    elif args.read:
        # Load matrices from default file
        mhdanalysis._define_symbols()
        mhdanalysis.load()
    else:
        # Compute matrices from scratch
        mhdanalysis.create_matrices()
        if args.save:
            mhdanalysis.save()

    # plot disperion relation
    mhdanalysis.plot_dispersion(kmin=0.1, kmax=1000., outfile=args.file)

    if args.interactive:
        # readline is optional, it allows Up/Down/History in the console
        import readline  # noqa
        import code
        variables = globals().copy()
        variables.update(locals())
        shell = code.InteractiveConsole(variables)
        shell.interact()


if __name__ == "__main__":
    main()

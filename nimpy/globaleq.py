#!/use/bin/env python3

r"""
| Compute global equilibrium parameters.
|
| The computation applies to both flux-aligned and non-aligned meshes.
| The output replaces the file fluxgrid.out.
|
| The following definitions are used:
|
| $R_m$ $Z_m$: R,Z location of the magnetic axis
| $R_o$: average R of last closed flux surface at $Z_m$
| $a$: half width of last closed flux surface at $Z_m$
| $<p>$: volume averaged pressure
| $f_o$: value of field $f$ at the magnetic axis
| $f_a$: flux surface averaged value of $f$ along the last closed flux surface
|
| $li = \frac{\overline{B_p^2}} {B_p \left ( a \right ) ^2} $
| The overline is a volume average:
| $\overline{B_p^2} = \frac{\int dV JB_p^2}{ \int dV J}$
|
|
| li1 - li3 use different methods to compute $B_p(a)^2$
| li1: $B_p(a) = <B_p^2>_a / <B_p>_a$, where <> is the flux surface average
| li2 and li3 compute $B_p$ using Ampere's law and the volume of a torus:
| $B_p(a) = \frac{I_{plasma}}{2 \pi a}$
| $Vol = 2 \pi^2 R a^2 = \int dV J$
| li2 uses $R = R_m$
| li3 uses $R = R_o$
|
| $\beta_p = \frac{2 \mu_0 \overline{p}}{ B_p}$
| $\beta_p$1-3 differ in how $B_p$ is computed.
| They use the same definitions as li1-3.
|
| $\beta_n = I/aB_t$ is computed as a percentage with I[Ma], a[m], B[T]
|
| All other definitions are self-explanatory

"""

import argparse
import numpy as np
from scipy.interpolate import UnivariateSpline

import nimpy.eval_nimrod as eval
import nimpy.fsa as fsa
import nimpy.util.nimrodin
from nimpy.util.yaml_input import yaml_input


GLOBALEQ_NML = "globaleq"

GLOBALEQ_INPUTS = {
    'nimrodin': 'nimrod.in',  # path to nimrod.in file
    'dumpfile': 'dumpgll.00000.h5',  # dumpfile to read
    'outfile': 'globaleq.txt',  # output file name
}


class GlobalEq:
    ''' Class for computing global equilibrium parameters
    '''

    def __init__(self, evalnimrod, nimrodin, globaleq_nml):
        '''Initialize GlobalEq instance

          :params evalnimrod: evalnimrod instance
          :params nimrodin: nimrodin instance
          :params globaleq_nml: globaleq namelist
        '''

        self.__evalnimrod = evalnimrod
        self.__nimrodin = nimrodin
        self.__inputdict = globaleq_nml
        self.__surface_integration()
        self.__axis_quantities()
        self.__geometric_quantities()
        self.__lcfs_quantities()
        self.__beta_current_quantities()

    def write(self):
        '''Write a textfile containing the equilibrium quantities '''

        outfile = self.__inputdict['outfile']
        try:
            with open(outfile, 'w', encoding="utf-8") as f:
                x4 = "    "
                f.write("GEOMETRIC QUANTITIES:\n")
                msg = f"R_m = {self.rzo[0]:10.3e} m" + x4
                msg += f"Z_m = {self.rzo[1]:10.3e} m\n"
                f.write(msg)
                msg = f"R_o = {self.rmean:10.3e} m" + x4
                msg += f"a   = {self.amean:10.3e} m\n"
                f.write(msg)
                msg = f"A = R_o/a   = {self.aratio:10.3e} m\n"
                f.write(msg)
                msg = f"kappa       = {self.kappa:10.3e} m\n"
                f.write(msg)
                msg = f"delta_top   = {self.delta1:10.3e} m" + x4
                msg += f"delta_bottom = {self.delta2:10.3e} m\n"
                f.write(msg)
                f.write("GLOBAL PLASMA QUANTITIES:\n")
                msg = f"B_o = {self.bt0:10.3e} T" + x4
                msg += f"I   = {self.crnt/1e6:10.3e} MA" + x4
                msg += f"I_N = I/aB = {self.inorm:10.3e}\n"
                f.write(msg)
                f.write("SAFETY FACTOR QUANTITIES:\n")
                msg = f"q0   = {self.q0:10.3e}" + x4
                msg += f"qmin = {self.qmin:10.3e}\n"
                msg += f"qmax = {self.qmax:10.3e}" + x4
                msg += f"qa   = {self.qa:10.3e}\n"
                f.write(msg)
                f.write("CURRENT QUANTITIES\n")
                msg = f"li1 = {self.li1:10.3e}" + x4
                msg += f"li2 = {self.li2:10.3e}" + x4
                msg += f"li3 = {self.li3:10.3e}\n"
                msg += f"Total flux = {self.psio:10.3e} Wb\n"
                f.write(msg)
                f.write("PRESSURE QUANTITIES\n")
                msg = f"p_0    = {self.p0:10.3e} Pa" + x4
                msg += f"<p>    = {self.pave:10.3e} Pa\n"
                msg += f"betap1  = {self.betap1:10.3e}" + x4
                msg += f"betap2  = {self.betap2:10.3e}\n"
                msg += f"betap3  = {self.betap3:10.3e}\n"
                msg += f"betat   = {self.betat:10.3e}" + x4
                msg += f"betan   = {self.betan:10.3e}\n"
                f.write(msg)
                # This should be generalized for multispecies
                f.write("TEMPERATURE & DENSITY:\n")
                msg = f"n_0    = {self.n0:10.3e} 1/m^3" + x4
                msg += f"n_a    = {self.na:10.3e} 1/m^3\n"
                msg += f"T_i0   = {self.ti0:10.3e} eV" + x4
                msg += f"T_ia   = {self.tia:10.3e} eV\n"
                msg += f"T_e0   = {self.te0:10.3e} eV" + x4
                msg += f"T_ea   = {self.tea:10.3e} eV\n"
                f.write(msg)
                f.write("SEPARATRIX LOCATIONS:\n")
                msg = f"At Z_m: R,Z = {self.rs1:10.3e}, {self.rzo[1]:10.3e}\n"
                msg += f"and R,Z = {self.rs2:10.3e}, {self.rzo[1]:10.3e}\n"
                msg += f"At max R: R,Z = {self.rsn:10.3e}, {self.zsn:10.3e}\n"
                msg += f"At min R: R,Z = {self.rsx:10.3e}, {self.zsx:10.3e}\n"
                msg += f"At max Z: R,Z = {self.rst:10.3e}, {self.zst:10.3e}\n"
                msg += f"At min Z: R,Z = {self.rsb:10.3e}, {self.zsb:10.3e}\n"
                f.write(msg)

        except Exception as err:
            msg = f"GlobalEq write: Error opening file {outfile}"
            print(msg)
            raise err

    def __surface_integration(self):
        ''' Function for handling the call to FSA
            Also sets saftey factor variables and magnetic axis and
            lcfs location
        '''

        # @TODO use yaml_input with preset defaults to read fsa nml
        # this should be part of the fsa input conversion

        fsadict = yaml_input.input.get('fsa', {})
        nsurf = fsadict.get('nsurf', 150)
        rzx = fsadict.get('rzx', None)
        rzo = fsadict.get('rzo', [1.768, -0.018831, 0.0])
        self.__addpert = fsadict.get('addpert', False)
        neta = fsadict.get('neta', 257)

        dvar, yvals, contours = fsa.FSA(self.__evalnimrod, rzo,
                                        self.__integrand, 7, nsurf=nsurf,
                                        depvar='eta', dpow=1, rzx=rzx,
                                        normalize=False,
                                        addpert=self.__addpert, neta=neta)
        iend = -1
        while np.isnan(yvals[:, iend]).any():
            iend -= 1
        iend += yvals.shape[1]+1

        self.__dvar = dvar[:, :iend]
        self.__yvals = yvals[:, :iend]
        contours = contours[:, :, :iend]
        # set safety factor quantities
        qval = dvar[7, :iend]
        self.q0 = qval[0]
        self.qa = qval[-1]
        if self.q0 >= 0:
            self.qmin = np.amin(qval)
            self.qmax = np.amax(qval)
        else:
            self.qmax = np.amin(qval)
            self.qmin = np.amax(qval)
        # set axis location and lcfs contour
        self.rzo = np.array([contours[0, 0, 0], contours[1, 0, 0], 0.0])
        self.__lcfs = contours[:, :, -1]

    def __integrand(self, rzc, y, dy, evalnimrod, fdict):
        ''' Integrand for fsa integration

            dy[2] contains jac
            dy[4] = jac * Jphi / r
            dy[5] = jac * |B_p|
            dy[6] = jac * |Bp|^2
            dy[7] = jac * Pr
            dy[8] = jac * nd
            dy[9] = jac * ti
            dy[10] = jac * te
        '''
        if self.__addpert:
            eqflag = 3
        else:
            eqflag = 1
        b0 = evalnimrod.eval_field('b', rzc, dmode=0, eq=eqflag)
        bp = b0[0:2]
        bp2 = np.dot(bp, bp)
        j0 = evalnimrod.eval_field('j', rzc, dmode=0, eq=eqflag)
        pr = evalnimrod.eval_field('p', rzc, eq=eqflag)[0]
        nd = evalnimrod.eval_field('n', rzc, eq=eqflag)[0]
        ti = evalnimrod.eval_field('ti', rzc, eq=eqflag)[0]
        te = evalnimrod.eval_field('te', rzc, eq=eqflag)[0]
        dy[4] = j0[2] / rzc[0] * dy[2]  # total current
        dy[5] = np.sqrt(bp2) * dy[2]
        dy[6] = bp2 * dy[2]  # |grad psi / R | ^2 J
        dy[7] = pr * dy[2]
        dy[8] = nd * dy[2]
        dy[9] = ti * dy[2]
        dy[10] = te * dy[2]
        return dy

    def __axis_quantities(self):
        ''' Compute value of quantities at the magnetic axis
            This needs to be called after rzo is computed
        '''
        if self.__addpert:
            eqflag = 3
        else:
            eqflag = 1

        b0 = self.__evalnimrod.eval_field('b', self.rzo, dmode=0, eq=eqflag)
        self.bt0 = b0[2]
        self.p0 = self.__evalnimrod.eval_field('p', self.rzo, eq=eqflag)[0]
        self.n0 = self.__evalnimrod.eval_field('n', self.rzo, eq=eqflag)[0]
        self.ti0 = self.__evalnimrod.eval_field('ti', self.rzo, eq=eqflag)[0]
        self.te0 = self.__evalnimrod.eval_field('te', self.rzo, eq=eqflag)[0]

    def __geometric_quantities(self):
        ''' Compute geometric quantities associated with the lcfs
        '''

        self.rsn, self.zsn = self.__lcfs[:, np.argmax(self.__lcfs[0, :])]
        self.rsx, self.zsx = self.__lcfs[:, np.argmin(self.__lcfs[0, :])]
        self.rsb, self.zsb = self.__lcfs[:, np.argmin(self.__lcfs[1, :])]
        self.rst, self.zst = self.__lcfs[:, np.argmax(self.__lcfs[1, :])]

        intersections = []
        last = self.__lcfs[:, -1]
        for idx in range(self.__lcfs.shape[1]):
            this = self.__lcfs[:, idx]
            value = self.__intersects_horizontal(self.rzo[1], last, this)
            if value is not False:
                intersections.append(value)
            last = this
        # check for two intersections
        if len(intersections) != 2:
            value = len(intersections)
            msg = f"GlobalEq: LCFS intersects horizontal line {value} times"
            raise ValueError(msg)
        self.rs1 = intersections[0][0]
        self.rs2 = intersections[1][0]

        self.rmean = (self.rs1 + self.rs2) / 2
        self.amean = np.abs((self.rs2 - self.rs1) / 2)
        self.aratio = self.rmean / self.amean
        self.kappa = (self.zst - self.zsb) / 2. / self.amean
        self.delta1 = (self.rmean - self.rst) / self.amean
        self.delta2 = (self.rmean - self.rsb) / self.amean

    def __lcfs_quantities(self):
        ''' Compute flux surface averaged quantities at lcfs '''
        vprime = self.__dvar[6]
        nd_fsa = self.__yvals[4] / vprime
        ti_fsa = self.__yvals[5] / vprime
        te_fsa = self.__yvals[6] / vprime
        self.na = nd_fsa[-1]
        self.tia = ti_fsa[-1]
        self.tea = te_fsa[-1]

    def __beta_current_quantities(self):
        ''' Compute beta, current, and inductance '''
        mu0 = self.__nimrodin['mu0']
        twopi = 2.0 * np.pi
        bp0 = self.__yvals[2, -1] / self.__yvals[1, -1]

        volume = self.__radial_integration(self.__dvar[2], self.__dvar[6])
        i_surf = self.__radial_integration(self.__dvar[2], self.__yvals[0])
        induct = self.__radial_integration(self.__dvar[2], self.__yvals[2])
        p_vol = self.__radial_integration(self.__dvar[2], self.__yvals[3])
        self.crnt = i_surf[-1]
        self.inorm = self.crnt / (self.amean * self.bt0 * 1.e6)
        self.pave = p_vol[-1] / volume[-1]
        self.betat = 2 * self.pave * mu0 / self.bt0**2
        self.betan = 1e8 * self.amean * self.bt0 * self.betat / self.crnt
        bp23fac = 4 * twopi * p_vol[-1] / (mu0 * self.crnt**2)
        self.betap1 = 2 * mu0 * self.pave / bp0**2
        self.betap2 = bp23fac / self.rzo[0]
        self.betap3 = bp23fac / self.rmean
        self.psio = self.__dvar[2, -1] * twopi
        self.li1 = (induct[-1] / volume[-1]) / bp0**2
        li23fac = 2 * twopi * induct[-1] / (mu0 * self.crnt)**2
        self.li2 = li23fac / self.rzo[0]
        self.li3 = li23fac / self.rmean

    @staticmethod
    def __radial_integration(psi, dfdpsi):
        ''' Perform the radial integration of a quantity f

            :params psi: radial coordinate psi (not normalized)
            :params dfdpsi: f-prime
            :returns: f(psi)
        '''
        npsi = len(psi)
        if psi[0] < 0.0:
            psi *= -1
        f = np.zeros(npsi)
        fspl = UnivariateSpline(psi, dfdpsi, k=5, s=0.0,
                                check_finite=True)
        for idx in range(npsi):
            if idx == 0:
                continue
            f[idx] = fspl.integral(0, psi[idx])
        return f

    @staticmethod
    def __intersects_horizontal(zval, start, end):
        ''' Computes the intersection between a ling segment and a
            horizontal line if it exists.

            To avoid duplication, assume that the line segment excludes the
            end point: (start, end]

            :params zval: height of the horizontal line
            :params start: starting point of line segment
            :params end: end point of line segment
            :returns: intersection (Return false if no intersection)

        '''
        if zval < min(start[1], end[1]) or zval >= (max(start[1], end[1])):
            return False
        dl = end - start
        length = np.linalg.norm(dl)
        dl = dl / length
        dz = (zval - start[1]) / dl[1]
        intersection = start + dl * dz
        return intersection


def compute_global(nimpyin=None):
    ''' main function for performing analysis of equilibrium in dumpgll

    :params nimpyin: nimpy.yaml input file
    '''

    nimpyin = nimpyin or "nimpy.yaml"
    yaml_input.read_input(nimpyin)
    globaleq_inputs = yaml_input.read_namelist(
        GLOBALEQ_NML,
        GLOBALEQ_INPUTS,
        require=False
    )

    nimrodin = globaleq_inputs['nimrodin']
    dumpfile = globaleq_inputs['dumpfile']

    nimin = nimpy.util.nimrodin.NimrodIn(filename=nimrodin)
    evalnimrod = eval.EvalNimrod(dumpfile, fieldlist='ntpbj')
    geq = GlobalEq(evalnimrod, nimin, globaleq_inputs)
    geq.write()


def main():
    ''' main function for invoking globaleq from command line '''

    description = "Compute global equilibrium parameters."
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('--nimpyin', '-p',
                        default='nimpy.yaml',
                        help='yaml input file')
    args = vars(parser.parse_args())
    compute_global(nimpyin=args['nimpyin'])


if __name__ == "__main__":
    main()

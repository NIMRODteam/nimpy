### flux_map
Creates mappings between nimrod RZ coordinates and equilibrium based
flux (PEST) coordinates. Both the forward and inverse mappings are computed.
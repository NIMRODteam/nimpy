'''
This module contains a class for reading nimpy.yaml input files,
assign default values, and associated functionality
'''

import os
import yaml


class _YamlInput:
    '''
    Class for reading yaml input
    '''
    def __init__(self) -> None:
        '''
        Initialize YamlInput class
        '''

        self.__filename = None
        self.input = None

    def get_filename(self) -> str:
        '''
        Return file name
        '''

        return self.__filename

    def read_input(self, filename: str = 'nimpy.yaml') -> None:
        '''
        Read YamlInput class, return an error if file has already been
        read

        :param filename: numpy.yaml file name
        '''

        msg = f"read_input called twice for {filename}"
        assert self.__filename is None, msg

        self.__filename = filename
        # check if file exists, and if so read the file
        if not os.path.isfile(self.__filename):
            msg = f"Unable to find nimpy yaml file {self.__filename}\n" \
                + "Find an example in the source at \n" \
                + "https://gitlab.com/NIMRODteam/open/nimpy/-/" \
                + "blob/main/scripts/examples/"
            raise FileNotFoundError(msg)

        with open(self.__filename, 'r') as file:
            self.input = yaml.safe_load(file)

    @staticmethod
    def __check_keys(default: dict, namelist: dict, name: str) -> None:
        '''
        Check keys in input dict to see if the exist
        Write out a list of wrong keys

        :param default: default inputs for namelist
        :param namelist: input namelist from nimpy.yaml
        :param name: namelist name
        '''

        bad_keys = []
        for key in namelist.keys():
            if key not in default.keys():
                bad_keys.append(key)

        if bad_keys:
            msg = f"Error reading namelist {name}.\n"
            msg += f"The keys {bad_keys} are not defined."
            raise KeyError(msg)

    @staticmethod
    def __fix_input(namelist: dict) -> None:
        '''
        Check input for cases where yaml load incorrectly
        interprets as input as string. Know problems include
        the case reading the value None

        :param namelist: yaml dictionary for a namelist
        '''

        for key, value in namelist.items():
            if isinstance(value, str):
                if value.lower() == "none":
                    namelist[key] = None

    def read_namelist(self, name: str,
                      default: dict,
                      require: bool = True) -> dict:
        '''
        read a namelist from the yaml input dictionary

        :param name: namelist name
        :param default: default values for namelist
        :param require: require name in input

        :return: dict containing only the inputs for the given namelist
        '''

        if name not in self.input:
            msg = f"Namelist {name} not found in yaml file"
            if require:
                raise KeyError(msg)

        temp = self.input.get(name, {})
        self.__check_keys(default, temp,  name)
        self.__fix_input(temp)
        namelist = default.copy()
        namelist.update(temp)
        return namelist


yaml_input = _YamlInput()
